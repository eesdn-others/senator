#ifndef FIXEDHYBRID_ROUTING_HPP
#define FIXEDHYBRID_ROUTING_HPP


#include "HybridRouting.hpp"

namespace Hybrid {
/*
* Hybrid routing with fixed backup paths
*/
class FixedHybridRouting : public HybridRouting {
	public:
		FixedHybridRouting(const FixedSDNInstance& _instance, const Matrix< std::tuple<int, int, Graph::Path> >& _backup) :
			HybridRouting(_instance)
		{
			for (int i = 0; i < m_backup.size1(); ++i) {
				for (int j = 0; j < m_backup.size2(); ++j) {
					std::get<0>(m_backup(i, j)) = std::get<0>(_backup(i, j));
					std::get<1>(m_backup(i, j)) = 0;
					std::get<2>(m_backup(i, j)) = std::get<2>(_backup(i, j));
				}
			}
		}
		
		FixedHybridRouting(const FixedSDNInstance& _instance, const Matrix<int>& _backup) :
			HybridRouting(_instance)
		{
			for (int i = 0; i < m_backup.size1(); ++i) {
				for (int j = 0; j < m_backup.size2(); ++j) {
					if(_backup(i, j) != -1){
						std::get<0>(m_backup(i, j)) = _backup(i, j);
					}
				}
			}
		}

		virtual ~FixedHybridRouting() {
		}

		virtual void addBackup(const Graph::Node _u, const Graph::Node _t, const Graph::Node _nh, const Graph::Path& _path) {
			assert(!_path.empty());
			assert(std::get<0>(m_backup(_u, _t)) == _nh);
			assert(std::get<2>(m_backup(_u, _t)) == _path || std::get<2>(m_backup(_u, _t)) == Graph::Path());
			assert(0 <= _nh && _nh < m_instance.graph.getOrder());

			std::get<0>(m_backup(_u, _t)) = _nh;
			std::get<1>(m_backup(_u, _t))++;
			std::get<2>(m_backup(_u, _t)) = _path;
		}

		virtual void removeBackup(const int _u, const int _t) {
			////std::cout    << "removeBackup" << std::make_tuple(_u, _t) << std::endl;
			assert(std::get<1>(m_backup(_u, _t)) > 0);
			--std::get<1>(m_backup(_u, _t));
			// if(std::get<1>(m_backup(_u, _t)) == 0) {
			// 	std::get<2>(m_backup(_u, _t)).clear();
			// }
		}

		virtual std::vector< std::tuple<int, double, Graph::Path> > getNextHops(const int _u, const int _s, const int _t, const double _d) {
			////std::cout    << "getNextHops" << std::make_tuple(_u, _s, _t, _d) << std::endl;
			std::vector< std::tuple<int, double, Graph::Path> > nextHops;
			if(m_instance.SDN[_u]) {
	        	const int nRules = std::get<0>(m_SDNRules[_u](_s, _t));

	        	if(nRules != -1) {
	        		if(m_flowD.hasEdge(_u, nRules)) {
	        			////std::cout << "Direct SDN (rules)" << std::endl;
	        			if(m_instance.graph.getEdgeWeight(_u, nRules) - m_flowD.getEdgeWeight(_u, nRules) >= _d) {
			        		nextHops.emplace_back(nRules, 
			        			1 + m_flowD.getEdgeWeight(_u, nRules) / m_instance.graph.getEdgeWeight(_u, nRules),
			        			Graph::Path());
		        		}
	        		} else if(std::get<0>(m_backup(_u, _t)) != -1) {
	        			if(!std::get<2>(m_backup(_u, _t)).empty() && !hasValidRules(std::get<2>(m_backup(_u, _t)))) {
		        			std::get<2>(m_backup(_u, _t)).clear();	
		        		}
	        			if(!std::get<2>(m_backup(_u, _t)).empty()) {
			        			////std::cout << "with fixed path " << std::endl;
				        	if( validPathForDemand(std::get<2>(m_backup(_u, _t)), _d)) {
				        		auto end = std::get<0>(m_backup(_u, _t));
				    	    		nextHops.emplace_back(end, 
				        				2 * std::get<2>(m_backup(_u, _t)).size(), 
					        			std::get<2>(m_backup(_u, _t)));
					        }
			        	} else {
			        		////std::cout << "without fixed path " << std::endl;
			        		if(std::get<2>(m_backup(_u, _t)).empty()) {
			        			for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
			        				if(n_Path.first == std::get<0>(m_backup(_u, _t))) { // Only get path with right backup path
					        			nextHops.emplace_back(n_Path.first,
					        				2 * n_Path.second.size(),
					        				n_Path.second);
				        			}
				        		}
		        			}
	        			}
	        		}
	        	} else {
		        	////std::cout << "Search direct SDN (no rules):";
					for(auto& n : m_flowD.getNeighbors(_u)) {
						if(m_flowD.hasEdge(_u, n) && m_instance.graph.getEdgeWeight(_u, n) - m_flowD.getEdgeWeight(_u, n) >= _d) {
				    		nextHops.emplace_back(n, 
				    			1 + m_flowD.getEdgeWeight(_u, n) / m_instance.graph.getEdgeWeight(_u, n),
				    			Graph::Path());
						}
					}
	        	}
			} else {
	        	if(m_flowD.hasEdge(_u, m_instance.nextHops(_u, _t))) {
	        		////std::cout    << "Direct OSPF" << std::endl;
	        		int n = m_instance.nextHops(_u, _t);
	        		if( m_instance.graph.getEdgeWeight(_u, n) - m_flowD.getEdgeWeight(_u, n) >= _d ) {
		        		nextHops.emplace_back(n, 
		        			1 + m_flowD.getEdgeWeight(_u, n) / m_instance.graph.getEdgeWeight(_u, n),
		        			Graph::Path());
	        		}
	        	} else if(std::get<0>(m_backup(_u, _t)) != -1) {
	        		////std::cout << "OSPF Encaps already here" << m_backup(_u, _t);
	        		if(!std::get<2>(m_backup(_u, _t)).empty() && !hasValidRules(std::get<2>(m_backup(_u, _t)))) {
	        			std::get<2>(m_backup(_u, _t)).clear();	
	        		}
	        		if(!std::get<2>(m_backup(_u, _t)).empty()) {
	        			////std::cout   << "with fixed path " << std::endl;

		        		if( validPathForDemand(std::get<2>(m_backup(_u, _t)), _d)) {
			        		auto end = std::get<0>(m_backup(_u, _t));
			        		nextHops.emplace_back(end, 
			        			2 * std::get<2>(m_backup(_u, _t)).size(), 
			        			std::get<2>(m_backup(_u, _t)));
			        	}
		        	} else {
		        		////std::cout   << "without fixed path " << std::endl;
		        		if(std::get<2>(m_backup(_u, _t)).empty()) {
		        			for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
		        				if(n_Path.first == std::get<0>(m_backup(_u, _t))) { // Only get path with right backup path
				        			nextHops.emplace_back(n_Path.first,
				        				2 * n_Path.second.size(),
				        				n_Path.second);
			        			}
			        		}
	        			}
        			}
        		}
	        }
	        return nextHops;
		}
};
}
#endif