#ifndef PATH_FORMULATION
#define PATH_FORMULATION

#include <DiGraph.hpp>
#include <ShortestPath.hpp>
#include <Matrix.hpp>
#include <cplex_utility.hpp>

#include <ilcplex/ilocplex.h>

#include "Hybrid.h"
#include "Solution.hpp"

void displayConstraint(const IloCplex& _solver, const IloRange& _range) {
    std::cout << _range << '\n';
    for(auto ite = _range.getLinearIterator(); ite.ok(); ++ite) {
        std::cout << ite.getVar() << " -> " << _solver.getValue(ite.getVar()) << '\n';
    }
}

namespace Hybrid {


class PathFormulation {
public:
    PathFormulation(const Instance& _instance, const int _nbPath, const double _maxTime) :
        m_instance(_instance),
        m_nbPath(_nbPath),
        m_maxTime(_maxTime),
        m_kShortestPaths([&](){
            Matrix< std::vector<Graph::Path> > kShortestPaths(m_instance.graph.getOrder(), m_instance.graph.getOrder());
            for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) { 
                    if(y != z) {
                        kShortestPaths(y, z) = ShortestPath<DiGraph>(m_instance.graph).getKShortestPath(y, z, m_nbPath);   
                        assert(!kShortestPaths(y, z).empty());
                    }
                }
            }
            return kShortestPaths;
        }()),
        m_pathOnEdge([&](){
            std::vector<std::vector<std::tuple<Graph::Node, Graph::Node, int>>> pathOnEdge(m_instance.graph.size());
            for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                    if(y != z) {
                        for(int j = 0; j < m_nbPath; ++j) {
                            for(auto iteU = m_kShortestPaths(y, z)[j].begin(), iteV = std::next(iteU); iteV != m_kShortestPaths(y, z)[j].end(); ++iteU, ++iteV) {
                                pathOnEdge[m_instance.edgeId(*iteU, *iteV)].emplace_back(y, z, j);
                            }
                        }
                    }
                }
            }

            // for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
            //     for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
            //         if(y != z) {
            //             int j = 0;
            //             for(const auto& path : m_kShortestPaths(y, z)) {
            //                 for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
            //                     pathOnEdge[m_instance.edgeId(*iteU, *iteV)].push_back(m_nbPath * m_instance.graph.getOrder() * y + m_nbPath * z + j);
            //                 }
            //                 ++j;
            //             }
            //         }
                    
            //     }
            // }
            return pathOnEdge;
        }()),
        m_env(),
        m_model(m_env),
        m_p([&](){
            IloNumVarArray p(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_nbPath * m_instance.demands.size(), 0, 1, ILOBOOL);
            for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                    if(y != z) {
                        for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                            if(z != m_instance.demands[i].t && y != m_instance.demands[i].t) {
                                for(int j = 0; j < m_nbPath; ++j) {
                                        setIloName(p[getPIndex(y, z, j, i)], "p"+toString(std::make_tuple(j, i)));
                                }
                            }
                        }
                    }
                }
            }
            return IloAdd(m_model, p);
        }()),
        m_h([&](){
            IloNumVarArray h(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_instance.graph.getOrder(), 0, 1, ILOBOOL);
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
                    for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                        if(y != t) {
                            setIloName(h[getHIndex(u, t, y)], "h"+toString(std::make_tuple(u, t, y)));
                        }
                    }
                }
            }
            return IloAdd(m_model, h);
        }()),
        m_x([&](){
            IloNumVarArray x(m_env, m_instance.graph.size(), 0, 1, ILOBOOL);
            for(int e = 0; e < m_instance.graph.size(); ++e) {
                setIloName(x[e], "x"+toString(m_instance.graph.getEdges()[e]));
            }
            return IloAdd(m_model, x);
        }()),
        m_e([&](){
            IloNumVarArray e(m_env, m_instance.demands.size() * m_instance.graph.getOrder(), 0, 1, ILOBOOL);
            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                for(int u = 0; u < m_instance.graph.getOrder(); ++u) {
                    setIloName(e[m_instance.graph.getOrder() * i + u], "e"+toString(std::make_pair(u, i)));
                }
            }
            return IloAdd(m_model, e);
        }()),
        m_r([&](){
            IloNumVarArray r(m_env, m_instance.graph.size(), 0, 1, ILOBOOL);
            for(int e = 0; e < m_instance.graph.size(); ++e) {
                setIloName(r[e], "r"+toString(m_instance.graph.getEdges()[e]));
            }
            return IloAdd(m_model, r);
        }()),
        m_f([&](){
            IloNumVarArray f(m_env, m_instance.demands.size() * m_instance.graph.size(), 0, 1, ILOBOOL);
            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                int e = 0;
                for(const auto& edge : m_instance.graph.getEdges()) {
                    setIloName(f[m_instance.graph.size() * i + e], "f"+toString(std::make_pair(i, edge)));
                    ++e;
                }
            }
            return IloAdd(m_model, f);
        }()),
        m_n([&](){
            IloNumVarArray n(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_instance.graph.size(), 0, 1, ILOBOOL);
            for(Graph::Node s = 0; s < m_instance.graph.getOrder(); ++s) {
                for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
                    int e = 0;
                    for(const auto& edge : m_instance.graph.getEdges()) {
                        if(edge.first != t) {
                            setIloName(n[getNIndex(e, s, t)], "n"+toString(std::make_tuple(m_instance.graph.getEdges()[e], s, t)));   
                        }
                        ++e;
                    }
                }
            }
            return IloAdd(m_model, n);
        }()),
        m_s([&](){
            IloNumVarArray s(m_env, m_instance.graph.getOrder(), 0, 1, ILOBOOL);
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                setIloName(s[u], "s"+toString(u));
            }
            return IloAdd(m_model, s);
        }()),
        m_obj([&](){
            IloNumArray vals(m_env);
            IloNumVarArray vars(m_env);
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                // Link activation cost
                // if(edge.first < edge.second) {
                vars.add(m_x[e]);
                vals.add(std::get<0>(m_instance.energy));
                // }

                // Link usage cost
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    vars.add(m_f[m_instance.graph.size() * i + e]);
                    vals.add(m_instance.demands[i].d / m_instance.graph.getEdgeWeight(edge) * std::get<1>(m_instance.energy));
                    for(const auto& x_y_j : m_pathOnEdge[e]) {
                        if(std::get<0>(x_y_j) != m_instance.demands[i].t 
                            && std::get<1>(x_y_j) != m_instance.demands[i].t) {
                            vars.add(m_p[getPIndex(std::get<0>(x_y_j), std::get<1>(x_y_j), std::get<2>(x_y_j), i)]);
                            vals.add(m_instance.demands[i].d / m_instance.graph.getEdgeWeight(edge) * std::get<1>(m_instance.energy));
                        }
                    }
                }

                // Node cost
                vars.add(m_r[e]);
                vals.add(std::get<3>(m_instance.energy));
                // vars.add(m_o[e]);
                // vals.add(std::get<3>(m_instance.energy) / 2);
                ++e;
            }
            return IloAdd(m_model, IloMinimize(m_env, (m_instance.graph.size() * std::get<2>(m_instance.energy)) + IloScalProd(vars, vals)));
        }()),
        m_constraints(m_env),
        m_flowConservationConstraints([&](){
            IloRangeArray flowConservationConstraints(m_env, m_instance.demands.size() * m_instance.graph.getOrder());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    IloNumVarArray vars(m_env);
                    IloNumArray vals(m_env);
                    for(const auto& v : m_instance.graph.getNeighbors(u)) {
                        if(u != m_instance.demands[i].t) {
                            vars.add(m_f[m_instance.graph.size() * i + m_instance.edgeId(u, v)]);
                            vals.add(1.0);
                        }
                        if(u != m_instance.demands[i].s) {
                            vars.add(m_f[m_instance.graph.size() * i + m_instance.edgeId(v, u)]);
                            vals.add(-1.0);
                        }
                    }
                    for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                        if(u != y) {
                            for(int j = 0; j < m_nbPath; ++j) {
                                if(y != m_instance.demands[i].t && u != m_instance.demands[i].t) {
                                    vars.add(m_p[getPIndex(u, y, j, i)]);
                                    vals.add(1.0);
                                // }
                                // if(u != m_instance.demands[i].t && ) {
                                    vars.add(m_p[getPIndex(y, u, j, i)]);
                                    vals.add(-1.0);
                                }
                            }
                        }
                    }
                    const int value = u == m_instance.demands[i].s ? 1 : (u == m_instance.demands[i].t ? -1 : 0);
                    m_constraints.add(flowConservationConstraints[m_instance.demands.size() * u + i] = IloRange(m_env, value, IloScalProd(vars, vals), value));
                    setIloName(flowConservationConstraints[m_instance.demands.size() * u + i], "m_flowConservationConstraints[" + std::to_string(u) + ", " + std::to_string(i) + "]");
                }
            }
            return IloAdd(m_model, flowConservationConstraints);
        }()),
        m_linkCapaConstraints([&](){
            IloRangeArray linkCapaConstraints(m_env, m_instance.graph.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                IloNumVarArray vars(m_env);
                IloNumArray vals(m_env);
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    vars.add(m_f[m_instance.graph.size() * i + e]);
                    vals.add(m_instance.demands[i].d);
                    for(const auto& x_y_j : m_pathOnEdge[e]) {
                        if(std::get<0>(x_y_j) != m_instance.demands[i].t 
                            && std::get<1>(x_y_j) != m_instance.demands[i].t) {
                            vars.add(m_p[getPIndex(std::get<0>(x_y_j), std::get<1>(x_y_j), std::get<2>(x_y_j), i)]);
                            vals.add(m_instance.demands[i].d);
                        }
                    }
                }
                vars.add(m_x[e]);
                vals.add(-m_instance.graph.getEdgeWeight(edge));

                m_constraints.add(linkCapaConstraints[e] = IloRange(m_env, -IloInfinity, IloScalProd(vars, vals), 0.0));
                setIloName(linkCapaConstraints[e], "linkCapaConstraints");
                ++e;
            }
            return IloAdd(m_model, linkCapaConstraints);
        }()),
        m_backupLimit1([&](){
            IloRangeArray backupLimit(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
                    if(u != t) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for(Graph::Node x = 0; x < m_instance.graph.getOrder(); ++x) {
                            if(t != x && u != x) {
                                vars.add(m_h[getHIndex(u, t, x)]);
                                vals.add(1.0);
                            }
                        }
                        m_constraints.add(backupLimit[m_instance.graph.getOrder() * u + t] = IloRange(m_env, 0.0, IloScalProd(vars, vals), 1.0));
                        setIloName(backupLimit[m_instance.graph.getOrder() * u + t], "m_backupLimit1");
                    }
                }
            }
            return IloAdd(m_model, backupLimit);
        }()),
        m_backupLimit2([&](){
            IloRangeArray backupLimit(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
                    if(u != t) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for(Graph::Node x = 0; x < m_instance.graph.getOrder(); ++x) {
                            if(t != x) {
                                vars.add(m_h[getHIndex(u, t, x)]);
                                vals.add(1.0);
                            }
                        }
                        //@TODO: Get demands per destination?
                        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                            if(m_instance.demands[i].t == t) {
                                vars.add(m_e[m_instance.graph.getOrder() * i + u]);
                                vals.add(-1.0);
                            }
                        }
                        m_constraints.add(backupLimit[m_instance.graph.getOrder() * u + t] = IloRange(m_env, -IloInfinity, IloScalProd(vars, vals), 0.0));
                        setIloName(backupLimit[m_instance.graph.getOrder() * u + t], "m_backupLimit2");
                    }
                }
            }
            return IloAdd(m_model, backupLimit);
        }()),
        m_pathUsage1([&](){
            IloRangeArray pathUsage(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_nbPath * m_instance.demands.size());
            for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                    if(y != z) {
                        int j = 0;
                        for(const auto& path : m_kShortestPaths(y, z)) {
                            IloNumVarArray vars(m_env);
                            IloNumArray vals(m_env);
                            for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                                vars.add(m_n[getNIndex(m_instance.edgeId(*iteU, *iteV), y, z)]);
                                vals.add(1.0);
                            }
                            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                                if(z != m_instance.demands[i].t && y != m_instance.demands[i].t) {
                                    m_constraints.add(pathUsage[getPIndex(y, z, j, i)] = 
                                        IloRange(m_env, 0.0, 
                                            IloScalProd(vars, vals) - IloNum(path.size()- 1) * m_p[getPIndex(y, z, j, i)], 
                                            IloInfinity));
                                    setIloName(pathUsage[getPIndex(y, z, j, i)], "m_pathUsage1");
                                }
                            }
                            ++j;
                        }
                    }
                }
            }
            return IloAdd(m_model, pathUsage);
        }()),
        m_pathUsage2([&](){
            IloRangeArray pathUsage(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_nbPath * m_instance.demands.size());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) { 
                    if(u != y) {
                        for(int j = 0; j < m_nbPath; ++j) {
                            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                                if(y != m_instance.demands[i].t && u != m_instance.demands[i].t) {
                                    m_constraints.add(pathUsage[getPIndex(u, y, j, i)] = IloRange(m_env, -IloInfinity, m_p[getPIndex(u, y, j, i)] - m_h[getHIndex(u, m_instance.demands[i].t, y)], 0.0));
                                    setIloName(pathUsage[getPIndex(u, y, j, i)], "m_pathUsage2");
                                }
                            }
                        }
                    }
                }
            }
            return IloAdd(m_model, pathUsage);
        }()),
        m_pathUsage3([&](){
            IloRangeArray pathUsage(m_env, m_instance.graph.getOrder() * m_instance.graph.getOrder() * m_nbPath * m_instance.demands.size());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) { 
                    if(u != y) {
                        for(int j = 0; j < m_nbPath; ++j) {
                            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                                if(y != m_instance.demands[i].t && u != m_instance.demands[i].t) {
                                    m_constraints.add(pathUsage[getPIndex(u, y, j, i)] = IloRange(m_env, -IloInfinity, m_p[getPIndex(u, y, j, i)] - m_e[m_instance.graph.getOrder() * i + u], 0.0));
                                    setIloName(pathUsage[getPIndex(u, y, j, i)], "m_pathUsage3");
                                }
                            }
                        }
                    }
                }
            }
            return IloAdd(m_model, pathUsage);
        }()),
        m_nextHopInactive([&](){
            IloRangeArray nextHopInactive(m_env, m_instance.demands.size() * m_instance.graph.size());
            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                int e = 0;
                for(const auto& edge : m_instance.graph.getEdges()) {
                    if(edge.first != m_instance.demands[i].t) {
                        m_constraints.add(nextHopInactive[m_instance.graph.size() * i + e] = 
                            IloRange(m_env, 0.0, 
                                m_e[m_instance.graph.getOrder() * i + edge.first] - m_n[getNIndex(e, m_instance.demands[i].s, m_instance.demands[i].t)] + m_x[e], 
                                IloInfinity));
                        setIloName(nextHopInactive[m_instance.graph.size() * i + e], "m_nextHopInactive"+toString(std::make_tuple(edge, i)));
                    }
                    ++e;
                }
            }
            return IloAdd(m_model, nextHopInactive);
        }()),
        m_nextHopActive([&](){
            IloRangeArray nextHopActive(m_env, m_instance.demands.size() * m_instance.graph.size());
            for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
                int e = 0;
                for(const auto& edge : m_instance.graph.getEdges()) {
                    if(edge.first != m_instance.demands[i].t) {
                        m_constraints.add(nextHopActive[m_instance.graph.size() * i + e] = 
                            IloRange(m_env, 0, 
                                m_e[m_instance.graph.getOrder() * i + edge.first] + m_n[getNIndex(e, m_instance.demands[i].s, m_instance.demands[i].t)] + m_x[e], 
                                2.0));
                        setIloName(nextHopActive[m_instance.graph.size() * i + e], "m_nextHopActive"+toString(std::make_tuple(edge, i)));
                    }
                    ++e;
                }
            }
            return IloAdd(m_model, nextHopActive);
            // return nextHopActive;
        }()),
        m_nextHopLimit([&](){
            IloRangeArray nextHopLimit(m_env, m_instance.demands.size() * m_instance.graph.getOrder());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    if(u != m_instance.demands[i].t) {
                        IloNumVarArray vars(m_env);
                        IloNumArray vals(m_env);
                        for(const auto& v : m_instance.graph.getNeighbors(u) ) {
                            vars.add(m_n[getNIndex(m_instance.edgeId(u, v), m_instance.demands[i].s, m_instance.demands[i].t)]);
                            vals.add(1.0);
                        }
                        m_constraints.add(nextHopLimit[m_instance.demands.size() * u + i] = IloRange(m_env, 1.0, IloScalProd(vars, vals), 1.0));
                        setIloName(nextHopLimit[m_instance.demands.size() * u + i], "m_nextHopLimit["+std::to_string(u)+", "+std::to_string(i)+"]");
                    }
                }
            }
            return IloAdd(m_model, nextHopLimit);
        }()),
        m_nextHopLimitOSPF([&](){
            IloRangeArray nextHopLimit(m_env, m_instance.demands.size() * m_instance.graph.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    if(edge.second != m_instance.nextHops(edge.first, m_instance.demands[i].t) && edge.first != m_instance.demands[i].t) {
                        m_constraints.add(nextHopLimit[m_instance.demands.size() * e + i] = IloRange(m_env, -IloInfinity, m_n[getNIndex(e, m_instance.demands[i].s, m_instance.demands[i].t)] - m_s[edge.first], 0.0));
                        setIloName(nextHopLimit[m_instance.demands.size() * e + i], "m_nextHopLimitOSPF");
                    }
                }
                ++e;
            }
            return IloAdd(m_model, nextHopLimit);
        }()),
        m_linkSDN([&](){
            IloRangeArray linkSDN(m_env, m_instance.graph.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                m_constraints.add(linkSDN[e] = IloRange(m_env, 0.0, m_x[e] - 1 + m_s[edge.first] + m_s[edge.second], IloInfinity));
                setIloName(linkSDN[e], "linkSDN");
                ++e;
            }
            return IloAdd(m_model, linkSDN);
        }()),
        m_equiLinks([&](){
            IloRangeArray equiLinks(m_env, m_instance.graph.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                m_constraints.add(equiLinks[e] = IloRange(m_env, 0.0, m_x[e] - m_x[m_instance.edgeId(edge.second, edge.first)], 0.0));
                ++e;
            }
            return IloAdd(m_model, equiLinks);
        }()),
        m_edgeUsage([&](){
            IloRangeArray edgeUsage(m_env, m_instance.graph.size() * m_instance.demands.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
                    if(edge.first != m_instance.demands[i].t) {
                        m_constraints.add(edgeUsage[m_instance.demands.size()* e + i] = 
                        IloRange(m_env, -IloInfinity, m_f[m_instance.graph.size() * i + e] - m_n[getNIndex(e, m_instance.demands[i].s, m_instance.demands[i].t)], 0.0));
                    } else {                        
                        m_constraints.add(edgeUsage[m_instance.demands.size()* e + i] = 
                        IloRange(m_env, 0.0, m_f[m_instance.graph.size() * i + e], 0.0));
                    }
                }
                ++e;
            }
            return IloAdd(m_model, edgeUsage);
        }()),
        m_activeRouter1([&](){
            IloRangeArray activeRouter(m_env, m_instance.graph.size());
            for(int e = 0; e < m_instance.graph.size(); ++e) {
                activeRouter[e] = IloRange(m_env, 0.0, m_r[e] - m_x[e], IloInfinity);
            }
            return IloAdd(m_model, activeRouter);
        }()),
        m_activeRouter2([&](){
            IloRangeArray activeRouter(m_env, m_instance.graph.size());
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                activeRouter[e] = IloRange(m_env, 1.0, m_r[e] + m_s[edge.first], IloInfinity);
                ++e;
            }
            return IloAdd(m_model, activeRouter);
        }()),
        m_nbSDN(IloAdd(m_model, IloRange(m_env, 0.0, IloSum(m_s), m_instance.nbSDN, "nbSDN"))),
        m_minNbLinks([&](){
            IloNumArray vals(m_env);
            IloNumVarArray vars(m_env);
            int e = 0;
            for(const auto& edge : m_instance.graph.getEdges()) {
                if(edge.first < edge.second) {
                    vars.add(m_x[e]);
                    vals.add(1.0);
                } 
                ++e;
            }
            return IloAdd(m_model, IloRange(m_env, m_instance.graph.getOrder()-1, IloScalProd(vars, vals), m_instance.graph.size()));
        }()),
        m_cut([&](){
            IloRangeArray cut(m_env, m_instance.graph.getOrder());
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                IloNumArray vals(m_env);
                IloNumVarArray vars(m_env);
                for(const auto& v : m_instance.graph.getNeighbors(u)) {
                    vars.add(m_x[m_instance.edgeId(u, v)]);
                    vals.add(1.0);
                }
                cut[u] = IloRange(m_env, 1.0, IloScalProd(vars, vals), IloInfinity);
            }
            return cut;
        }()),
        m_solver([&](){
            IloCplex solver(m_model);
            // #ifdef NDEBUG
            // solver.setOut(m_env.getNullStream());
            // #endif
            solver.setParam(IloCplex::Threads, 1);
            solver.setParam(IloCplex::TiLim, m_maxTime);
            // Set branching priorities
            for(int e = 0; e < m_instance.graph.size(); ++e) {
                solver.setPriority(m_x[e], 1.0);
            }
            
            for(int u = 0; u < m_instance.graph.getOrder(); ++u) {
                solver.setPriority(m_s[u], 2.0);
            }
            return solver;
        }())
    {
    }

    void addMIPStart(const HybridRouting& _hr) {
        IloNumVarArray vars(m_env);
        IloNumArray vals(m_env);
            // Fix SDN variable
        for(int i = 0; i < m_instance.graph.getOrder(); ++i) {   
            vars.add(m_s[i]);
            vals.add(_hr.isSDN(i) ? 1.0 : 0.0); 
        }

        // Fix edge activity
        int e = 0;
        for(const auto& edge : m_instance.graph.getEdges()) {
            vars.add(m_x[e]);
            vals.add(_hr.getFlowGraph().hasEdge(edge) ? 1.0 : 0.0);
            std::cout << m_x[e] << " -> " << (_hr.getFlowGraph().hasEdge(edge) ? 1.0 : 0.0) << '\n';
            vars.add(m_r[e]);
            vals.add(_hr.getFlowGraph().hasEdge(edge) || !_hr.isSDN(edge.first) ? 1.0 : 0.0);

            // vars.add(m_o[e]);
            // vals.add(_hr.getFlowGraph().hasEdge(edge) || !_hr.isSDN(edge.second) ? 1.0 : 0.0);
            ++e;
        }

        // Set all m_f to 0 
        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
            for(int e = 0; e < m_instance.graph.size(); ++e) {
                vars.add(m_f[m_instance.graph.size() * i + e]);
                vals.add(0.0);
            }
        }
         
        // Set all m_p to 0   
        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
            for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                    if(z != y) {
                        for(int j = 0; j < m_nbPath; ++j) {
                            if(z != m_instance.demands[i].t && y != m_instance.demands[i].t) {
                                vars.add(m_p[getPIndex(y, z, j, i)]);
                                vals.add(0.0);
                            }
                        }
                    }
                }
            }
        }
                
        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
            #if defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1 
            std::cout << m_instance.demands[i] << " => " << _hr.getPath(m_instance.demands[i].s, m_instance.demands[i].t) << '\n'; 
            #endif
            auto direct_tunnels = _hr.getDecomposedPath(m_instance.demands[i].s, m_instance.demands[i].t);
            for(const std::tuple<int, int, int, int>& tpl : direct_tunnels.first) {
                const auto u = std::get<0>(tpl), v = std::get<1>(tpl);
                if(m_instance.demands[i].s == std::get<2>(tpl) && m_instance.demands[i].t == std::get<3>(tpl)) {
                    vars.add( m_f[m_instance.graph.size() * i + m_instance.edgeId(u, v)] );
                    #if defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1 
                    std::cout << "f["<< std::make_tuple(i, u, v) << "]" << '\n'; 
                    #endif
                    vals.add(1.0);
                }
            }
            for(const auto& tpl : direct_tunnels.second) {
                const auto& bPath = std::get<0>(tpl);
                bool found = false;
                int j = 0;
                for(auto& path : m_kShortestPaths(bPath.front(), bPath.back())) {
                    if(path == bPath) {
                        vars.add( m_p[getPIndex(bPath.front(), bPath.back(), j, i)] ); //std::make_tuple(&path, std::get<1>(tpl), std::get<2>(tpl))] );
                        #ifndef NDEBUG
                        std::cout << "g["<< std::make_tuple(path, j, i) << "]" << '\n'; 
                        #endif
                        vals.add(1.0);
                        found = true;
                        break;
                    }
                    ++j;
                }    
                if(!found) {
                    std::cerr << "Backup path not found!" << bPath << " => " << m_kShortestPaths(bPath.front(), bPath.back()) << '\n';
                }
            }
        }

        for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                    if(u != t && t != y && u != y) {
                        vars.add( m_h[getHIndex(u, t, y)] );
                        if(_hr.isBackupEnd(u, t, y)) {
                            #if defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1 
                            std::cout << "h["<< std::make_tuple(u, t, y) << "]" << '\n'; 
                            #endif
                            vals.add(1.0);
                        } else {
                            vals.add(0.0);
                        }
                    }
                }
            }
        }
        m_solver.addMIPStart(vars, vals);

        IloConstraintArray consArr(m_env);
        IloNumArray numArr(m_env);
        for(IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asConstraint().getImpl()) {
                consArr.add((*iter).asConstraint());
                numArr.add(1);
            }
        }
        for(IloModel::Iterator iter(m_model); iter.ok(); ++iter) {
            if ((*iter).asVariable().getImpl()) {
                // consArr.add((*iter).asVariable());
                consArr.add(IloBound((*iter).asVariable(), IloBound::Lower));
                consArr.add(IloBound((*iter).asVariable(), IloBound::Upper));
                numArr.add(1);
                numArr.add(1);
            }
        }
        if(m_solver.refineMIPStartConflict(0, consArr, numArr)) {
            const auto conflict = m_solver.getConflict(consArr);
            m_env.getImpl()->useDetailedDisplay(IloTrue);
            for(int i = 0; i < int(consArr.getSize()); ++i) {
                if ( conflict[i] == IloCplex::ConflictMember) {
                    std::cout << "Proved  : " << consArr[i] << '\n';
                } else if ( conflict[i] == IloCplex::ConflictPossibleMember) {
                    std::cout << "Possible: " << consArr[i] << '\n';
                }
            }
            
        }
    }

    void freezeSolution(const HybridRouting& _hr) {
        // Fix SDN variable
        for(int i = 0; i < m_instance.graph.getOrder(); ++i) {   
            m_model.add(m_s[i] == (_hr.isSDN(i) ? 1.0 : 0.0));
        }

        // Fix edge activity
        int e = 0;
        for(const auto& arc : m_instance.graph.getEdges()) {
            m_model.add(m_x[e] == (_hr.getFlowGraph().hasEdge(arc) ? 1.0 : 0.0));
            ++e;
        }
                
        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
            #if defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1 
            std::cout << m_instance.demands[i] << " => " << _hr.getPath(m_instance.demands[i].s, m_instance.demands[i].t) << '\n'; 
            #endif
            auto direct_tunnels = _hr.getDecomposedPath(m_instance.demands[i].s, m_instance.demands[i].t);
            for(const std::tuple<int, int, int, int>& tpl : direct_tunnels.first) {
                const auto u = std::get<0>(tpl), v = std::get<1>(tpl);
                m_model.add( m_f[m_instance.graph.size() * i + m_instance.edgeId(u, v)] == 1.0);
                #if defined(NDEBUG) && defined(LOG_LEVEL) && LOG_LEVEL <= 1 
                std::cout << "f["<< tpl << "]" << '\n'; 
                #endif
            }
            for(const auto& tpl : direct_tunnels.second) {
                const auto& bPath = std::get<0>(tpl);
                bool found = false;
                int j = 0;
                for(auto& path : m_kShortestPaths(bPath.front(), bPath.back())) {
                    if(path == bPath) {
                        m_model.add( m_p[getPIndex(bPath.front(), bPath.back(), j, i)] == 1.0); //std::make_tuple(&path, std::get<1>(tpl), std::get<2>(tpl))] );
                        #ifndef NDEBUG
                        std::cout << "g["<< std::make_tuple(path, std::get<1>(tpl), std::get<2>(tpl)) << "]" << '\n'; 
                        #endif
                        found = true;
                        break;
                    }
                    ++j;
                }    
                if(!found) {
                    std::cerr << "Backup path not found!" << bPath << " => " << m_kShortestPaths(bPath.front(), bPath.back()) << '\n';
                }
            }
        }

        for(Graph::Node t = 0; t < m_instance.graph.getOrder(); ++t) {
            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                    if(u != t && t != y && u != y) {
                        m_model.add( m_h[getHIndex(u, t, y)] == (_hr.isBackupEnd(u, t, y) ? 1.0 : 0.0));
                    }
                }
            }
        }
    }

    Solution getSolution() const {
        // Get SDN Nodes
        std::vector<int> SDN(m_instance.graph.getOrder(), false);
        for(int u = 0; u < m_instance.graph.getOrder(); ++u) {
            if(m_solver.getValue(m_s[u])) {
                SDN[u] = true;
            }
        }

        // Get graph
        DiGraph flowGraph(m_instance.graph);
        int e = 0;
        for(const auto& edge : m_instance.graph.getEdges()) {
            if(!m_solver.getValue(m_x[e])) {
                flowGraph.removeEdge(edge);
            }
            ++e;
        }
        std::cout << flowGraph.size() << '/' << m_instance.graph.size() << '\n';

        // Get paths
        std::vector<std::tuple<Graph::Node, Graph::Node, Graph::Node>> backups;
        std::vector<Graph::Path> paths(m_instance.demands.size());

        for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
            std::cout << m_instance.demands[i] << '\n';
            Graph::Node u = m_instance.demands[i].s;
            while(u != m_instance.demands[i].t) {
                for(const auto& v : m_instance.graph.getNeighbors(u)) {
                    if(m_solver.getValue(m_f[m_instance.graph.size() * i + m_instance.edgeId(u, v)])) {
                        paths[i].emplace_back(u);
                        u = v;
                        break;
                    }
                }
                for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                    if(u != z) {
                        if(z != m_instance.demands[i].t && u != m_instance.demands[i].t) {
                            for(int j = 0; j < m_nbPath; ++j) {
                                if(m_solver.getValue(m_p[getPIndex(u, z, j, i)])) {
                                    for(const auto& node : m_kShortestPaths(u, z)[j]) {
                                        paths[i].emplace_back(node);
                                    }
                                    std::tuple<Graph::Node, Graph::Node, Graph::Node> backup(u, m_instance.demands[i].t, j);
                                    if(std::find(backups.begin(), backups.end(), backup) == backups.end()) {
                                        backups.push_back(backup);
                                    }
                                    u = paths[i].back();
                                    paths[i].pop_back();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            paths[i].emplace_back(u);
        }
        return Solution(m_solver.getObjValue(), SDN, flowGraph, paths, backups, m_solver.getMIPRelativeGap(), m_solver.getTime());
    }

    bool solve() {
        bool solved = m_solver.solve();
        std::cout << "Solution is " <<  m_solver.getStatus() << '\n';
        if(solved) {
            std::cout << "Objective value: "<< m_solver.getObjValue() << '\n';  
            for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) { // For each demand

                std::cout << "Demand: " << m_instance.demands[i] << " -> ";

                for(Graph::Node y = 0; y < m_instance.graph.getOrder(); ++y) {
                    for(Graph::Node z = 0; z < m_instance.graph.getOrder(); ++z) {
                        if(y != z && z != m_instance.demands[i].t && y != m_instance.demands[i].t) {
                            for(int j = 0; j < m_nbPath; ++j) {
                                if(m_solver.getValue(m_p[getPIndex(y, z, j, i)]) == 1) {
                                    std::cout << "Path between "<<y<<" and "<<z<<" for demand "<<i<<" : "<<m_kShortestPaths(y, z)[j]<<'\n';
                                    const Graph::Node nh = m_instance.nextHops(y, m_instance.demands[i].t);
                                    std::cout << "OSPF Next hop is " << nh << '\n';
                                    std::cout << "Neighbors of " << y << " -> " << m_instance.graph.getNeighbors(y) << '\n';
                                    std::cout << "Real next hop is ";
                                    Graph::Node rNh = -1;
                                    for(const auto& v : m_instance.graph.getNeighbors(y)) {
                                        if(m_solver.getValue(m_n[getNIndex(m_instance.edgeId(y, v), m_instance.demands[i].s, m_instance.demands[i].t)]) == 1) {
                                            assert([&](){
                                                if(rNh != -1) {
                                                    std::cout << "Two or more next hop\n";
                                                    displayConstraint(m_solver, m_nextHopLimit[m_instance.demands.size() * y + i]);
                                                    return false;
                                                }
                                                return true;
                                            }());
                                            rNh = v;
                                            std::cout << v << ' ';
                                        }
                                    }
                                    assert(rNh != -1);
                                    std::cout << "\nNext hop is on ? " << m_solver.getValue(m_x[m_instance.edgeId(y, rNh)]) << '\n';
                                    
                                    assert([&](){
                                        if(m_solver.getValue(m_e[m_instance.graph.getOrder() * i + y]) == m_solver.getValue(m_x[m_instance.edgeId(y, rNh)])) {
                                            std::cout << m_e[m_instance.graph.getOrder() * i + y] << " == " << m_x[m_instance.edgeId(y, rNh)] << '\n';
                                            for(const auto& v : m_instance.graph.getNeighbors(y)) {
                                                displayConstraint(m_solver, m_nextHopInactive[m_instance.graph.size() * i + m_instance.edgeId(y, v)]);
                                                displayConstraint(m_solver, m_nextHopActive[m_instance.graph.size() * i + m_instance.edgeId(y, v)]);
                                            }
                                            return false;
                                        }
                                        if(m_solver.getValue(m_x[m_instance.edgeId(y, rNh)]) == 1) {
                                            std::cout << m_x[m_instance.edgeId(y, rNh)] << '\n';
                                            for(const auto& v : m_instance.graph.getNeighbors(y)) {
                                                displayConstraint(m_solver, m_nextHopInactive[m_instance.graph.size() * i + m_instance.edgeId(y, v)]);
                                                displayConstraint(m_solver, m_nextHopActive[m_instance.graph.size() * i + m_instance.edgeId(y, v)]);
                                            }
                                            displayConstraint(m_solver, m_pathUsage2[getPIndex(y, z, j, i)]);
                                            return false;
                                        }
                                        return true;
                                    }());

                                    for(int i2 = 0; i2 < static_cast<int>(m_instance.demands.size()); ++i2) { // For each demand
                                        if(m_instance.demands[i].t == m_instance.demands[i2].t) {
                                            std::cout << m_instance.demands[i2] << "->" << m_solver.getValue(m_e[m_instance.graph.getOrder() * i2 + y]) << '\n';
                                        }
                                    }
                                    std::cout << "Backup from "<<y<<" to "<< m_instance.demands[i].t <<" is "<<z<<" ? " << m_solver.getValue(m_h[getHIndex(y, m_instance.demands[i].t, z)]) << '\n';
                                }
                            }
                        }
                    }
                }
                int e = 0;
                for(const auto& edge : m_instance.graph.getEdges()) { // For each edge
                    if(m_solver.getValue(m_f[m_instance.graph.size() * i + e]) > 1e-6) {
                        std::cout << edge << " ";
                        assert([&](){
                            if((m_instance.nextHops(edge.first, m_instance.demands[i].t) == edge.second || m_solver.getValue(m_s[edge.first])) 
                                    && m_solver.getValue(m_x[e])) {
                                return true;
                            } else {
                                if(!m_solver.getValue(m_x[e])) {
                                    std::cout << edge << " is off, "<< m_solver.getValue(m_x[m_instance.edgeId(edge.second, edge.first)]) << "\n";
                                    std::cout << m_equiLinks[e] << "\n";
                                }
                                std::cout << "Next hop is : " << m_instance.nextHops(edge.first, m_instance.demands[i].t) << '\n';
                                // std::cout << m_flowConservationConstraints[m_instance.demands.size() * edge.first + i] << '\n';
                                // std::cout << m_flowConservationConstraints[m_instance.demands.size() * edge.second + i] << '\n';
                                std::cout << m_edgeUsage[m_instance.demands.size()* e + i] << '\n';
                                if(edge.first != m_instance.demands[i].t) {
                                    std::cout << m_solver.getValue(m_n[getNIndex(e, m_instance.demands[i].s, m_instance.demands[i].t)]) << '\n';   
                                } else {
                                    std::cout << "isSDN"<< m_solver.getValue(m_s[edge.first]) << ")";   
                                }
                                return false;
                            }
                        }());//<< ", " << m_solver.getValue(m_s[edge.first]));
                    }
                    ++e;
                }
                std::cout << '\n';
            }

            for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
                if(m_solver.getValue(m_s[u])) {
                    std::cout << u << " is SDN\n";
                }
            }
        } else {
            m_solver.exportModel("path.lp");
            std::cout << '\n' << "No solution - starting Conflict refinement" << '\n';

            IloConstraintArray infeas(m_env);
            IloNumArray preferences(m_env);

            infeas.add(m_constraints);
            infeas.add(m_nbSDN);

            for(IloInt i = 0; i < infeas.getSize(); ++i) {
                preferences.add(1.0);  // user may wish to assign unique preferences
            }

            if(m_solver.refineConflict(infeas, preferences)) {
                IloCplex::ConflictStatusArray conflict = m_solver.getConflict(infeas);
                m_env.getImpl()->useDetailedDisplay(IloTrue);
                std::cout << "Conflict :" << '\n';
                for(IloInt i = 0; i < static_cast<int>(infeas.getSize()); ++i) {
                    if( conflict[i] == IloCplex::ConflictMember){
                        std::cout << "Proved  : " << infeas[i] << '\n';
                    } else if( conflict[i] == IloCplex::ConflictPossibleMember) {
                        std::cout << "Possible: " << infeas[i] << '\n';
                    }
                }
            } else {
                std::cout << "Conflict could not be refined\n\n";
            }

            std::cout << "Exporting to path.lp\n";
        }
        return solved;
    }

private:
    Instance m_instance;
    int m_nbPath;
    double m_maxTime;    
    Matrix< std::vector<Graph::Path> > m_kShortestPaths;
    std::vector<std::vector<std::tuple<int, int, int>>> m_pathOnEdge;

    IloEnv m_env;
    IloModel m_model;

    IloNumVarArray m_p;
    IloNumVarArray m_h;
    IloNumVarArray m_x;
    IloNumVarArray m_e;
    IloNumVarArray m_r;
    // IloNumVarArray m_o;
    IloNumVarArray m_f;
    IloNumVarArray m_n;
    IloNumVarArray m_s;

    IloObjective m_obj;

    IloConstraintArray m_constraints;

    IloRangeArray m_flowConservationConstraints;
    IloRangeArray m_linkCapaConstraints;
    IloRangeArray m_backupLimit1;
    IloRangeArray m_backupLimit2;
    IloRangeArray m_pathUsage1;
    IloRangeArray m_pathUsage2;
    IloRangeArray m_pathUsage3;
    IloRangeArray m_nextHopInactive;
    IloRangeArray m_nextHopActive;
    IloRangeArray m_nextHopLimit;
    IloRangeArray m_nextHopLimitOSPF;
    IloRangeArray m_linkSDN;
    IloRangeArray m_equiLinks;
    IloRangeArray m_edgeUsage;
    IloRangeArray m_activeRouter1;
    IloRangeArray m_activeRouter2;
    // IloRangeArray m_activeInputInterface1;
    // IloRangeArray m_activeInputInterface2;
    IloRange m_nbSDN;
    IloRange m_minNbLinks;
    IloRangeArray m_cut;

    IloCplex m_solver;

    int getPIndex(const Graph::Node _y, const Graph::Node _z, const int _j, const int _i) const {
        assert(0 <= _y && _y < m_instance.graph.getOrder());
        assert(0 <= _z && _z < m_instance.graph.getOrder());
        assert(0 <= _j && _j < m_nbPath);
        assert(0 <= _i && _i < static_cast<int>(m_instance.demands.size()));
        assert(_y != _z);
        assert(_z != m_instance.demands[_i].t);
        assert(_y != m_instance.demands[_i].t);
        return m_instance.demands.size() * m_nbPath * m_instance.graph.getOrder() * _y
            + m_instance.demands.size() * m_nbPath * _z
            + m_instance.demands.size() * _j
            +_i;
    }

    // int getPIndex(const int _id, const int _i) const {
    //     assert(0 <= _i && _i < static_cast<int>(m_instance.demands.size()));
    //     return m_instance.demands.size() * _id
    //         + _i;
    // }

    int getHIndex(const Graph::Node _u, const Graph::Node _t, const Graph::Node _y) const {
        assert(0 <= _u);
        assert(_u < m_instance.graph.getOrder());
        assert(0 <= _t);
        assert(_t < m_instance.graph.getOrder());
        assert(0 <= _y);
        assert(_y < m_instance.graph.getOrder());
        assert(_t != _y);
        return m_instance.graph.getOrder() * m_instance.graph.getOrder() * _u
            + m_instance.graph.getOrder() * _t
            + _y;
    }

    int getNIndex(const int _e, const Graph::Node _s, const Graph::Node _t) const {
        assert(0 <= _e);
        assert(_e < m_instance.graph.size());
        assert(0 <= _s);
        assert(_s < m_instance.graph.getOrder());
        assert(0 <= _t);
        assert(_t < m_instance.graph.getOrder());
        assert(m_instance.graph.getEdges()[_e].first != _t);
        return m_instance.graph.getOrder() * m_instance.graph.getOrder() * _e
            + m_instance.graph.getOrder() * _s
            + _t;
    }

};
}

#endif