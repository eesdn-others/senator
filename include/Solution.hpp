#ifndef SOLUTION_HPP
#define SOLUTION_HPP

#include <DiGraph.hpp>

namespace Hybrid {
class Solution {

public:
	Solution(const double _totalUsedEnergy, const std::vector<int>& _SDN, const DiGraph& _flowGraph, const std::vector<Graph::Path>& _paths, 
			const std::vector<std::tuple<Graph::Node, Graph::Node, Graph::Node>>& _backups,
			double _gap, double _time) :
		m_totalUsedEnergy(_totalUsedEnergy),
		m_SDN(_SDN),
		m_flowGraph(_flowGraph),
		m_paths(_paths),
		m_backups(_backups),
		m_gap(_gap),
		m_time(_time)
	{}

	void save(const std::string& _filename) const {
		std::cout << "Saved to " << _filename << '\n';
		std::ofstream ofs(_filename);
		ofs << m_totalUsedEnergy << '\n';
		ofs << m_gap << '\n';
		ofs << m_time << '\n';

		// Save SDN nodes
		bool first = true;
		for(int i = 0; i < static_cast<int>(m_SDN.size()); ++i) {
			if(m_SDN[i]) {
				if(!first) {
					ofs << '\t';
				} else {
					first = false;
				}
				ofs << i;
			}
		}
		ofs << '\n';

		// Save final topology
		ofs << m_flowGraph.size() << '\n';
		for(const auto& edge : m_flowGraph.getEdges()) {
			ofs << edge.first << '\t' << edge.second << '\t'  << m_flowGraph.getEdgeWeight(edge) << '\n';
		}

		// Save paths
		ofs << m_paths.size() << '\n';
		for(int i = 0; i < static_cast<int>(m_paths.size()); ++i) {
			ofs << m_paths[i].front() << '\t' << m_paths[i].back();
			for(const auto& u : m_paths[i]) {
				ofs << '\t' << u;
			}
			ofs << '\n';
		}

		// Save backups
		ofs << m_backups.size() << '\n';
		for(const auto& t : m_backups) {
			ofs << std::get<0>(t) << '\t' << std::get<1>(t) << '\t' << std::get<2>(t) << '\n';
		}
	}

	bool checkSolution(const Instance& _instance) const {
		bool valid = true;
		for(int i = 0; i < static_cast<int>(_instance.demands.size()); ++i) {
			// Check path is valid
			for(auto iteU = m_paths[i].begin(), iteV = std::next(iteU); iteV != m_paths[i].end(); ++iteU, ++iteV) {
				
			}
			
		}		
		return valid;
	} 

private:
	double m_totalUsedEnergy;
	std::vector<int> m_SDN;
	DiGraph m_flowGraph;
	std::vector<Graph::Path> m_paths;
	std::vector<std::tuple<Graph::Node, Graph::Node, Graph::Node>> m_backups;
	double m_gap;
	double m_time;

};
}
#endif