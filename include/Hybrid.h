#ifndef HYBRID
#define HYBRID

#include <DiGraph.hpp> 

namespace Hybrid {

struct Instance {
    Instance(const DiGraph& _graph, const std::tuple<double, double, double, double>& _energy, const Matrix<Graph::Node>& _nextHops, const std::vector<Demand>& _demands, const int _nbSDN) :
        graph(_graph),
        energy(_energy),
        nextHops(_nextHops),
        demands(_demands),
        edgeId([&](){
            Matrix<int> edgeId(graph.getOrder(), graph.getOrder(), -1);
            int i = 0;
            for(const Graph::Edge& edge : graph.getEdges()) {
                edgeId(edge.first, edge.second) = i;
                ++i;
            }
            return edgeId;
        }()),
        nbSDN(_nbSDN)
    {}

    DiGraph graph;
    std::tuple<double, double, double, double> energy;
    Matrix<Graph::Node> nextHops;
    std::vector<Demand> demands;
    Matrix<int> edgeId;
    int nbSDN;
};

struct FixedSDNInstance : public Instance {
    FixedSDNInstance(const DiGraph& _graph, const std::tuple<double, double, double, double>& _energy, const Matrix<Graph::Node>& _nextHops, 
            const std::vector<Demand>& _demands, const std::vector<Graph::Node>& _SDN) :
        Instance(_graph, _energy, _nextHops, _demands, std::accumulate(_SDN.begin(), _SDN.end(), 0.0)),
        SDN(_SDN)
    {}

    std::vector<Graph::Node> SDN;
};

}
#endif