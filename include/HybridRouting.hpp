#ifndef HYBRID_ROUTING_HPP
#define HYBRID_ROUTING_HPP


#include <DiGraph.hpp>
#include <ShortestPath.hpp>
#include <SNDlib.hpp>
#include <utility.hpp>

#include "Hybrid.h"

namespace Hybrid {
class HybridRouting {
	public:
		HybridRouting(const FixedSDNInstance& _instance) :
			m_instance(_instance),
			m_flowD([&](){
				DiGraph flowD(m_instance.graph.getOrder());
				for(const auto& edge : m_instance.graph.getEdges()) {
			    	m_flowD.addEdge(edge.first, edge.second, 0);
			    }
				return flowD;
			}()),
			m_flowOnEdge( m_instance.graph.size(), m_instance.demands.size() ),
			m_nbFlowOnEdge( m_instance.graph.size(), 0),
	 		m_paths( m_instance.demands.size() ),
	 		m_backup( m_instance.graph.getOrder(), m_instance.graph.getOrder(), std::make_tuple(-1, 0, Graph::Path() )),
	 		m_SDNRules( m_instance.graph.getOrder(), Matrix< std::pair<int, int> >(m_instance.graph.getOrder(), m_instance.graph.getOrder(), std::make_pair(-1, 0)) ),
	 		m_SDNBackupRules( m_instance.graph.getOrder(), Matrix< std::tuple<int, int, Graph::Path> >(m_instance.graph.getOrder(), m_instance.graph.getOrder(), std::make_tuple(-1, 0, Graph::Path() )) )
		{
		}

		virtual ~HybridRouting() {
		}

		// Return a list of SDN links
		std::vector<Graph::Edge> getSDNLinks() const {
			std::vector<Graph::Edge> edges = m_flowD.getEdges();
			edges.erase(std::remove_if(edges.begin(), 
					edges.end(), 
					[&](const Graph::Edge& _e){
						return !m_instance.SDN[_e.first] 
							|| (m_instance.SDN[_e.first] && m_instance.SDN[_e.second] && _e.first > _e.second);
					}), 
				edges.end());
			return edges;
		}

		int findNextHopSDN(const Graph::Node& _u, const Graph::Node& _s, const Graph::Node& _t) const {
			return std::get<0>(m_SDNRules[_u](_s, _t));
		}

		int findSDNBackup(const Graph::Node& _u, const Graph::Node& _s, const Graph::Node& _t) const {
			return std::get<0>(m_SDNBackupRules[_u](_s, _t));
		}

		/*
		* Return true if all edges in _path have a residual capacity >= d
		*/
		bool validPathForDemand(const Graph::Path& _path, double _d) const {
			if(_path.empty()) {
				return false;
			}
			for(auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end(); ++iteU, ++iteV) {
				if(m_instance.graph.getEdgeWeight(*iteU, *iteV) - m_flowD.getEdgeWeight(*iteU, *iteV) < _d || !m_flowD.hasEdge(*iteU, *iteV)) {
					return false;
				}
			}
			return true;
		}

		/*
		* Return the set of nodes that can reach _t without adding tunneling rules
		*/
		std::vector<int> getSources(const int _s, const int _t, const double _d) const {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "getSources" << std::make_tuple(_s, _t, _d) << '\n';
			#endif
			std::vector<int> sources;
			std::list<int> stack { _t };
			std::vector<int> done(m_instance.graph.getOrder(), false);
			std::vector<int> inStack(m_instance.graph.getOrder(), false);
			const auto vertices = m_instance.graph.getVertices();

			while( !stack.empty() ) {
				const auto v = stack.front(); 
				stack.pop_front();
				done[v] = 1;
				for(Graph::Node u = 0; u < m_instance.graph.getOrder(); ++u) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout<< Graph::Edge(u, v) << ": ";
					#endif
					if(done[u] != 1 && !inStack[u]) {
						if(m_flowD.hasEdge(u, v)) {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout<< "Direct" << '\n';
							#endif
							if(m_instance.SDN[u]) {
								if( std::get<0>(m_SDNBackupRules[u](_s, _t)) == -1 && (std::get<0>(m_SDNRules[u](_s, _t)) == -1 || std::get<0>(m_SDNRules[u](_s, _t)) == v) ) { // Or already set tunneling rule
									sources.push_back(u);
									stack.push_back(u);
									inStack[u] = true;
								}
							} else {							
								if( m_instance.nextHops(u, _t) == v ) {
									sources.push_back(u);
									stack.push_back(u);
									inStack[u] = true;
								}
							}
						} else {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout<< "Tunnel" << '\n';
							#endif
							if(m_instance.SDN[u]) { // Existing tunnels
								if(( std::get<0>(m_SDNBackupRules[u](_s, _t)) == v ) && validPathForDemand(std::get<2>(m_SDNBackupRules[u](_s, _t)), _d)) {
									sources.push_back(u);
									stack.push_back(u);
									inStack[u] = true;
								}
							} else {
								if(!m_flowD.hasEdge(u, m_instance.nextHops(u, _t)) && std::get<0>(m_backup(u, _t)) == v && validPathForDemand(std::get<2>(m_backup(u, _t)), _d)) {
									sources.push_back(u);
									stack.push_back(u);
									inStack[u] = true;
								}
							}
						}
					}
				}
			}
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "endgetSources" << '\n';
			#endif
			return sources;
		}

		/*
		* Return valid paths from _u to _t such that path from t to _t are valid
		*/
		std::vector< std::pair<int, Graph::Path> > getEndBackup(const int _u, const int _s, const int _t, const double _d) const {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "getEndBackup"<< std::make_tuple(_u, _s, _t, _d) << '\n';
			#endif
			auto sources = getSources(_s, _t, _d);

			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "Sources for "<< _t << ": " << sources << ": ";
			#endif
			std::vector< std::pair<int, Graph::Path> > endBackUp;
			for(auto& source : sources) {
				if(_u != source && !m_flowD.hasEdge(_u, source)) {
					// Look for valid path to source
					const auto path = ShortestPath<DiGraph>(m_flowD).getShortestPath(_u, source, 
						[&](int u, int v) {
							if(m_instance.graph.getEdgeWeight(u, v) - m_flowD.getEdgeWeight(u, v) >= _d) {
								if(m_instance.SDN[u]) {
									if(u == _u && m_instance.SDN[v]) {
										return false;
									}
									return (std::get<0>(m_SDNRules[u](_u, source)) == -1 || std::get<0>(m_SDNRules[u](_u, source)) == v);
								} else {
									return m_instance.nextHops(u, source) == v;
								}
							} else {
								return false;
							}
						},
						[&](int u, int v) {
							return m_flowD.getEdgeWeight(u, v) / m_instance.graph.getEdgeWeight(u, v);
						}
					);
					assert(path.size() != 1);
					if(path.size() > 1 && !(m_instance.SDN[*std::next(path.begin())] && m_instance.SDN[*path.begin()])) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< source << ", ";
						#endif
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< path << '\n';
						#endif
						// assert(!(m_instance.SDN[*std::next(path.begin())] && m_instance.SDN[*path.begin()]));
						endBackUp.emplace_back(source, path);					
					}
				}
			}
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< '\n';
			#endif
			return endBackUp;
		}

		virtual std::vector< std::tuple<int, double, Graph::Path> > getNextHops(const int _u, const int _s, const int _t, const double _d) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "getNextHops" << std::make_tuple(_u, _s, _t, _d) << '\n';
			#endif
			std::vector< std::tuple<int, double, Graph::Path> > nextHops;

			if(m_instance.SDN[_u]) {
	        	int nRules = std::get<0>(m_SDNRules[_u](_s, _t));
	        	if(nRules != -1) { // Already present SDN rules for (s, t)
	        		if(m_flowD.hasEdge(_u, nRules)) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << "Direct SDN (rules)" << '\n';
	        			#endif

						if(m_instance.graph.getEdgeWeight(_u, nRules) - m_flowD.getEdgeWeight(_u, nRules) >= _d) {
			        		nextHops.emplace_back(nRules, 
			        			1 + m_flowD.getEdgeWeight(_u, nRules) / m_instance.graph.getEdgeWeight(_u, nRules),
			        			Graph::Path());
		        		}
		        	} else if(std::get<0>(m_backup(_u, _t)) != -1) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		        		std::cout << "OSPF encaps already here ";
		        		#endif

	        			if(!std::get<2>(m_backup(_u, _t)).empty() && !hasValidRules(std::get<2>(m_backup(_u, _t)))) {
		        			std::get<2>(m_backup(_u, _t)).clear();	
		        		}
	        			if(!std::get<2>(m_backup(_u, _t)).empty()) {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			        		std::cout << "with fixed path " << '\n';
	        				#endif

				        	if( validPathForDemand(std::get<2>(m_backup(_u, _t)), _d)) {
				        		auto end = std::get<0>(m_backup(_u, _t));
				    	    		nextHops.emplace_back(end, 
				        				2 * std::get<2>(m_backup(_u, _t)).size(), 
					        			std::get<2>(m_backup(_u, _t)));
					        }
			        	} else {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			        		std::cout << "without fixed path " << '\n';
	        				#endif
	        				// Thus, search for a new tunnel
			        		if(std::get<2>(m_backup(_u, _t)).empty()) {
			        			for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
			        				if(n_Path.first == std::get<0>(m_backup(_u, _t))) { // Only get path with right backup path
					        			nextHops.emplace_back(n_Path.first,
					        				2 * n_Path.second.size(),
					        				n_Path.second);
				        			}
				        		}
		        			}
	        			}
	        		} else {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		        		std::cout<< "Search OSPF encaps" << '\n';
	        			#endif
		        		for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
		        			nextHops.emplace_back(n_Path.first,
		        				2 * n_Path.second.size(),
		        				n_Path.second);
		        		}
		        	}
	        	} else {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
	        		std::cout << "Search direct SDN (no rules):";
        			#endif
					for(auto& n : m_flowD.getNeighbors(_u)) {
						if(m_flowD.hasEdge(_u, n) && m_instance.graph.getEdgeWeight(_u, n) - m_flowD.getEdgeWeight(_u, n) >= _d) {
			        		nextHops.emplace_back(n, 
			        			1 + m_flowD.getEdgeWeight(_u, n) / m_instance.graph.getEdgeWeight(_u, n),
			        			Graph::Path());
		        		}
	        		};
	        	}
	        } else {
	        	if(m_flowD.hasEdge(_u, m_instance.nextHops(_u, _t))) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
	        		std::cout<< "Direct OSPF" << '\n';
	        		#endif
	        		int n = m_instance.nextHops(_u, _t);
	        		if( m_instance.graph.getEdgeWeight(_u, n) - m_flowD.getEdgeWeight(_u, n) >= _d ) {
		        		nextHops.emplace_back(n, 
		        			1 + m_flowD.getEdgeWeight(_u, n) / m_instance.graph.getEdgeWeight(_u, n),
		        			Graph::Path());
	        		}
	        	} else if(std::get<0>(m_backup(_u, _t)) != -1) {
	        		// std::cout << "m_backup("<<_u<<", "<<_t<<"): " << m_backup(_u, _t) << '\n';
        			if(!std::get<2>(m_backup(_u, _t)).empty() && !hasValidRules(std::get<2>(m_backup(_u, _t)))) {
	        			std::get<2>(m_backup(_u, _t)).clear();	
	        		}
        			if(!std::get<2>(m_backup(_u, _t)).empty()) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
	        			std::cout << "with fixed path " << '\n';
        				#endif	
			        	if( validPathForDemand(std::get<2>(m_backup(_u, _t)), _d)) {
			        		auto end = std::get<0>(m_backup(_u, _t));
			    	    		nextHops.emplace_back(end, 
			        				2 * std::get<2>(m_backup(_u, _t)).size(), 
				        			std::get<2>(m_backup(_u, _t)));
				        }
		        	} else {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		        		std::cout << "without fixed path " << '\n';
	        			#endif
		        		if(std::get<2>(m_backup(_u, _t)).empty()) {
		        			for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
		        				if(n_Path.first == std::get<0>(m_backup(_u, _t))) { // Only get path with right backup path
				        			nextHops.emplace_back(n_Path.first,
				        				2 * n_Path.second.size(),
				        				n_Path.second);
			        			}
			        		}
	        			}
        			}
        		} else {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
	        		std::cout<< "Search OSPF encaps" << '\n';
	        		#endif
	        		for(auto& n_Path : getEndBackup(_u, _s, _t, _d)) {
	        			nextHops.emplace_back(n_Path.first,
	        				2 * n_Path.second.size(),
	        				n_Path.second);
	        		}
	        	}
	        }
	        return nextHops;
		}	

		virtual Graph::Path getRealPath(const int _s, const int _t, const double _d, const int _maxLength = std::numeric_limits<int>::max()) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< '\n' << "getRealPath" << std::make_tuple(_s, _t, _d) << '\n';
    		#endif
			// struct Comparator {
	  //           std::vector<double>& distance;

	  //           Comparator(std::vector<double>& _distance) : distance(_distance) {}


	  //           bool operator()(const Graph::Node& u, const Graph::Node& v) const {
	  //               return distance[u] < distance[v];
	  //           }

	  //           // Copy
	  //           Comparator(const Comparator& _other) : distance(_other.distance) {};
	  //           Comparator& operator=(const Comparator& _other) {
	  //               distance = _other.distance;
	  //               return *this;
	  //           };
	  //           // Move
	  //           Comparator(Comparator&& _other) : distance(_other.distance) {};
	  //           Comparator& operator=(Comparator&& _other) {
	  //               distance = _other.distance;
	  //               return *this;
	  //           };
	  //       };

		    std::vector<double> distance(m_instance.graph.getOrder(), std::numeric_limits<int>::max());
		    std::vector<double> nbHops(m_instance.graph.getOrder(), std::numeric_limits<int>::max());
		    // Comparator comp(distance);
		    std::vector<int> parent(m_instance.graph.getOrder(), -1);

		    std::vector<short> color(m_instance.graph.getOrder(), 0);
		    std::vector<Graph::Path> paths(m_instance.graph.getOrder());

		    std::vector< typename BinaryHeap<int, std::function<bool(int, int)>>::Handle* > handles(m_instance.graph.getOrder());
		    BinaryHeap<int, std::function<bool(int, int)>> heap(m_instance.graph.getOrder(), 
		    	[&](const Graph::Node& u, const Graph::Node& v){
		    		return distance[u] < distance[v];
		    	}
		    );

		    parent[_s] = _s;
		    distance[_s] = 0;
		    nbHops[_s] = 0;
		    handles[_s] = heap.push(_s);
		    color[_s] = 1;
		    
		    while (_t != heap.top()) {
				#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		    	std::cout << "Parents: " << parent << '\n';
		    	std::cout << "distance: " << distance << '\n';
		    	#endif
		        if(heap.empty()) {
		            return Graph::Path();
		        }
		        auto s = heap.top(); heap.pop();
		        color[s] = 2;
		        
		        std::vector< std::tuple<int, double, Graph::Path> > nextHops = getNextHops(s, _s, _t, _d);
				#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		        std::cout<< "nextHops: " << nextHops << '\n';
		        #endif
		        for (const auto& node_Weight_Path : nextHops) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		        	std::cout << "Next hop: " << node_Weight_Path << '\n';
	        		#endif
		        	const auto t = std::get<0>(node_Weight_Path);
		        	assert(t != -1);
		        	const auto weight = std::get<1>(node_Weight_Path);
		        	const auto path = std::get<2>(node_Weight_Path);

		            if(color[t] != 2) {                
		                auto distT = distance[s] + weight;
		                auto nHopT = path.empty() ? nbHops[s] + 1 : nbHops[s] + path.size() - 1;
		                if ( distT < distance[t] && nHopT <= _maxLength ) {
		                    distance[t] = distT;
		                    nbHops[t] = nHopT;
		                    paths[t] = path;
		                    #if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		                    std::cout<< "Inter path for " << Demand(_s, _t, _d) << ", " << t << "=>" << paths[t] << '\n';
		                    #endif
		                    if(color[t] == 1) {
		                        heap.decrease(handles[t]);
		                    } else {
		                        handles[t] = heap.push(t);
		                        color[t] = 1;
		                    }
		                    parent[t] = s;
		                }
		            }
		        }
		    }

			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		    std::cout<< "Path found, buildin it" << '\n';
    		#endif
		    Graph::Path finalPath;
		    int node = _t;
		    while ( node != _s ) {
		    	auto parentNode = parent[node];
		    	if(paths[node].empty()) {
		    		// Direct path
		    		if(m_instance.SDN[parentNode]) {
		    			// Add rule
		    			addRule(parentNode, _s, _t, node);
		    		}
		    		finalPath.push_back(node);
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		    		std::cout<< "(Direct path) Push " << node << '\n';
	        		#endif	 
		    	} else {
		    		// Not direct path
		   //  		if(m_instance.SDN[parentNode]) {
					// #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		   //  			std::cout<< "(SDN Encaps path): " << '\n';
		   //  		#endif
		   //  			addBackupRule(parentNode, _s, _t, node, paths[node]);
		   //  			for(auto iteV = paths[node].end(), iteU = std::prev(iteV); iteV != paths[node].begin(); --iteU, --iteV) {
		   //  				if(m_instance.SDN[*iteU]) {
		   //  					addRule(*iteU, paths[node].front(), paths[node].back(), *iteV);
		   //  				}
		   //  				if(iteU != paths[node].begin()) {
		   //  					finalPath.push_back(*iteU);
					// #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		   //  					std::cout<< "Push " << *iteU << ", ";	 
		   //  		#endif
		   //  				}
		   //  			}
					// #if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		   //  			std::cout<< '\n';
		   //  		#endif
		   //  		} else {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
		    		std::cout<< "(OSPF Encaps path): " << paths[node] << '\n';
		    		#endif
	    			addBackup(parentNode, _t, node, paths[node]);
	    			//@CLARIFY Why reverse ? [Because we're building the path from finish to start]
	    			for(auto iteV = paths[node].rbegin(), iteU = std::next(iteV); iteU != paths[node].rend(); ++iteU, ++iteV) {
	    				#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			    		std::cout<< "(OSPF Encaps path): " << Graph::Edge(*iteU, *iteV) << '\n';
			    		#endif
	    				if(m_instance.SDN[*iteU]) {
	    					addRule(*iteU, paths[node].front(), paths[node].back(), *iteV);
	    				}
	    				if(iteU != paths[node].rend()) {
	    					finalPath.push_back(*iteV);
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 1	
	    					std::cout<< "(OSPF Encaps path) Push " << *iteV << '\n';
	    					#endif
	    				}
	    			}
		    		// }
		    	}
		    	node = parentNode;	
		    }
		    finalPath.push_back(_s);
		    std::reverse(finalPath.begin(), finalPath.end());
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout << "Push " << node << '\n';	 
			#endif
		    return finalPath;
		}

		void assignPath(const int _demandID, const Graph::Path& _path) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "assignPath: " << m_instance.demands[_demandID] << " => " << _path << '\n';
			#endif
			assert(!_path.empty());
			for(auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end(); ++iteU, ++iteV) {
				// assert(std::find(m_flowOnEdge(*iteU, *iteV).begin(), m_flowOnEdge(*iteU, *iteV).end(), _demand) == m_flowOnEdge(*iteU, *iteV).end());
				m_flowOnEdge(m_instance.edgeId(*iteU, *iteV), _demandID) = 1;
				m_flowD.setEdgeWeight(*iteU, *iteV, m_instance.demands[_demandID].d + m_flowD.getEdgeWeight(*iteU, *iteV));
				assert(m_flowD.getEdgeWeight(*iteU, *iteV) <= m_instance.graph.getEdgeWeight(*iteU, *iteV));
			}
			m_paths[_demandID] = _path;
		}

		std::vector<int> shutdownLink(const int _u, const int _v) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "shutdownLink" << std::make_tuple(_u, _v) << '\n';
			std::cout<< "m_flowOnEdge " << std::make_tuple(_u, _v) << ": " << m_flowOnEdge(_u, _v) << '\n';
			std::cout<< "m_flowOnEdge " << std::make_tuple(_v, _u) << ": " << m_flowOnEdge(_v, _u) << '\n';
			#endif

			std::vector<int> demandsToRemove(m_instance.demands.size());
			for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
				if(m_flowOnEdge(m_instance.edgeId(_u, _v), i) || m_flowOnEdge(m_instance.edgeId(_v, _u), i)) {
					demandsToRemove[i] = 1;
				}
			}
			
			std::vector<int> removedDemands;
			for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
				if(demandsToRemove[i]) {
					removeDemand(i);
					removedDemands.push_back(i);
				}
			}
			m_flowD.removeEdge(_u, _v);
			m_flowD.removeEdge(_v, _u);
			return removedDemands;
		}

		virtual void addRule(const int _u, const int _s, const int _t, const int _nh) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< "addRule" << std::make_tuple(_u, _s, _t, _nh) << '\n';
			#endif
			assert(m_instance.SDN[_u]);
			assert(m_SDNRules[_u](_s, _t).first == _nh || m_SDNRules[_u](_s, _t).first == -1);
			assert(0 <= _u && _u < m_instance.graph.getOrder());
			assert(0 <= _s && _s < m_instance.graph.getOrder());
			assert(0 <= _t && _t < m_instance.graph.getOrder());
			assert(0 <= _nh && _nh < m_instance.graph.getOrder());
			m_SDNRules[_u](_s, _t).first = _nh;
			m_SDNRules[_u](_s, _t).second++;
		}

		virtual void removeRule(const int _u, const int _s, const int _t) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< "removeRule" << std::make_tuple(_u, _s, _t) << '\n';
			#endif
			assert(0 <= _u && _u < m_instance.graph.getOrder());
			assert(0 <= _s && _s < m_instance.graph.getOrder());
			assert(0 <= _t && _t < m_instance.graph.getOrder());
			assert(m_instance.SDN[_u]);
			assert(m_SDNRules[_u](_s, _t).second > 0);
			m_SDNRules[_u](_s, _t).second--;
			if(m_SDNRules[_u](_s, _t).second == 0) {
				m_SDNRules[_u](_s, _t).first = -1;
			}
		}

		virtual void addBackupRule(const int _u, const int _s, const int _t, const int _nh, const Graph::Path& _path) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< "addBackupRule" << std::make_tuple(_u, _s, _t, _nh, _path) << ": ";
			#endif
			assert(m_instance.SDN[_u]);
			assert((std::get<0>(m_SDNBackupRules[_u](_s, _t)) == _nh && std::get<2>(m_SDNBackupRules[_u](_s, _t)) == _path) || std::get<0>(m_SDNBackupRules[_u](_s, _t)) == -1);
			assert(!m_instance.SDN[*std::next(_path.begin())]);
			assert(!_path.empty());
			std::get<0>(m_SDNBackupRules[_u](_s, _t)) = _nh;
			std::get<1>(m_SDNBackupRules[_u](_s, _t))++;
			std::get<2>(m_SDNBackupRules[_u](_s, _t)) = _path;
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< m_SDNBackupRules[_u](_s, _t) << '\n';
			#endif
		}

		virtual void removeBackupRule(const int _u, const int _s, const int _t) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< "removeBackupRule" << std::make_tuple(_u, _s, _t) << ": ";
			#endif
			assert(m_instance.SDN[_u]);
			assert(std::get<1>(m_SDNBackupRules[_u](_s, _t)) > 0);
			std::get<1>(m_SDNBackupRules[_u](_s, _t))--;
			if(std::get<1>(m_SDNBackupRules[_u](_s, _t)) == 0) {
				std::get<0>(m_SDNBackupRules[_u](_s, _t)) = -1;
				std::get<2>(m_SDNBackupRules[_u](_s, _t)).clear();
			}
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 1
			std::cout<< m_SDNBackupRules[_u](_s, _t) << '\n';
			#endif
		}

		void removeFlowOnEdge(const int _u, const int _v, const int _demandID) {
			m_flowOnEdge(m_instance.edgeId(_u, _v), _demandID) = 0;	
			--m_nbFlowOnEdge[m_instance.edgeId(_u, _v)];
		}

		virtual void addBackup(const int _u, const int _t, const int _nh, const Graph::Path& _path) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "addBackup" << std::make_tuple(_u, _t, _nh, _path) << '\n';
			#endif
			// assert(!m_instance.SDN[_u]);
			assert(!_path.empty());
			assert((std::get<0>(m_backup(_u, _t)) == _nh && std::get<2>(m_backup(_u, _t)) == _path) || std::get<0>(m_backup(_u, _t)) == -1);
			assert(_nh >= 0 && _nh < m_instance.graph.getOrder());

			std::get<0>(m_backup(_u, _t)) = _nh;
			std::get<1>(m_backup(_u, _t))++;
			std::get<2>(m_backup(_u, _t)) = _path;
		}

		virtual void removeBackup(const int _u, const int _t) {
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< "removeBackup" << std::make_tuple(_u, _t) << '\n';
			#endif
			// assert(!m_instance.SDN[_u]);
			assert(std::get<1>(m_backup(_u, _t)) > 0);
			--std::get<1>(m_backup(_u, _t));
			if(std::get<1>(m_backup(_u, _t)) == 0) {
				std::get<0>(m_backup(_u, _t)) = -1;
				std::get<2>(m_backup(_u, _t)).clear();
			}
		}
		/* 
		* 
		*/
		void removeDemand(const int _demandID) {		
			const Graph::Path& path = m_paths[_demandID];
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout<< '\n' << "Removing " << m_instance.demands[_demandID] << ' ' << path << '\n';
			#endif
			assert(!path.empty());

 			for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
				std::cout<< Graph::Edge(*iteU, *iteV) << ": " ;
				#endif
				if(m_instance.SDN[*iteU]) {
					if(m_SDNRules[*iteU](m_instance.demands[_demandID].s, m_instance.demands[_demandID].t).first == *iteV) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< "Direct SDN" << '\n';
						#endif
						removeRule(*iteU, m_instance.demands[_demandID].s, m_instance.demands[_demandID].t);

						removeFlowOnEdge(*iteU, *iteV, _demandID);
						m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
					} else {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< "Encaps SDN" << '\n';
						#endif
						auto subPath = std::get<2>(m_backup(*iteU, m_instance.demands[_demandID].t));		
						removeBackup(*iteU, m_instance.demands[_demandID].t);

						assert(!subPath.empty());
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< subPath << '\n';
						#endif

						for(; *iteV != subPath.back(); ++iteU, ++iteV) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout<< Graph::Edge(*iteU, *iteV) << '\n';
							#endif
							assert(std::get<0>(m_SDNRules[*iteU](subPath.front(), subPath.back())) == *iteV || m_instance.nextHops(*iteU, subPath.back()) == *iteV);
							if(m_instance.SDN[*iteU]) {
								removeRule(*iteU, subPath.front(), subPath.back());
							}
							removeFlowOnEdge(*iteU, *iteV, _demandID);
							m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
						}
						removeFlowOnEdge(*iteU, *iteV, _demandID);
						m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
					}
				} else {
					if(m_instance.nextHops(*iteU, m_instance.demands[_demandID].t) == *iteV) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< "Direct OSPF" << '\n';
						#endif

						removeFlowOnEdge(*iteU, *iteV, _demandID);
						m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
					} else {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< "Encaps OSPF" << '\n';
						#endif
						assert(std::get<0>(m_backup(*iteU, m_instance.demands[_demandID].t)) != -1);

						auto subPath = std::get<2>(m_backup(*iteU, m_instance.demands[_demandID].t));		
						removeBackup(*iteU, m_instance.demands[_demandID].t);

						// ++iteU, ++iteV;


						assert(!subPath.empty());
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout<< subPath << '\n';
						#endif

						for(; *iteV != subPath.back(); ++iteU, ++iteV) {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout<< Graph::Edge(*iteU, *iteV) << '\n';
							#endif
							assert(std::get<0>(m_SDNRules[*iteU](subPath.front(), subPath.back())) == *iteV || m_instance.nextHops(*iteU, subPath.back()) == *iteV);
							if(m_instance.SDN[*iteU]) {
								removeRule(*iteU, subPath.front(), subPath.back());
							}
							removeFlowOnEdge(*iteU, *iteV, _demandID);
							m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
						}
						removeFlowOnEdge(*iteU, *iteV, _demandID);
						m_flowD.setEdgeWeight(*iteU, *iteV, m_flowD.getEdgeWeight(*iteU, *iteV) - m_instance.demands[_demandID].d);
					}
				}
			}
			m_paths[_demandID].clear();
		}

		const Matrix< std::tuple<int, int, Graph::Path> >& getBackup() const {
			return m_backup; 
		}

		bool routeDemands(const std::vector<int>& _demandIDs) {
			// Find a path for each demand
		 	for(const auto& demandID : _demandIDs) {
				#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		 		std::cout<< "Routing " << m_instance.demands[demandID] << '\n';
		 		#endif

				auto path = this->getRealPath(m_instance.demands[demandID].s, m_instance.demands[demandID].t, m_instance.demands[demandID].d);
				#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
				std::cout<< "Find path for " << m_instance.demands[demandID] << ": " << path << '\n';
				#endif

				if ( path.empty() ) {
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout<< "No path found for"<<m_instance.demands[demandID]<<"\n";
					#endif
					return false;
				}
				
				this->assignPath(demandID, path);
				assert(!m_paths[demandID].empty());
		 	}
		 	return true;
		}

		// bool routeDemandsMaxStretch(const std::vector<Demand>& demands, const double _maxStretch) {
		// 	// Find a path for each demand
		//  	for(auto& demand : demands) {
		// 		#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		//  		std::cout<< "Routing " << demand << '\n';
		//  		#endif
		//  		int maxLength = static_cast<int>((ShortestPath<DiGraph>(m_instance.graph).getShortestPathNbArcs(demand.s, demand.t).size()-1) * _maxStretch) ;

		// 		const auto path = this->getRealPath(demand.s, demand.t, demand.d, maxLength);
		// 		#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		// 		std::cout<< "Find path for " << demand << ": " << path << '\n';
		// 		#endif

		// 		if ( path.empty() ) {
		// 			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		// 			std::cout<< "No path found for demand between " << demand.s << " and " << demand.t <<" ("<<demand.d<<')'<< '\n';
		// 			#endif
		// 			return false;
		// 		}
				
		// 		this->assignPath(demand, path);
		// 		assert(!m_paths(demand.s, demand.t).empty());
		//  	}
		//  	return true;
		// }

		// bool routeDemandsMaxStretch(const std::vector<Demand>& demands, const int _maxStretch) {
		// 	// Find a path for each demand
		//  	for(auto& demand : demands) {
		// 		#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		//  		std::cout<< "Routing " << demand << '\n';
		//  		#endif
		//  		int maxLength = (ShortestPath<DiGraph>(m_instance.graph).getShortestPathNbArcs(demand.s, demand.t).size() - 1 + _maxStretch) ;

		// 		auto path = this->getRealPath(demand.s, demand.t, demand.d, maxLength);
		// 		#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		// 		std::cout<< "Find path for " << demand << ": " << path << '\n';
		// 		#endif

		// 		if ( path.empty() ) {
		// 			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
		// 			std::cout<< "No path found for demand between " << demand.s << " and " << demand.t <<" ("<<demand.d<<')'<< '\n';
		// 			#endif
		// 			return false;
		// 		}
				
		// 		this->assignPath(demand, path);
		// 		assert(!m_paths(demand.s, demand.t).empty());
		//  	}
		//  	return true;
		// }

		int getActiveLinks() const {
			return m_flowD.size();
		}

		const DiGraph& getFlowGraph() const {
			return m_flowD;
		}

		double getPower() const {
			double power = 0;
			for(const auto& edge : m_instance.graph.getEdges()) {
				power += std::get<2>(m_instance.energy);
				// If link is off, count switch if they're not SDN
				if(!m_flowD.hasEdge(edge)) {
					if(!m_instance.SDN[edge.first]) {
						power += std::get<3>(m_instance.energy);
					}
				} else {
					power += std::get<0>(m_instance.energy) 
						+ std::get<1>(m_instance.energy) * m_flowD.getEdgeWeight(edge) / m_instance.graph.getEdgeWeight(edge)
						+ std::get<3>(m_instance.energy);
				}
			}
			return power;
		}

		bool checkPath() const {
			for(int i = 0 ; i < static_cast<int>(m_instance.demands.size()); ++i) {
				const auto path = m_paths[i];
				for(auto iteU = m_paths[i].begin(), iteV = std::next(iteU); iteV != m_paths[i].end(); ++iteU, ++iteV) {
					if( !m_flowD.hasEdge(*iteU, *iteV) ) {
						std::cerr << "Path between " << m_paths[i].front() << " and " << m_paths[i].back() << " is not valid " << Graph::Edge(*iteU,*iteV) << "\t" << m_paths[i] << '\n';
						return false;
					}
				}
			}
			return true;
		}

		bool checkSolution() const {
			bool valid = true;

			for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
				if(m_paths[i].empty()) {
					return false;
				}
				const Graph::Node s = m_paths[i].front(), 
					t = m_paths[i].back();
				#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
				std::cout<< "Checking " << Graph::Edge(s, t) << '\n';
				#endif
				Graph::Path path;
				std::list<std::pair<int, int>> srcDst { std::make_pair(s, t) };
				int u = s;
				while(!srcDst.empty()) {
					int source = srcDst.front().first, destination = srcDst.front().second;

					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout << "path: " << path << ", header stack: "<< srcDst << '\n';
					#endif
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout << u << " ";
					#endif
					if(m_instance.SDN[u]) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << "SDN Encaps: " << std::get<0>(m_backup(u, destination)) << ", Direct: " << m_SDNRules[u](s, t).first << '\n';
						#endif
					} else {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << "OSPF Encaps: " << std::get<0>(m_backup(u, destination)) << ", Direct: " << m_instance.nextHops(u, destination) << '\n';
						#endif
					}

					if(m_instance.SDN[u]) {
						if(m_flowD.hasEdge(u, m_SDNRules[u](source, destination).first)) {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout << "SDN direct" << '\n';
							#endif
							path.emplace_back(u);
							u = m_SDNRules[u](source, destination).first;
						} else {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout<< "OSPF tunnel" << '\n';
							#endif
							srcDst.emplace_front(u, std::get<0>(m_backup(u, destination)));
						}
					} else {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << "m_instance.nextHops" << std::make_tuple(u, destination) << "=>" << m_instance.nextHops(u, destination) << "has edge: " << m_flowD.hasEdge(u, m_instance.nextHops(u, destination)) << '\n';
						#endif
						if(m_flowD.hasEdge(u, m_instance.nextHops(u, destination))) {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout << "OSPF direct" << '\n';
							#endif
							path.emplace_back(u);
							u = m_instance.nextHops(u, destination);
						} else {
							#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
							std::cout << "OSPF tunnel" << '\n';
							#endif
							srcDst.emplace_front(u, std::get<0>(m_backup(u, destination)));
						}
					}
					assert(destination != -1);
					if(u == -1) {
						std::cerr << "Invalid next hop" << '\n';
						valid = false;
					} 
					if(path.size() >= 2 && !m_flowD.hasEdge(*std::prev(std::prev(path.end())), path.back())) {
						std::cerr << Graph::Edge(*std::prev(std::prev(path.end())), path.back()) <<" is off for demand " << Graph::Edge(s, t) << m_paths[i] << " vs " << path << '\n';
						valid = false;
					}
					if(u == srcDst.front().second) {
						srcDst.pop_front();
					}
				}
				path.emplace_back(u);
				if(m_paths[i] != path) {
					std::cerr << "Path are different!" << m_paths[i] << " vs " << path << '\n';
					valid = false;
				}
			}		
			return valid;
		}

		int getNbBackup() const {
			int nb = 0;
			for (int i = 0; i < m_backup.size1(); ++i) {
				for (int j = 0; j < m_backup.size2(); ++j) {
					if(std::get<1>(m_backup(i, j)) > 0) {
						++nb;
					}
				}
			}
			return nb;
		}

		int getNbRules() const {
			int nbRules = 0;
			for (int u = 0; u < m_instance.graph.getOrder(); ++u) {
				for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
					for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
						if(std::get<0>(m_SDNRules[u](s, t)) != -1) {
							nbRules++;
						}
					}
				}
			}
			return nbRules;
		}

		void save(const std::string& _filename, const double _time = 0.0) const {
			std::ofstream ofs(_filename);

			ofs << getPower() << '\n';
			ofs << -1 << '\n'; // Gap
			ofs << _time << '\n';
			bool first = true;
			for(int i = 0; i < m_instance.graph.getOrder(); ++i) {
				if(m_instance.SDN[i]) {
					if(!first) {
						ofs << '\t';
					} else {
						first = false;
					}
					ofs << i;
				}
			}
			ofs << '\n';

			ofs << m_flowD.size() << '\n';
			for(auto& e : m_flowD.getEdges()) {
				ofs << e.first << '\t' << e.second << '\t'  << m_flowD.getEdgeWeight(e) << '\n';
			}
			int nbPaths = 0;
			for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
				if(!m_paths[i].empty()) {
					++nbPaths;
				}
			}
			ofs << nbPaths << '\n';
			for(int i = 0; i < static_cast<int>(m_instance.demands.size()); ++i) {
				if(!m_paths[i].empty()) {
					ofs << m_paths[i].front() << '\t' << m_paths[i].back();
					for(auto& u : m_paths[i]) {
						ofs << '\t' << u;
					}
					ofs << '\n';
				}
			}
			std::vector< std::tuple<int, int, int> > backups;
			for (int i = 0; i < m_backup.size1(); ++i)
			{
				for (int j = 0; j < m_backup.size2(); ++j)
				{
					if(std::get<1>(m_backup(i, j)) > 0) {
						backups.emplace_back(i, j, std::get<0>(m_backup(i, j)));
					}
				}
			}
			ofs << backups.size() << '\n';
			for(auto& t : backups) {
				ofs << std::get<0>(t) << '\t' << std::get<1>(t) << '\t' << std::get<2>(t) << '\n';
			}

			std::cout << "Saved to " << _filename << '\n';
		}

		/*
		* Returns true if load on _edge2 is smaller than load on _edge1
		*/
		bool getEdgeWithSmallestLoad(const Graph::Edge& _edge1, const Graph::Edge& _edge2) const {
			assert(m_flowD.hasEdge(_edge1.first, _edge1.second));
			assert(m_flowD.hasEdge(_edge2.first, _edge2.second));

			return (m_flowD.getEdgeWeight(_edge1) + m_flowD.getEdgeWeight(_edge1.second, _edge1.first)) 
				> ( m_flowD.getEdgeWeight(_edge2) + m_flowD.getEdgeWeight(_edge2.second, _edge2.first) );
		}

		bool getEdgeWithSmallestAverageDemand(const Graph::Edge& _edge1, const Graph::Edge& _edge2) const {
			assert(m_flowD.hasEdge(_edge1.first, _edge1.second));
			assert(m_flowD.hasEdge(_edge2.first, _edge2.second));

			return (m_flowD.getEdgeWeight(_edge1) + m_flowD.getEdgeWeight(_edge1.second, _edge1.first)) / (m_nbFlowOnEdge[m_instance.edgeId(_edge1.first, _edge1.second)] + m_nbFlowOnEdge[m_instance.edgeId(_edge1.second, _edge1.first)]) 
				> (m_flowD.getEdgeWeight(_edge2) + m_flowD.getEdgeWeight(_edge2.second, _edge2.first)) / (m_nbFlowOnEdge[m_instance.edgeId(_edge2.first, _edge2.second)] + m_nbFlowOnEdge[m_instance.edgeId(_edge2.second, _edge2.first)]);
		}

		/*
		* Returns true if load on _edge2 is smaller than load on _edge1 or if _edge2 is full SDN link
		*/
		bool getEdgeWithSmallestLoadFirstSDN(const Graph::Edge& _edge1, const Graph::Edge& _edge2) const {
			assert(m_flowD.hasEdge(_edge1.first, _edge1.second));
			assert(m_flowD.hasEdge(_edge2.first, _edge2.second));
			const int nbSDN1 = isSDN(_edge1.first) + isSDN(_edge1.second);
			const int nbSDN2 = isSDN(_edge2.first) + isSDN(_edge2.second);
			if(nbSDN2 > nbSDN1) {
				return true;
			} else if(nbSDN2 < nbSDN1) {
				return false;
			} else {
				return (m_flowD.getEdgeWeight(_edge1) + m_flowD.getEdgeWeight(_edge1.second, _edge1.first)) 
					> ( m_flowD.getEdgeWeight(_edge2) + m_flowD.getEdgeWeight(_edge2.second, _edge2.first) );
			}

			
		}

		std::vector< std::tuple<int, int, int> > getRules(const int _u) const {
			std::vector<std::tuple<int, int, int>> rules;

			for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
				for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
					auto p = m_SDNRules[_u](s, t).first;
					if(p != -1) {
						rules.emplace_back(s, t, p);
					}
				}
			}
			return rules;
		}

		std::vector< std::tuple<int, int, int> > getRules() {
			std::vector<std::tuple<int, int, int>> rules;

			for(int u = 0; u < m_instance.graph.getOrder(); ++u) {
				for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
					for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
						auto p = m_SDNRules[u](s, t).first;
						if(p != -1) {
							rules.emplace_back(s, t, p);
						}
					}
				}
			}
			return rules;
		}

		std::vector<Graph::Path> getBackupPath() const {
			std::vector<Graph::Path> paths;
			for (int u = 0; u < m_instance.graph.getOrder(); ++u) {
				for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
					if(std::get<0>(m_backup(u, t)) != -1) {
						paths.push_back(std::get<2>(m_backup(u, t)));
					}
				}
			}
			return paths;
		}

		int getNbBackupRules() const {
			int nbRules = 0;
			for (int u = 0; u < m_instance.graph.getOrder(); ++u) {
				for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
					for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
						if(std::get<1>(m_SDNBackupRules[u](s, t)) > 0) {
							nbRules++;
						}
					}
				}
			}
			return nbRules;
		}

		std::vector< std::tuple<int, int, int, int> > getBackupRules(const int _u) const {
			std::vector< std::tuple<int, int, int, int> > rules;

			for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
				for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
					auto p = std::get<0>(m_SDNBackupRules[_u](s, t));
					if(p != -1) {
						rules.emplace_back(_u, s, t, p);
					}
				}
			}
			return rules;
		}

		std::vector< std::tuple<int, int, int, int> > getBackupRules() const {
			std::vector< std::tuple<int, int, int, int> > rules;
			for (int u = 0; u < m_instance.graph.getOrder(); ++u) {
				for(int s = 0; s < m_instance.graph.getOrder(); ++s) {
					for(int t = 0; t < m_instance.graph.getOrder(); ++t) {
						const auto p = std::get<0>(m_SDNBackupRules[u](s, t));
						if(p != -1) {
							rules.emplace_back(u, s, t, p);
						}
					}
				}
			}
			return rules;
		}


		std::vector< std::tuple<int, int, int> > getUsedTunnels() const {
			std::vector< std::tuple<int, int, int> > usedTunnels;

			for (int i = 0; i < m_backup.size1(); ++i)
			{
				for (int j = 0; j < m_backup.size2(); ++j)
				{
					if(std::get<1>(m_backup(i, j)) > 0) {
						assert(std::get<0>(m_backup(i, j)) != -1);
						usedTunnels.emplace_back(i, j, std::get<0>(m_backup(i, j)));
					}
				}
			}
			return usedTunnels;
		}

		std::vector< std::tuple<int, int, int> > getUsedTunnels(const int _u) const {
			std::vector< std::tuple<int, int, int> > usedTunnels;
			for (int j = 0; j < m_backup.size2(); ++j)
			{
				if(std::get<1>(m_backup(_u, j)) > 0) {
					assert(std::get<0>(m_backup(_u, j)) != -1);
					usedTunnels.emplace_back(_u, j, std::get<0>(m_backup(_u, j)));
				}
			}
			return usedTunnels;
		}

		bool hasValidRules(const Graph::Path& _path) {
			assert(!_path.empty());
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout << "hasValidRules" << _path << '\n';
			#endif
			const Graph::Node s = _path.front(), t = _path.back();
			for(auto iteU = _path.begin(), iteV = std::next(iteU); iteV != _path.end(); ++iteU, ++iteV) {
				if(m_instance.SDN[*iteU]) {
					if(m_SDNRules[*iteU](s, t).first != *iteV) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << '\n';
						#endif
						return false;
					}
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout << m_SDNRules[*iteU](s, t) << ", ";
					#endif
				} else {
					if(m_instance.nextHops(*iteU, t) != *iteV) {
						#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
						std::cout << '\n';
						#endif
						return false;
					}
					#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
					std::cout << m_instance.nextHops(*iteU, t) << ", ";
					#endif
				}
			}
			#if defined(LOG_LEVEL) && LOG_LEVEL <= 0
			std::cout << '\n';
			#endif
			return true;
		}

		std::vector<int> getSDN() const {
			std::vector<int> SDNs;
			for (int i = 0; i < int(m_instance.SDN.size()); ++i)
			{
				if(m_instance.SDN[i]) {
					SDNs.push_back(i);
				}
			}
			return SDNs;
		}

		/*
		* MIP Start
		*/

		bool isSDN(const int _u) const {
			return m_instance.SDN[_u];
		}

		// bool isDirectForwarding(const int _u, const int _v, const int _s, const int _t) const {
		// 	if(m_flowD.hasEdge(_u, _v) && m_flowOnEdge(m_instance.edgeId(_u, _v), )
		// 		[&](const Demand& _d) {
		// 			return _d.s == _s && _d.t == _t;
		// 		}) != m_flowOnEdge(_u, _v).end()) {
		// 		if(m_instance.SDN[_u]) {
		// 			std::cout << "SDN: " << m_SDNRules[_u](_s, _t).first << " <=> " << _v << '\n';
		// 			return m_SDNRules[_u](_s, _t).first == _v;
		// 		} else {
		// 			std::cout << "OSPF: " << m_instance.nextHops(_u, _t) << " <=> " << _v << '\n';
		// 			return m_instance.nextHops(_u, _t) == _v;
		// 		}
		// 	} else {
		// 		return false;
		// 	}
		// }

		// bool isBackupPath(const int _u, const int _s, const int _t, const Graph::Path& _path) const {
		// 	if(std::find(m_paths(_s, _t).begin(), m_paths(_s, _t).end(), _u) != m_paths(_s, _t).end()) {
		// 		if(m_instance.SDN[_u]) {
	 // 				if( !m_flowD.hasEdge(_u, m_SDNRules[_u](_s, _t).first) ) {
	 // 					printf("SDN next hop %d down\n", m_SDNRules[_u](_s, _t).first);
	 // 					return std::get<1>(m_backup(_u, _t)) > 0 && std::get<2>(m_backup(_u, _t)) == _path;	
	 // 				}
	 // 			} else {
	 // 				if( !m_flowD.hasEdge(_u, m_instance.nextHops(_u, _t)) ) {
	 // 					printf("OSPF next hop %d down\n", m_instance.nextHops(_u, _t));
	 // 					return std::get<1>(m_backup(_u, _t)) > 0 && std::get<2>(m_backup(_u, _t)) == _path;	
	 // 				}
	 // 			}
	 // 		}
 	// 		return false;
		// }

		bool isBackupEnd(const int _u, const int _t, const int _x) const {
			return std::get<0>(m_backup(_u, _t)) == _x;
		}

		const std::vector< Graph::Path >& getPaths() const {
			return m_paths;
		}

		std::pair< std::vector< std::tuple<int, int, int, int> >, std::vector< std::tuple< Graph::Path, int, int > > > getDecomposedPath(const int _s, const int _t) const {
			std::pair< std::vector< std::tuple<int, int, int, int> >, std::vector< std::tuple< Graph::Path, int, int > > > decomposedPath;

			std::list<std::pair<int, int>> srcDst { std::make_pair(_s, _t) };
			int u = _s;
			while(!srcDst.empty()) {
				int source = srcDst.front().first, destination = srcDst.front().second;
				if(m_instance.SDN[u]) {
					if(m_flowD.hasEdge(u, m_SDNRules[u](source, destination).first)) {
						decomposedPath.first.emplace_back(u, m_SDNRules[u](source, destination).first, source, destination);
						u = m_SDNRules[u](source, destination).first;
					} else {
						decomposedPath.second.emplace_back(std::get<2>(m_backup(u, destination)), source, destination);
						srcDst.emplace_front(u, std::get<0>(m_backup(u, destination)));
					}
				} else {
					if(m_flowD.hasEdge(u, m_instance.nextHops(u, destination))) {
						decomposedPath.first.emplace_back(u, m_instance.nextHops(u, destination), source, destination);
						u = m_instance.nextHops(u, destination);
					} else {
						decomposedPath.second.emplace_back(std::get<2>(m_backup(u, destination)), source, destination);
						srcDst.emplace_front(u, std::get<0>(m_backup(u, destination)));
					}
				}
				if(u == srcDst.front().second) {
					srcDst.pop_front();
				}
			}
			return decomposedPath;
		}

	protected:
		FixedSDNInstance m_instance;

		DiGraph m_flowD;
		Matrix<int> m_flowOnEdge;
		std::vector<int> m_nbFlowOnEdge;
		std::vector< Graph::Path > m_paths;

		Matrix< std::tuple<int, int, Graph::Path> > m_backup; // {endOfTunnel} {# use} {Graph::Path}
		std::vector< Matrix< std::pair<int, int> > > m_SDNRules; // {next hop} {#  use}
		std::vector< Matrix< std::tuple<int, int, Graph::Path> > > m_SDNBackupRules;
};

bool heuristic(HybridRouting& _hr, const std::vector<int> _demands) {
	// Route all demands
 	if(!_hr.routeDemands(_demands)) {
        std::cerr << "No valid route found!" << '\n';
 		return -1;
 	}

    // If successful, try shutting down SDN links one by one
 	auto edges = _hr.getSDNLinks();
    std::cout << "SDN links: " << edges << '\n';
 	auto solution = _hr;
 	while(!edges.empty()) {
        // std::cout << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n';
        // Sort by least loaded link
        std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _edge1, const Graph::Edge& _edge2) {
            return solution.getEdgeWithSmallestLoad(_edge1, _edge2);
        });

        // Try remove edges
        const auto edge = edges.back();
        edges.pop_back();
        const auto removedDemands = _hr.shutdownLink(edge.first, edge.second);
        // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
        // std::cout << "Rules: " << solution.getRules() << '\n';

 		if( _hr.routeDemands( removedDemands )) {
 			solution = _hr;
            std::cout << "Shut down " << edge << '\n';
            // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
            // std::cout << "Rules: " << solution.getRules() << '\n';

            // std::cout << "# backup: " << solution.getNbBackup() << ", " << solution.getNbBackupRules() << '\n';
            // std::cout << "# rules: " << solution.getNbRules() << '\n';
 		} else {
 			std::cout << "Could not shut down " << edge << '\n';
            std::cout << "# backup: " << _hr.getNbBackup() << ", " << _hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << _hr.getNbRules() << '\n';
 			_hr = solution;
 		}
 	}
 	_hr = solution;
 	return _hr.checkSolution();
}

bool heuristic_averageDemand(HybridRouting& _hr, const std::vector<int> _demands) {
	// Route all demands
 	if(!_hr.routeDemands(_demands)) {
        std::cerr << "No valid route found!" << '\n';
 		return -1;
 	}

    // If successful, try shutting down SDN links one by one
 	auto edges = _hr.getSDNLinks();
    std::cout << "SDN links: " << edges << '\n';
 	auto solution = _hr;
 	while(!edges.empty()) {
        // std::cout << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n';
        // Sort by least loaded link
        std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _edge1, const Graph::Edge& _edge2) {
            return solution.getEdgeWithSmallestAverageDemand(_edge1, _edge2);
        });

        // Try remove edges
        auto edge = edges.back();
        edges.pop_back();
        auto removedDemands = _hr.shutdownLink(edge.first, edge.second);
        // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
        // std::cout << "Rules: " << solution.getRules() << '\n';

 		if( _hr.routeDemands( removedDemands )) {
 			solution = _hr;
            std::cout << "Shut down " << edge << '\n';
            // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
            // std::cout << "Rules: " << solution.getRules() << '\n';

            // std::cout << "# backup: " << solution.getNbBackup() << ", " << solution.getNbBackupRules() << '\n';
            // std::cout << "# rules: " << solution.getNbRules() << '\n';
 		} else {
 			std::cout << "Could not shut down " << edge << '\n';
            std::cout << "# backup: " << _hr.getNbBackup() << ", " << _hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << _hr.getNbRules() << '\n';
 			_hr = solution;
 		}
 	}
 	_hr = solution;
 	return _hr.checkSolution();
}

bool heuristic_firstSDN(HybridRouting& _hr, const std::vector<int> _demands) {
	// Route all demands
 	if(!_hr.routeDemands(_demands)) {
        std::cerr << "No valid route found!" << '\n';
 		return -1;
 	}

    // If successful, try shutting down SDN links one by one
 	auto edges = _hr.getSDNLinks();
    std::cout << "SDN links: " << edges << '\n';
 	auto solution = _hr;
 	while(!edges.empty()) {
        // std::cout << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n'
            // << "----------------------------------------------------------------" << '\n';
        // Sort by least loaded link
        std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _edge1, const Graph::Edge& _edge2) {
            return solution.getEdgeWithSmallestLoadFirstSDN(_edge1, _edge2);
        });

        // Try remove edges
        auto edge = edges.back();
        edges.pop_back();
        auto removedDemands = _hr.shutdownLink(edge.first, edge.second);
        // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
        // std::cout << "Rules: " << solution.getRules() << '\n';

 		if( _hr.routeDemands( removedDemands )) {
 			solution = _hr;
            std::cout << "Shut down " << edge << '\n';
            // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
            // std::cout << "Rules: " << solution.getRules() << '\n';

            // std::cout << "# backup: " << solution.getNbBackup() << ", " << solution.getNbBackupRules() << '\n';
            // std::cout << "# rules: " << solution.getNbRules() << '\n';
 		} else {
 			std::cout << "Could not shut down " << edge << '\n';
            std::cout << "# backup: " << _hr.getNbBackup() << ", " << _hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << _hr.getNbRules() << '\n';
 			_hr = solution;
 		}
 	}
 	_hr = solution;
 	return _hr.checkSolution();
}

// bool heuristic_maxMultStretch(HybridRouting& _hr, const std::vector<int> _demands, double _maxStretch) {
// 	// Route all demands
//  	if(!_hr.routeDemandsMaxStretch(_demands, _maxStretch)) {
//         std::cerr << "No valid route found!" << '\n';
//  		return -1;
//  	}

//     // If successful, try shutting down SDN links one by one
//  	auto edges = _hr.getSDNLinks();
//     std::cout << "SDN links: " << edges << '\n';
//  	auto solution = _hr;
//  	while(!edges.empty()) {
//         // std::cout << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n';
//         // Sort by least loaded link
//        	std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _edge1, const Graph::Edge& _edge2) {
//             return solution.getEdgeWithSmallestAverageDemand(_edge1, _edge2);
//         });

//         // Try remove edges
//         const auto edge = edges.back();
//         edges.pop_back();
//         const auto removedDemands = _hr.shutdownLink(edge.first, edge.second);
//         // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
//         // std::cout << "Rules: " << solution.getRules() << '\n';

//  		if( _hr.routeDemandsMaxStretch( removedDemands, _maxStretch )) {
//  			solution = _hr;
//             std::cout << "Shut down " << edge << '\n';
//             // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
//             // std::cout << "Rules: " << solution.getRules() << '\n';

//             // std::cout << "# backup: " << solution.getNbBackup() << ", " << solution.getNbBackupRules() << '\n';
//             // std::cout << "# rules: " << solution.getNbRules() << '\n';
//  		} else {
//  			std::cout << "Could not shut down " << edge << '\n';
//             std::cout << "# backup: " << _hr.getNbBackup() << ", " << _hr.getNbBackupRules() << '\n';
//             std::cout << "# rules: " << _hr.getNbRules() << '\n';
//  			_hr = solution;
//  		}
//  	}
//  	_hr = solution;
//  	return _hr.checkSolution();
// }

// bool heuristic_maxAddStretch(HybridRouting& _hr, const std::vector<Demand> _demands, int _maxStretch) {
// 	// Route all demands
//  	if(!_hr.routeDemandsMaxStretch(_demands, _maxStretch)) {
//         std::cerr << "No valid route found!" << '\n';
//  		return -1;
//  	}

//     // If successful, try shutting down SDN links one by one
//  	auto edges = _hr.getSDNLinks();
//     std::cout << "SDN links: " << edges << '\n';
//  	auto solution = _hr;
//  	while(!edges.empty()) {
//         // std::cout << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n'
//             // << "----------------------------------------------------------------" << '\n';
//         // Sort by least loaded link
//         std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _edge1, const Graph::Edge& _edge2) {
//             return solution.getEdgeWithSmallestAverageDemand(_edge1, _edge2);
//         });

//         // Try remove edges
//         const auto edge = edges.back();
//         edges.pop_back();
//         const auto removedDemands = _hr.shutdownLink(edge.first, edge.second);
//         // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
//         // std::cout << "Rules: " << solution.getRules() << '\n';

//  		if( _hr.routeDemandsMaxStretch( removedDemands, _maxStretch )) {
//  			solution = _hr;
//             std::cout << "Shut down " << edge << '\n';
//             // std::cout << "Backups: " << solution.getUsedTunnels() << '\n' << solution.getBackupRules() << '\n';
//             // std::cout << "Rules: " << solution.getRules() << '\n';

//             // std::cout << "# backup: " << solution.getNbBackup() << ", " << solution.getNbBackupRules() << '\n';
//             // std::cout << "# rules: " << solution.getNbRules() << '\n';
//  		} else {
//  			std::cout << "Could not shut down " << edge << '\n';
//             std::cout << "# backup: " << _hr.getNbBackup() << ", " << _hr.getNbBackupRules() << '\n';
//             std::cout << "# rules: " << _hr.getNbRules() << '\n';
//  			_hr = solution;
//  		}
//  	}
//  	_hr = solution;
//  	return _hr.checkSolution();
// }
}
#endif