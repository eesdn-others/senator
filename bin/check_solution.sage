import collections
import sys
sys.path.append("../../MyPython")
import sndlib
import random
import numpy as np
import csv
from utility import path_edge_iterator

def loadHops(name):
	filename = "instances/{}_nextHop.txt".format(name)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
	

		row = r.next()
		order = int(row[0])
		nextHops = np.full((order, order), -1, dtype=np.dtype(int))
		for row in r:
			nextHops[int(row[0]), int(row[1])] = int(row[2])
	
	return nextHops

def loadSDNs(name, k):
	filename = "instances/{}_SDNs.txt".format(name)
	SDNs = []
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		r.next()
		for i in range(k):
			row = r.next()
			SDNs.append(int(row[0]))
	return SDNs




def loadTunnels(name, k, p):
	filename = "results/{}_{}_{:.2f}_usedTunnels.txt".format(name, k, p)
	tunnels = []
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		r.next()
		for row in r: 
			tunnels.append([int(row[0]), int(row[1]), int(row[2])])
	return tunnels

def loadSDNsRules(name, k, p):
	filename = "results/{}_{}_{:.2f}_rules.txt".format(name, k, p)
	
	rules = collections.defaultdict(list)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		for row in r: 
			rules[int(row[0])].append([int(row[1]), int(row[2]), int(row[3])])
	return rules

def loadBackupRules(name, k, p):
	filename = "results/{}_{}_{:.2f}_backupRules.txt".format(name, k, p)
	
	rules = collections.defaultdict(list)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		for row in r: 
			rules[int(row[0])].append([int(row[1]), int(row[2]), int(row[3])])
	return rules

def loadEdgeDown(name, k, p):
	filename = "results/{}_{}_{:.2f}_edges.txt".format(name, k, p)
	
	edges = []
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		for row in r: 
			edges.append( (int(row[0]), int(row[1])) )

	return edges

def getRule(rules, s, t):
	for rule in rules:
		if rule[0] == s and rule[1] == t:
			return rule[2]
	return -1

def getNextHopTunnel(tunnels, u, t):
	for t in tunnels:
		if t[0] == u and t[1] == t:
			return t[2]
	return -1

def getBackupRules(rules, u, s, t):
	for r in rules:
		if r[0] == s and r[1] == t:
			return r[2]
	return -1

if __name__ == "__main__":
	name = sys.argv[1]
	k = int(sys.argv[2])
	p = float(sys.argv[3])

	dg, demands = sndlib.getNetwork(name, p)
	nextHops = loadHops(name)
	SDNs = loadSDNs(name, k)

	rules = loadSDNsRules(name, k, p)
	edgeDowns = loadEdgeDown(name, k, p)
	backupRules = loadBackupRules(name, k, p)
	tunnels = loadTunnels(name, k, p)
	print SDNs

	def	findPath(u, s, t):
		# print u,
		if u == t:
			return [u]

		if u in SDNs:
			# print "SDN"
			nextHop = getRule(rules[u], s, t)
			# if nextHop == -1:
			# 	# print "No direct forwarding rules, check encapsulation"
			# 	nextHop = getBackupRules(backupRules[u], u, s, t)
			# 	# print "t",
			# 	return [u] + findPath(u, u, nextHop)[1:-1] + findPath(nextHop, s, t)
			# el
			if (u, nextHop) in edgeDowns or nextHop == -1:
				# print "Edge {} is down ".format((u, nextHop))
				print "Wrong path"
				sys.exit(-1)
			else:
				# print "Direct forwarding"
				# print "d",
				return [u] + findPath(nextHop, s, t)
		else:
			# print "OSPF"
			nextHop = nextHops[u, t]
			if nextHop == -1: 
				# print "No direct forwarding rules, check encapsulation"
				nextHop = getNextHopTunnel(tunnels, u, t)
				if nextHop == -1:
					print "Wrong path"
					sys.exit(-1)
				# print "t",
				return [u] + findPath(u, u, nextHop)[1:-1] + findPath(nextHop, s, t)
			elif (u, nextHop) in edgeDowns:
				# print "Edge {} is down ".format((u, nextHop))
				print "Wrong path"
				sys.exit(-1)
			else:
				# print "Direct forwarding"
				# print "d",
				return [u] + findPath(nextHop, s, t)

	for (s, t), d in demands.iteritems():
		print (s, t), findPath(s, s, t)
		# print

		









