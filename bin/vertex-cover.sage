import sys
sys.path.append("../../MyPython/")
import sndlib

def vertex_cover(name):
	g = sndlib.getGraph(name)

	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", 3600)

	x = lp.new_variable(binary=True, name="i")

	for u, v in g.edge_iterator(labels=None):
		lp.add_constraint(x[u] + x[v] >= 1)

	lp.set_objective(lp.sum([x[u] for u in g.vertex_iterator()]))

	print lp.solve(log=1)

	print [u for u in x.keys() if lp.get_values(x[u])]
	g.to_undirected().plot(vertex_colors={"blue":[u for u in x.keys() if not lp.get_values(x[u])], "green": [u for u in x.keys() if lp.get_values(x[u])]}).save(name+"_simple.pdf")

def vertex_cover_cycle(name):
	g = sndlib.getGraph(name)

	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", 3600)

	x = lp.new_variable(binary=True, name="i")

	for u, v in g.edge_iterator(labels=None):
		lp.add_constraint(x[u] + x[v] >= 1)

	for u in g.vertex_iterator():
		lp.add_constraint( lp.sum([x[v] for v in g.neighbor_out_iterator(u)]) == lp.sum([x[v] for v in g.neighbor_in_iterator(u)]))
		lp.add_constraint( lp.sum([x[v] for v in g.neighbor_in_iterator(u)]) >= 1 )
		lp.add_constraint( lp.sum([x[v] for v in g.neighbor_out_iterator(u)]) >= 1 )

	lp.set_objective(lp.sum([x[u] for u in g.vertex_iterator()]))

	print lp.solve(log=1)

	print [u for u in x.keys() if lp.get_values(x[u])]

	g.to_undirected().plot(vertex_colors={"blue":[u for u in x.keys() if not lp.get_values(x[u])], "green": [u for u in x.keys() if lp.get_values(x[u])]}).save(name+"_cycle.pdf")

if __name__ == "__main__":
	name = sys.argv[1]
	f = None
	if sys.argv[2] == "simple":
		f = vertex_cover
	elif sys.argv[2] == "cycle":
		f = vertex_cover_cycle
	f(name)
