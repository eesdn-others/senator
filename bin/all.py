import sys
sys.path.append("../../MyPython")
from pool import *
import numpy as np

sizes = {"germany50":50, "zib54":54, "atlanta":15, "ta2": 65, "test":7, "abilene":12, "pdh":11}
names = [sys.argv[1]] if sys.argv[1] != "all" else ["ta2", "zib54", "germany50", "atlanta", "abilene"]
algo = sys.argv[2]

time = 20*60

percents = {"germany50": [1.0, 1.5, 2.0, 2.5, 3.0], 
	"atlanta": [1, 2.75, 3.5, 4.75, 6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"abilene": [1],
	"pdh": [1]}
heuristics = ["normal"]#, "normal"]
pool = []

if algo in ["heur", "all"]:
	typeSDN = [0]#, 1, 2]
	energys = [sys.argv[3]] if sys.argv[3] != "all" else ["test", "real1"]
	h = sys.argv[4]
	for energy in energys:
		for name in names:
			for n in range(0, sizes[name]+1):
				for p in percents[name]:
					for t in typeSDN:
						pool.append("./Hybrid {} -s {} -d {} -e {} -n {} -m {}".format(name, n, p, energy, t, h))
						pool.append("./FixedHybrid {} -s {} -d {} -e {} -n {} -m {}".format(name, n, p, energy, t, h))

elif algo in ["LP", "all"]:
	for name in names:
		for n in range(0, sizes[name]+1):
			pool.append("./LP {} -s {} -t 3600".format(name, n))
else:
	print "No algo specified!", algo
	sys.exit(-1)

# for p in percents:
# 	pool.append("sage plot.py {} {}".format(name, p))

chargePool(pool)