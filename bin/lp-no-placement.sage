import collections
import sys
sys.path.append("../../MyPython")
import sndlib
import random
import numpy as np
import csv
from utility import path_edge_iterator

def nextInPath(p, u):
	assert(u in p)
	assert(u != p[-1])
	for i in xrange(len(p)-1):
		if p[i] == u:
			return p[i+1]

def loadEnergy(energy):
	filename = "instances/energy_{}.txt".format(energy)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		row = r.next()
		return [float(v) for v in row]

def loadHops(name):
	filename = "instances/{}_nextHop.txt".format(name)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
	

		row = r.next()
		order = int(row[0])
		nextHops = np.full((order, order), -1)
		for row in r:
			nextHops[int(row[0]), int(row[1])] = int(row[2])
	
	return nextHops

# def routing(dg, demands):
# 	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
# 	lp.solver_parameter("CPX_PARAM_TILIM", 3600)

# 	f = lp.new_variable(binary=True, name="f")
# 	loadMax = lp.new_variable(real=True, nonnegative=True, name="loadMax")

# 	lp.set_objective(loadMax[0])

# 	for u in dg.vertex_iterator():
# 		for (s, t), d in demands.iteritems():
# 			value = 0
# 			if u == s:
# 				 value = 1
# 			elif u == t:
# 				value = -1
# 			lp.add_constraint(lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) - lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) == value, name="flow conservation {} on {}".format((s, t), (u, v)))

# 			lp.add_constraint(lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) <= 1)
# 			lp.add_constraint(lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) <= 1)#, name="flow conservation {} on {}".format((s, t), (u, v)))

# 	for u, v, c in dg.edges():
# 		lp.add_constraint(lp.sum([demands[s, t] * f[u, v, s, t] for s, t in demands.keys()]) <= c, name="link capa {}".format((u, v)))
# 		lp.add_constraint(lp.sum([demands[s, t] / c * f[u, v, s, t] for s, t in demands.keys()]) <= loadMax[0])

# 	# lp.write_lp("routing.lp")
# 	lp.solve()

# 	for u, v, c in dg.edge_iterator():
# 		l = 0
# 		for s, t in demands.keys():
# 			if lp.get_values(f[u, v, s, t]):
# 				l += demands[s, t]
# 		print "{} -> {} / {} ({})".format((u, v), l, c, 100 * l / float(c))

# 	paths = {}
# 	for s, t in demands.iterkeys():
# 		path = []
# 		u = s
# 		path.append(u)
# 		while u != t:
# 			for v in dg.neighbors_out(u):
# 				b = lp.get_values(f[u, v, s, t])
# 				if(b):
# 					# print "lp.get_values(f[{}, {}, {}, {}]) = {}".format(u, v, s, t, b)
# 					u = v
# 					path.append(u)
# 					break
# 		# for i in range(len(path)-1):
# 		# 	paths[path[i], t] = path[i:]

# 		paths[s, t] = path
# 	return paths

def lp_no_placement(dg, SDNs, demands, nextHops, q, BP, LBP, time, frac=False):
	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", time)

	f = None
	b = None
	a = None
	if frac:
		f = lp.new_variable(real=True, nonnegative=True, name="f")
		b = lp.new_variable(real=True, nonnegative=True, name="b")
		a = lp.new_variable(real=True, nonnegative=True, name="a")		
	else:
		f = lp.new_variable(binary=True, name="f")
		b = lp.new_variable(binary=True, name="b")
		a = lp.new_variable(binary=True, name="a")

	x = lp.new_variable(binary=True, name="x")
	# z = lp.new_variable(binary=True, name="z")
	# i = lp.new_variable(binary=True, name="i")
	# o = lp.new_variable(binary=True, name="o")
	
	sources = set()	
	destinations = set()
	for s, t in demands.keys():
		sources.add(s)
		destinations.add(t)

	paths = {}
	pathsEdge = collections.defaultdict(list)
	for s in dg.vertex_iterator():
		for t in dg.vertex_iterator():
			if s != t:
				u = s
				v = nextHops[s, t]
				paths[s, t] = [s]
				while v != -1:
					paths[s, t].append(v)
					u = v 
					v = nextHops[u, t]
				for u_, v_ in path_edge_iterator(paths[s, t]):
					pathsEdge[u_, v_].append(paths[s, t])

	print "Building ILP..."

	flowOnEdge = collections.defaultdict(list)
	backupOnEdge = collections.defaultdict(list)
	
	print "Building flow conservation constraints..."
	for s, t in demands.iterkeys():
		demand = demands[s, t]
		# path = paths[s, t]
		outBackup = np.empty(dg.order(), dtype=object)
		inBackup = np.empty(dg.order(), dtype=object)
		incomingFlow = np.empty(dg.order(), dtype=object)
		outgoingFlow = np.empty(dg.order(), dtype=object)
		for j in xrange(dg.order()):
			outBackup[j] = []
			inBackup[j] = []
			incomingFlow[j] = []
			outgoingFlow[j] = []

		for u in dg.vertex_iterator():
			# Find next hop
			next = nextHops[u, t]
					

			# Set allowed backup path
			if u in SDNs or (next != None and next in SDNs):
				for v in dg.vertex_iterator():
					if v != u:
						if not u in SDNs or not v in dg.neighbors_out(u):
							bu = b[u, v, s, t]
							outBackup[u].append(bu)
							inBackup[v].append(bu)
							p =  paths[u, v]
							for u_, v_ in path_edge_iterator(p):
								backupOnEdge[u_, v_].append((bu, demand))

							# print "path between", (u, v), p, "goes through", (p[i], p[i-1])
							if not u in SDNs:
								lp.add_constraint(bu <= (1 - x[u, next]), name="Limit backup {} to {}".format(u, t))
								# print "x[{}, {}]".format(u, next)
			
			if u in SDNs:
				for v in dg.neighbor_out_iterator(u):
					if v != u:
						fu = f[u, v, s, t]
						outgoingFlow[u].append(fu)
						incomingFlow[v].append(fu)
						flowOnEdge[u, v].append((fu, demand))
			elif next != -1:
				fu = f[u, next, s, t] 
				outgoingFlow[u].append(fu)
				incomingFlow[next].append(fu)	
				flowOnEdge[u, next].append((fu, demand))

		for u in dg.vertex_iterator():
			value = 1 if u == s else -1 if u == t else 0
			lp.add_constraint(lp.sum(outBackup[u]) - lp.sum(inBackup[u]) + lp.sum(outgoingFlow[u]) - lp.sum(incomingFlow[u]) == value, name="flow {} conservation on {}".format((s, t), u))


	h = {}
	print "Building load..."
	for u, v, c in dg.edges():
		h[u, v] = lp.sum([fv * demand for (fv, demand) in flowOnEdge[u, v]]) + lp.sum([bv * demand for (bv, demand) in backupOnEdge[u, v]])

	print "Building link capa constraints.."
	for u, v, c in dg.edges():
		if u in SDNs or v in SDNs:
			lp.add_constraint( h[u, v] <= x[u, v] * c, name="link load {}".format((u, v)) )
		else:
			lp.add_constraint( h[u, v] <= c, name="link load {}".format((u, v)) )

	print "Upper bound on number of arcs"
	lp.add_constraint( lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) <= dg.size(), name="Upper bound number")
	if len(demands) == dg.order() * (dg.order()-1):
		print "Lower bound on number of arcs"
		lp.add_constraint(2 * (dg.order() - 1) <= lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) , name="Lower bound number")

	print "Building restrict backup path constraints..."
	for u in dg.vertex_iterator():
		if not u in SDNs:
			for t in destinations:
				lp.add_constraint( lp.sum([ a[u, y, t] for y in dg.vertex_iterator() if u != v ]) <= 1 )

	for u in dg.vertex_iterator():
		if not u in SDNs:
			for y in dg.vertex_iterator():
				if u != y:
					for s, t in demands.keys():
						lp.add_constraint(b[u, y, s, t] <= a[u, y, t])

	# print "Building interface constraints..."
	# for u, v in dg.edge_iterator(labels=None):
	# 	if u in SDNs and v in SDNs:
	# 		lp.add_constraint( 0 <= i[v, u] + o[u, v] - 2 * x[u, v] <= 1)
	# 	if u in SDNs:
	# 		lp.add_constraint( x[u, v] <= o[u, v] )
	# 	if v in SDNs:
	# 		lp.add_constraint( x[u, v] <= i[v, u] )
	# print "Building link usability constraints..."
	# for (u, v, s, t), fv in f.items():
	# 	if u in SDNs or v in SDNs:
	# 		lp.add_constraint(fv <= x[u, v], name="link usability {} flow {}".format((u, v), (s, t)))
	# 		# print "x[{}, {}]".format(u, v)

	# print "Building path usability constraints..."
	# for (u, v), bs in backupOnEdge.items():
	# 	for (bv, demand) in bs:
	# 		if u in SDNs or v in SDNs:
	# 			lp.add_constraint(bv <= x[u, v], name="path usability flow {} on {}".format((s, t), (u, v)))
	# 			# print "x[{}, {}]".format(u, v)

	print "Building interface constraints..."
	for u, v in dg.edge_iterator(labels=None):
		lp.add_constraint( x[u, v] == x[v, u] )
		if not u in SDNs and not v in SDNs:
			lp.add_constraint(x[u, v] >= 1)

	# print "Building SDN active constraints..."
	# for u in SDNs:
	# 	for v in dg.neighbors_out(u):
	# 		lp.add_constraint(z[u] >= o[u, v], name="SDN {} off {}".format(u, (u, v)))
	# 	for v in dg.neighbors_in(u):
	# 		lp.add_constraint(z[u] >= i[u, v], name="SDN {} off {}".format(u, (u, v)))

	print "Building objective value..."
	
	# lp.set_objective( lp.sum([ z[u]*BP[u] for u in dg.vertices() if u in SDNs ]) + \
	# 	lp.sum([ h[u, v] * q[u, v, u] + LBP[u, v, u] * o[u, v] for u in dg.vertices() if u in SDNs for v in dg.neighbor_out_iterator(u) ]) + \
	# 	lp.sum([ h[v, u] * q[v, u, u] + LBP[v, u, u] * i[u, v] for u in dg.vertices() if u in SDNs for v in dg.neighbor_in_iterator(u) ]) + \
	# 	sum([ BP[u] for u in dg.vertices() if not u in SDNs ]) + \
	# 	lp.sum([ h[u, v] * q[u, v, u] + LBP[u, v, u] for u in dg.vertices() if not u in SDNs for v in dg.neighbor_out_iterator(u) ]) + \
	# 	lp.sum([ h[v, u] * q[v, u, u] + LBP[v, u, u] for u in dg.vertices() if not u in SDNs for v in dg.neighbor_in_iterator(u) ]) )

	lp.set_objective( lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) )

	# lp.set_objective(lp.sum(x.values()))

	# print lp.show()
	lp.write_lp("no-placement.lp")
	print "Saved to \"no-placement.lp\""
	lp.solve(log=1)

	##########################
	##########################
	##########################
	##########################
	##########################

	for (u, v), xVar in x.items():
		print (u, v), "->", lp.get_values(xVar)

	finalPaths = {}
	for s, t in demands.iterkeys():
		print "Path for", (s, t), ":", 
		path = []
		u = s
		path.append(u)
		print u,
		while u != t:
			nextHop = None
			hopType = None
			for v in dg.neighbors_out(u):
				if((u, v, s, t) in f.keys() and lp.get_values(f[u, v, s, t])):
					# print "lp.get_values(f[{}, {}, {}, {}]) = {}".format(u, v, s, t, b)
					nextHop = v
					hopType = 'f'
					break
			if nextHop == None:
				for y in dg.vertex_iterator():
					try:
						if (u, y, s, t) in b.keys() and lp.get_values(b[u, y, s, t]):
							nextHop = y
							hopType = 'b'
							# print "backup:", (u, x, s, t)
							break
					except:
						pass
			assert nextHop != None, "{} has no path after {}".format((s, t), u)
			
			if hopType == 'f':
				print nextHop, 
				path.append(nextHop)
				assert not nextHop in path, "{} already in path".format(nextHop)
			else:
				print (paths[u, nextHop]),
				path += paths[u, nextHop][1:]
			u = nextHop

		finalPaths[s, t] = path
		print

	print "Getting backups"
	backup = []
	for u in dg.vertex_iterator():
		for y in dg.vertex_iterator():
			if u != y:
				for t in destinations:
					for s in sources:
						if (u, y, s, t) in b.keys() and lp.get_values(b[u, y, s, t]):
							backup.append([u, t, y])
							break

	flowG = DiGraph()
	for u, v, c in dg.edges():
		flowG.add_edge(u, v, sum([ lp.get_values(fVar) * demand for (fVar, demand) in flowOnEdge[u, v] ]) + sum([ lp.get_values(bVar) * demand for (bVar, demand) in backupOnEdge[u, v] ]))

	inactiveLinks = 0
	for (u, v), xVar in x.items():
		if not lp.get_values(xVar):
			inactiveLinks += 1

	print "# inactive links {} (total: {})".format(inactiveLinks, dg.size())
	print "Power:", lp.get_objective_value()
	print "Energy saved: {} ({})".format( maxPower - lp.get_objective_value(), ( maxPower - lp.get_objective_value() ) / float( maxPower ) )

	for u, v, c in dg.edge_iterator():
		print (u, v), flowG.edge_label(u, v), '/', c

	return flowG, finalPaths, lp.get_relative_objective_gap(), backup

def saveToCSV(filename, flowG, SDNs, resPaths, gap, backup):
	with open(filename, 'wb') as csvfile:
		w = csv.writer(csvfile, delimiter='\t')
		w.writerow(SDNs)
		w.writerow([flowG.size()])
		for u, v, c in flowG.edges():
			w.writerow([u, v, c])
		w.writerow([len(resPaths)])
		for (x, y), p in resPaths.iteritems():
			w.writerow([x, y] + p)
		w.writerow([gap])
		w.writerow([len(backup)])
		for b in backup:
			w.writerow(b)


# Return the n vertices with the highest betweeness
def SDNBetweeness(n, dg, paths, demands=None):
	if demands == None:
		demands = paths.keys()

	betweeness = np.full(dg.order(), 0, dtype=np.dtype(int))
	for s, t in demands:
		for u in paths[s, t]:
			betweeness[u] += 1
	print betweeness

	return sorted(dg.vertex_iterator(), key=lambda x: betweeness[x], reverse=True)[:n]

# Return the n vertices with the highest degree
def SDNDegree(n, dg):
	return sorted(dg.vertex_iterator(), key=lambda x: dg.out_degree(x), reverse=True)[:n]

if __name__ == "__main__":
	name = sys.argv[1]
	k = int(sys.argv[2])
	p = float(sys.argv[3])
	dg, demands = sndlib.getNetwork(name, p)
	placement = sys.argv[4]
	typ = sys.argv[5]
	energy = sys.argv[6]
	time = int(sys.argv[7])

	nextHops = loadHops(name)
	energyTuple = loadEnergy(energy)
	print energyTuple

	print "Network with {} nodes and {} links for {} demands".format(dg.order(), dg.size(), len(demands))
	BP = {}
	for u in dg.vertex_iterator():
		BP[u] = energyTuple[0]
	q = {}
	LBP = {}

	for u, v, c in dg.edge_iterator():
		q[u, v, u] = energyTuple[2] / float(c)
		LBP[u, v, u] = energyTuple[1]

		q[v, u, u] = energyTuple[2] / float(c)
		LBP[v, u, u] = energyTuple[1]
	
	maxPower = 0
	for u in dg.vertex_iterator():
		maxPower += BP[u]
		for v in dg.neighbor_out_iterator(u):
			maxPower += LBP[u, v, u] + dg.edge_label(u, v) * q[u, v, u]
		
		for v in dg.neighbor_in_iterator(u):
			maxPower += LBP[v, u, u] + dg.edge_label(u, v) * q[v, u, u]

	print "Max power:", maxPower
	
	# paths = routing(dg, demands)
	# for u in dg.vertex_iterator():
	# 	for v in dg.vertex_iterator():
	# 		if u != v:
	# 			if not (u, v) in paths.keys():
	# 				paths[u, v] = dg.shortest_path(u, v)
	# 			print (u, v), "->", paths[u, v]

	SDNs = None
	if placement == "bet":
		SDNs = SDNBetweeness(k, dg, paths, demands.keys())
	elif placement == "deg":
		SDNs = SDNDegree(k, dg)
	else:
		sys.exit(-1)

	print "Choosing {} nodes as SDNs: {}".format(k, SDNs)
	print '----------------------------------------------'

	if typ == "f":
		flowG, resPaths, gap, backup = lp_no_placement(dg, SDNs, demands, nextHops, q, BP, LBP, time, frac=True)
		saveToCSV("./results/{}/{}_{}_{}_LP_NP_{}_{}.res".format(energy, name, len(SDNs), p, placement, time), flowG, SDNs, resPaths, gap, backup)
	elif typ == "nf":
		flowG, resPaths, gap, backup = lp_no_placement(dg, SDNs, demands, nextHops, q, BP, LBP, time)
		saveToCSV("./results/{}/{}_{}_{}_ILP_NP_{}_{}.res".format(energy, name, len(SDNs), p, placement, time), flowG, SDNs, resPaths, gap, backup)
	else:
		print "No type selected!"
