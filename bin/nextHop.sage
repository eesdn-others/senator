import csv
import sys
sys.path.append("../../MyPython")
import sndlib

def routing(name, p):
	dg, demands = sndlib.getNetwork(name, p)

	for i in dg.vertex_iterator():
		for j in dg.vertex_iterator():
			if i != j and not (i, j) in demands.keys():
				demands[i, j] = 0

	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", 3600)
	# lp.solver_parameter("CPX_PARAM_EPGAP", 0.01)

	f = lp.new_variable(binary=True, name="f")
	nextHop = lp.new_variable(binary=True, name="nextHop")
	loadMax = lp.new_variable(real=True, nonnegative=True, name="loadMax")

	for u in dg.vertex_iterator():
		for (s, t), d in demands.iteritems():
			
			value = 0
			if u == s:
				 value = 1
			elif u == t:
				value = -1

			lp.add_constraint( lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) - lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) == value, name="flow conservation {} on {}".format((s, t), (u, v)) )

			lp.add_constraint( lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) <= 1 )
			lp.add_constraint( lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) <= 1 )

	for u, v, c in dg.edges():
		lp.add_constraint( lp.sum([demands[s, t] * f[u, v, s, t] for s, t in demands.iterkeys()]) <= c, name="link capa {}".format((u, v)) )
		# lp.add_constraint( lp.sum([demands[s, t] / c * f[u, v, s, t] for s, t in demands.iterkeys()]) <= loadMax[0] )

	for u in dg.vertex_iterator():
		for t in dg.vertex_iterator(): 
			lp.add_constraint( lp.sum([ nextHop[u, v, t] for v in dg.neighbor_out_iterator(u) ]) <= (1 if u != t else 0)) # Only one next hop per destination

	
	for (u, v, s, t), fVar in f.items():
		lp.add_constraint(fVar <= nextHop[u, v, t])

	lp.set_objective(lp.sum(fVar for fVar in f.values()))

	lp.solve(log=1)
 
	nextHops = []

	for (u, v, t), nVar in nextHop.items():
		if( lp.get_values(nVar) ):
			nextHops.append((u, t, v))

	filename  = "instances/{}_nextHop.txt".format(name)
	with open(filename, 'wb') as csvfile:
		w = csv.writer(csvfile, delimiter='\t')
		w.writerow([dg.order()])
		for u, t, v in nextHops:
			w.writerow([u, t, v])
		
#def shortestPathRouting(name, p):
	

if __name__ == "__main__":
	if len(sys.argv) < 3:
		print "Missing parameters. Use sage nextHop.sage name p"
		sys.exit(-1)
	name = sys.argv[1]
	p = float(sys.argv[2])

	routing(name, p)