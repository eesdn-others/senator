import collections
import sys
sys.path.append("../../MyPython")
import sndlib
import random
import numpy as np
import csv
from utility import path_edge_iterator

def saveToCSV(filename, flowG, SDNs, resPaths, gap):
	with open(filename, 'wb') as csvfile:
		w = csv.writer(csvfile, delimiter='\t')
		w.writerow(SDNs)
		w.writerow([flowG.size()])
		for u, v, c in flowG.edges():
			w.writerow([u, v, c])
		w.writerow([len(resPaths)])
		for (x, y), p in resPaths.iteritems():
			w.writerow([x, y] + p)
		w.writerow([gap])
		# w.writerow(activeLinks)

def spanning_subgraph(g):
	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", 3600)

	x = lp.new_variable(binary=True, name="x")
	f = lp.new_variable(binary=True, name="f")

	for u in g.vertex_iterator():
		for s in g.vertex_iterator():
			for t in g.vertex_iterator():
				if s != t:
					value = 1 if u == s else -1 if u == t else 0
					lp.add_constraint(lp.sum( [f[u, v, s, t] for v in g.neighbor_out_iterator(u)] ) - lp.sum( [f[v, u, s, t] for v in g.neighbor_in_iterator(u)] ) == value)

	for u, v in g.edge_iterator(labels=None):
		for s in g.vertex_iterator():
			for t in g.vertex_iterator():
				if s != t:
					lp.add_constraint(x[u, v] >= f[u, v, s, t])

	lp.set_objective(lp.sum(x.values()))

	lp.solve(log=1)
	return [(u, v) for (u, v), xVar in x.items() if lp.get_values(xVar)]





if __name__ == "__main__":
	name = sys.argv[1]
	k = int(sys.argv[2])
	p = float(sys.argv[3])
	typ = sys.argv[4]

	dg, demands = sndlib.getNetwork(name, p)
	
	edges = spanning_subgraph(dg)
	print len(edges)
	colors = {"blue":edges}
	dg.plot(edge_colors=colors).show(name+".pdf")