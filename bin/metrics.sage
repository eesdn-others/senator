import csv
import sys
sys.path.append("../MyPython")
from utility import path_edge_iterator
import sndlib

def readFromCSV(name, nbSDNs, percent=1.0, t="ILP_P"):
	filename = "results/{}_{}_{:.1f}_{}.res".format(name, nbSDNs, float(percent), t)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')

		row = r.next()
		SDNs = [int(v) for v in row]

		resG = DiGraph()
		m = int(r.next()[0])
		for _ in xrange(m):
			row = r.next()
			resG.add_edge(int(row[0]), int(row[1]), float(row[2]))

		dg, demands = sndlib.getNetwork(name, percent)

		paths = {}
		row = r.next()
		for _ in xrange(int(row[0])):
			row = r.next()
			paths[int(row[0]), int(row[1])] = [int(v) for v in row[2:]]

		gap = float(r.next()[0])
		nbTunnels = float(r.next()[0])
		
		########################################################################################################################
		BP = {}
		q = {}
		LBP = {}
		for u in dg.vertex_iterator():
			BP[u] = 50
		for u, v, c in dg.edge_iterator():
			q[u, v, u] = 10 / float(c)
			LBP[u, v, u] = 10

			q[v, u, u] = 10 / float(c)
			LBP[v, u, u] = 10

		maxPower = 0
		for u in dg.vertex_iterator():
			maxPower += BP[u]
			for v in dg.neighbor_out_iterator(u):
				maxPower += LBP[u, v, u] + dg.edge_label(u, v) * q[u, v, u]
			
			for v in dg.neighbor_in_iterator(u):
				maxPower += LBP[v, u, u] + dg.edge_label(v, u) * q[v, u, u]

		i = {}
		o = {}
		for u, v, c in resG.edge_iterator():
			i[v, u] = c != 0 and v in SDNs
			o[u, v] = c != 0 and u in SDNs

		power = 0
		for u in resG.vertex_iterator():
			power += BP[u]
			for v in resG.neighbor_out_iterator(u):
				power += o[u, v] * LBP[u, v, u] + resG.edge_label(u, v) * q[u, v, u]
			
			for v in resG.neighbor_in_iterator(u):
				power += i[u, v] * LBP[v, u, u] + resG.edge_label(v, u) * q[v, u, u]

	return SDNs, dg, demands, paths, gap, maxPower, resG, power, nbTunnels
