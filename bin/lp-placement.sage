import collections
import sys
sys.path.append("../../MyPython")
import sndlib
import random
import numpy as np
import csv
from utility import path_edge_iterator

def loadEnergy(energy):
	filename = "instances/energy_{}.txt".format(energy)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
		row = r.next()
		return [float(v) for v in row]

# def routing(dg, demands):
# 	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
# 	lp.solver_parameter("CPX_PARAM_TILIM", 3600)
# 	# lp.solver_parameter("CPX_PARAM_EPGAP", 0.01)

# 	f = lp.new_variable(binary=True, name="f")
# 	loadMax = lp.new_variable(real=True, nonnegative=True, name="loadMax")

# 	lp.set_objective(loadMax[0])

# 	for u in dg.vertex_iterator():
# 		for (s, t), d in demands.iteritems():
# 			value = 0
# 			if u == s:
# 				 value = 1
# 			elif u == t:
# 				value = -1
# 			lp.add_constraint(lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) - lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) == value, name="flow conservation {} on {}".format((s, t), (u, v)))

# 			lp.add_constraint(lp.sum([f[u, v, s, t] for v in dg.neighbors_out(u)]) <= 1)
# 			lp.add_constraint(lp.sum([f[v, u, s, t] for v in dg.neighbors_in(u)]) <= 1)#, name="flow conservation {} on {}".format((s, t), (u, v)))

# 	for u, v, c in dg.edges():
# 		lp.add_constraint(lp.sum([demands[s, t] * f[u, v, s, t] for s, t in demands.iterkeys()]) <= c, name="link capa {}".format((u, v)))
# 		lp.add_constraint(lp.sum([demands[s, t] / c * f[u, v, s, t] for s, t in demands.iterkeys()]) <= loadMax[0])

# 	# lp.write_lp("routing.lp")
# 	lp.solve()

# 	for u, v, c in dg.edge_iterator():
# 		l = 0
# 		for s, t in demands.keys():
# 			if lp.get_values(f[u, v, s, t]):
# 				l += demands[s, t]
# 		print "{} -> {} / {} ({})".format((u, v), l, c, 100 * l / float(c))

# 	paths = {}
# 	for s, t in demands.iterkeys():
# 		path = []
# 		u = s
# 		path.append(u)
# 		while u != t:
# 			for v in dg.neighbors_out(u):
# 				b = lp.get_values(f[u, v, s, t])
# 				if(b):
# 					# print "lp.get_values(f[{}, {}, {}, {}]) = {}".format(u, v, s, t, b)
# 					u = v
# 					path.append(u)
# 					break
# 		# for i in range(len(path)-1):
# 		# 	paths[path[i], t] = path[i:]

# 		paths[s, t] = path
# 	return paths

def loadHops(name):
	filename = "instances/{}_nextHop.txt".format(name)
	with open(filename, 'r') as csvfile:
		r = csv.reader(csvfile, delimiter='\t')
	

		row = r.next()
		order = int(row[0])
		nextHops = np.full((order, order), -1, dtype=np.dtype(int))
		for row in r:
			nextHops[int(row[0]), int(row[1])] = int(row[2])
	
	return nextHops

def lp_placement(dg, k, demands, nextHops, time, frac=False):
	print "lp_placement"
	lp = MixedIntegerLinearProgram(maximization = False, solver = "CPLEX")
	lp.solver_parameter("CPX_PARAM_TILIM", time)
	lp.solver_parameter("CPX_PARAM_THREADS", 1)

	f = None
	b = None
	a = None
	if frac:
		f = lp.new_variable(real=True, nonnegative=True, name="f")
		b = lp.new_variable(real=True, nonnegative=True, name="b")
		a = lp.new_variable(real=True, nonnegative=True, name="a")		
	else:
		f = lp.new_variable(binary=True, name="f")
		b = lp.new_variable(binary=True, name="b")
		a = lp.new_variable(binary=True, name="a")

	x = lp.new_variable(binary=True, name="x")
	# z = lp.new_variable(binary=True, name="z")
	S = lp.new_variable(binary=True, name="S")

	# i = lp.new_variable(binary=True, name="i")
	# o = lp.new_variable(binary=True, name="o")

	
	sources = set()	
	destinations = set()
	for s, t in demands.keys():
		sources.add(s)
		destinations.add(t)

	print "Building OSPF paths..."
	paths = {}
	pathsEdge = collections.defaultdict(list)
	for s in dg.vertex_iterator():
		for t in dg.vertex_iterator():
			if s != t:
				u = s
				v = nextHops[s, t]
				paths[s, t] = [s]
				while v != -1:
					paths[s, t].append(v)
					u = v 
					v = nextHops[u, t]
				for u_, v_ in path_edge_iterator(paths[s, t]):
					pathsEdge[u_, v_].append(paths[s, t])

	print "Building ILP..."

	# print "Building flow conservation constraints..."
	# for u in dg.vertex_iterator():
	# 	for s, t in demands.keys():
	# 		value = 1 if u == s else -1 if u == t else 0
	# 		lp.add_constraint(lp.sum( [ b[u, y, s, t] for y in dg.vertex_iterator() if y != u] ) - lp.sum( [ b[y, u, s, t] for y in dg.vertex_iterator() if y != u] ) + \
	# 						  lp.sum( [ f[u, v, s, t] for v in dg.neighbor_out_iterator(u) ] ) - lp.sum( [ f[v, u, s, t] for v in dg.neighbor_in_iterator(u) ] ) == value, name="flow {} conservation on {}".format((s, t), u))

	print "Building flow conservation constraints..."
	for u in dg.vertex_iterator():
		for s, t in demands.keys():
			value = 1 if u == s else -1 if u == t else 0
			lp.add_constraint(lp.sum( [ b[u, y, s, t] for y in dg.vertex_iterator() if y != u] ) - lp.sum( [ b[y, u, s, t] for y in dg.vertex_iterator() if y != u] ) + \
							  lp.sum( [ f[u, v, s, t] for v in dg.neighbor_out_iterator(u) ] ) - lp.sum( [ f[v, u, s, t] for v in dg.neighbor_in_iterator(u) ] ) == value, name="flow {} conservation on {}".format((s, t), u))
			
			lp.add_constraint(lp.sum( [ b[u, y, s, t] for y in dg.vertex_iterator() if y != u] ) + lp.sum( [ f[u, v, s, t] for v in dg.neighbor_out_iterator(u) ] ) <= 1)
			lp.add_constraint(lp.sum( [ b[y, u, s, t] for y in dg.vertex_iterator() if y != u] ) + lp.sum( [ f[v, u, s, t] for v in dg.neighbor_in_iterator(u) ] ) <= 1)


	h = {}
	print "Building load..."
	for u, v, c in dg.edges():
		h[u, v] = lp.sum([d*1.1 * b[p[0], p[-1], s, t] for p in pathsEdge[u, v] for (s, t), d in demands.iteritems()]) + lp.sum([d * f[u, v, s, t] for (s, t), d in demands.iteritems()])

	print "Building edge capacity constraints.."
	for u, v, c in dg.edge_iterator():
		lp.add_constraint( h[u, v] <= x[u, v] * c, name= "link {} capa constraints".format((u, v)) )

	# print "Building link usability constraints..."
	# for (u, v, s, t), fVar in f.items():
	# 	lp.add_constraint( fVar <= x[u, v], name="link usability {} flow {}".format((u, v), (s, t)) )

	# print "Building path usability constraints..."
	# for (y, w, s, t), bVar in b.items():
	# 	p = paths[y, w]
	# 	for u, v in path_edge_iterator(p):
	# 		lp.add_constraint(bVar <= x[u, v], name= "Path {} usability constraints {}".format((y, w), (u, v)))

	print "Backup usability constraints..."
	for u in dg.vertex_iterator():
		for s, t in demands.keys():
			for y in dg.vertex_iterator():
				if u != y:
					if not y in dg.neighbor_out_iterator(u):
						next = None
						if u != t:
							next = nextHops[u, t]
							
						if next != None:
							lp.add_constraint( b[u, y, s, t] <= S[u] + 1 - x[u, next], name="backup {} {} usability".format((u, y), (s, t)) )		
					else:
						next = None
						if u != t:
							next = nextHops[u, t]
							
						if next != None:
							lp.add_constraint( b[u, y, s, t] <= 1 - x[u, next], name="backup {} {} usability".format((u, y), (s, t)) )
							lp.add_constraint( b[u, y, s, t] <= 1 - S[u], name="backup {} {} usability".format((u, y), (s, t)) )

	print "Building links on/off equality constraints..."
	for u, v in dg.edge_iterator(labels=None):
		lp.add_constraint( x[u, v] == x[v, u] )

	print "Building disableable links..."
	for u, v in dg.edge_iterator(labels=None):
		lp.add_constraint(x[u, v] >= 1 - S[u] - S[v])
	
	# print "Building SDN active constraints..."
	# for u in dg.vertex_iterator():
	# 	for v in dg.neighbors_out(u):
	# 		lp.add_constraint(z[u] >= o[u, v], name="SDN {} off {}".format(u, (u, v)))
	# 	for v in dg.neighbors_in(u):
	# 		lp.add_constraint(z[u] >= i[u, v], name="SDN {} off {}".format(u, (u, v)))

	# print "Building placement constraints..."
	# for u in dg.vertex_iterator():
	# 	for v in dg.neighbors_out(u):
	# 		lp.add_constraint( o[u, v] >= (1 - S[u]) , name="placement {}".format(u) )
	# 	for v in dg.neighbors_in(u):
	# 		lp.add_constraint( i[u, v] >= (1 - S[u]) , name="placement {}".format(u) )

	print "Building flow usage constraints..."
	for u in dg.vertex_iterator():
		for s, t in demands.keys():
			next = nextHops[u, t]
			for v in dg.neighbor_out_iterator(u):
				if v != next:
					lp.add_constraint( f[u, v, s, t] <= S[u], name= "{} to {} not for {} flow  usage".format(u, v, (s, t)) )


	print "Building restrict backup path constraints..."
	for u in dg.vertex_iterator():
		for t in destinations:
			lp.add_constraint( lp.sum([ a[u, y, t] for y in dg.vertex_iterator() if u != v ]) <= S[u] * dg.order() + 1)

	for u in dg.vertex_iterator():
		for y in dg.vertex_iterator():
			if u != y:
				for s, t in demands.keys():
					lp.add_constraint(b[u, y, s, t] <= a[u, y, t] + S[u])


	lp.add_constraint(lp.sum(S.values()) <= k, name="SDNs number")

	print "Upper bound on number of arcs"
	lp.add_constraint( lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) <= dg.size(), name="Upper bound number")
	if len(demands) == dg.order() * (dg.order()-1):
		print "Lower bound on number of arcs"
		lp.add_constraint(2 * (dg.order() - 1) <= lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) , name="Lower bound number")

	print "Building objective value..."
	# lp.set_objective( lp.sum([ z[u] * BP[u] for u in dg.vertices() ]) + \
	# 	lp.sum([ h[u, v] * q[u, v, u] + LBP[u, v, u] * o[u, v] for u in dg.vertices() for v in dg.neighbor_out_iterator(u) ]) +\
	# 	lp.sum([ h[v, u] * q[v, u, u] + LBP[v, u, u] * i[u, v] for u in dg.vertices() for v in dg.neighbor_in_iterator(u) ]) )

	lp.set_objective( lp.sum( [ x[u, v] for u, v in dg.edge_iterator(labels=None) ] ) )

	# print lp.show()
	lp.write_lp("placement.lp")
	print "Saved to \"placement.lp\""
	lp.solve(log=1)

	############################
	############################
	############################
	############################
	############################

	for (u, v), xVar in x.items():
		print (u, v), "->", lp.get_values(xVar)

	SDNs = [ u for u, sVar in S.items() if lp.get_values(sVar) ]
	print "SDNs :", SDNs
	print "# Active links:", len([xVar for (u, v), xVar in x.items() if lp.get_values(xVar)])

	finalPaths = {}
	for s, t in demands.iterkeys():
		print "Path for", (s, t), ":", 
		path = []
		u = s
		path.append(u)
		print u,
		while u != t:
			nextHop = None
			hopType = None
			for v in dg.neighbors_out(u):
				if((u, v, s, t) in f.keys() and lp.get_values(f[u, v, s, t])):
					# print "lp.get_values(f[{}, {}, {}, {}]) = {}".format(u, v, s, t, b)
					assert nextHop == None, "next hop is already set as {}".format(nextHop)
					nextHop = v
					hopType = 'f'
			if nextHop == None:
				for y in dg.vertex_iterator():
					try:
						if (u, y, s, t) in b.keys() and lp.get_values(b[u, y, s, t]):
							nextHop = y
							hopType = 'b'
							# print "backup:", (u, x, s, t)
							break
					except:
						pass
			assert nextHop != None, "{} has no path after {}".format((s, t), u)
			
			if hopType == 'f':
				# assert not nextHop in path, "{} already in path".format(nextHop)				
				print nextHop, 
				path.append(nextHop)
			else:
				print (paths[u, nextHop]),
				path += paths[u, nextHop][1:]
			u = nextHop

		finalPaths[s, t] = path
		print

	# for u in dg.vertex_iterator():
	# 	for y in dg.vertex_iterator():
	# 		if u != y:
	# 			for t in destinations:
	# 				for s in sources:
	# 					if (u, y, s, t) in b.keys() and lp.get_values(b[u, y, s, t]):
	# 						backup.append([u, t, y])
	# 						break

	# print "# inactive links {} (total: {})".format(inactiveLinks, dg.size())

	# print "Energy saved: {} ({})".format( maxPower - lp.get_objective_value(), ( maxPower - lp.get_objective_value() ) / float( maxPower ) )
	
	flowG = DiGraph()
	for u, v, c in dg.edges():
		flowG.add_edge(u, v, sum([ lp.get_values(f[u, v, s, t]) * d for (s, t), d in demands.iteritems() ]) + sum([ lp.get_values(b[p[0], p[-1], s, t]) * d*1.1 for p in pathsEdge[u, v] for (s, t), d in demands.iteritems() ]))

	for u, v, c in dg.edge_iterator():
		print (u, v), flowG.edge_label(u, v), '/', c

	backup = []
	for (u, y, s, t), bVar in b.items():
		if lp.get_values(bVar):
			backup.append([u, t, y])
	print "# backups", len(backup)


	return flowG, SDNs, finalPaths, lp.get_relative_objective_gap(), backup

def saveToCSV(filename, flowG, SDNs, resPaths, gap, backup):
	with open(filename, 'wb') as csvfile:
		w = csv.writer(csvfile, delimiter='\t')
		w.writerow(SDNs)
		w.writerow([flowG.size()])
		for u, v, c in flowG.edges():
			w.writerow([u, v, c])
		w.writerow([len(resPaths)])
		for (x, y), p in resPaths.iteritems():
			w.writerow([x, y] + p)
		w.writerow([gap])
		w.writerow([len(backup)])
		for b in backup:
			w.writerow(b)

if __name__ == "__main__":
	name = sys.argv[1]
	k = sys.argv[2]
	p = float(sys.argv[3])
	dg, demands = sndlib.getNetwork(name, p)
	nextHops = loadHops(name)
	typ = sys.argv[4]
	energy = sys.argv[5]
	time = int(sys.argv[6])

	# energyTuple = loadEnergy(energy)

	print "Network with {} nodes and {} links for {} demands".format(dg.order(), dg.size(), len(demands))
	# BP = {}
	# for u in dg.vertex_iterator():
	# 	BP[u] = energyTuple[0]
	# q = {}
	# LBP = {}

	# for u, v, c in dg.edge_iterator():
	# 	q[u, v, u] = energyTuple[2] / float(c)
	# 	LBP[u, v, u] = energyTuple[1]

	# 	q[v, u, u] = energyTuple[2] / float(c)
	# 	LBP[v, u, u] = energyTuple[1]

	# maxPower = 0
	# for u in dg.vertex_iterator():
	# 	maxPower += BP[u]
	# 	for v in dg.neighbor_out_iterator(u):
	# 		maxPower += LBP[u, v, u] + dg.edge_label(u, v) * q[u, v, u]
		
	# 	for v in dg.neighbor_in_iterator(u):
	# 		maxPower += LBP[v, u, u] + dg.edge_label(v, u) * q[v, u, u]

	# print "Max power:", maxPower
	
	# paths = routing(dg, demands)
	# for u in dg.vertex_iterator():
	# 	for v in dg.vertex_iterator():
	# 		if u != v:
	# 			if not (u, v) in paths.keys():
	# 				paths[u, v] = dg.shortest_path(u, v)
	# 			print (u, v), "->", paths[u, v]
	print '----------------------------------------------'

	if typ == "frac":
		flowG, SDNs, resPaths, gap, backup = lp_placement(dg, k, demands, nextHops, time, frac=True)
		print "Chosen SDNs:", SDNs
		saveToCSV("./results/{}/{}_{}_{}_LP_P_{}.res".format(energy, name, k, p, time), flowG, SDNs, resPaths, gap, backup)
	elif typ == "nf":
		flowG, SDNs, resPaths, gap, backup = lp_placement(dg, k, demands, nextHops, time)
		print "Chosen SDNs:", SDNs
		saveToCSV("./results/{}/{}_{}_{}_ILP_P_{}.res".format(energy, name, k, p, time), flowG, SDNs, resPaths, gap, backup)
	else:
		print "No type selected!", typ