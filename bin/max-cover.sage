import sys
sys.path.append("../../MyPython/")
import sndlib
import csv 

names = [sys.argv[1]] if sys.argv[1] != "all" else ["ta2", "zib54", "germany50", "atlanta"]


for name in names:
	g = sndlib.getGraph(name)
	nodes = []

	with open("./instances/" + name + "_max-cover.txt", 'w') as csvfile:
		writer = csv.writer(csvfile, delimiter='\t')
		writer.writerow( [ g.order() ] )
		for n in range(0, g.order()+1):
			lp = MixedIntegerLinearProgram(maximization = True, solver = "CPLEX")
			lp.solver_parameter("CPX_PARAM_TILIM", 3600)

			x = lp.new_variable(binary=True, name="x")
			y = lp.new_variable(binary=True, name="y")

			for u, v in g.edge_iterator(labels=None):
				lp.add_constraint(x[u] + x[v] >= y[u, v])

			lp.add_constraint( lp.sum( [x[u] for u in g.vertex_iterator() ]) == n )

			lp.set_objective(lp.sum([y[u, v] for u, v in g.edge_iterator(labels=None)]))

			nbArcs = lp.solve()
			print n, nbArcs, [ u for u, xVar in x.items() if lp.get_values(xVar) ]
			nodes.append([ u for u, xVar in x.items() if lp.get_values(xVar) ])
			
			subset = True
			for node in nodes[n-1]:
				if not node in nodes[n]:
					subset = False
					print node, " not in previous cover",
			if not subset:
				print 

			writer.writerow( [ u for u, xVar in x.items() if lp.get_values(xVar) ] )