import csv
import sys
import matplotlib
import collections
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter
import metrics
import numpy

import sys
sys.path.append("../../MyPython")
import operator
import sndlib
from math import sqrt
def latexify(fig_width=None, fig_height=None, columns=1):
    """Set up matplotlib's RC params for LaTeX plotting.
    Call this before plotting a figure.

    Parameters
    ----------
    fig_width : float, optional, inches
    fig_height : float,  optional, inches
    columns : {1, 2}
    """

    # code adapted from http://www.scipy.org/Cookbook/Matplotlib/LaTeX_Examples

    # Width and max height in inches for IEEE journals taken from
    # computer.org/cms/Computer.org/Journal%20templates/transactions_art_guide.pdf

    assert(columns in [1,2])

    if fig_width is None:
        fig_width = 3.39 if columns==1 else 6.9 # width in inches

    if fig_height is None:
        golden_mean = (sqrt(5)-1.0)/2.0    # Aesthetic ratio
        fig_height = fig_width*golden_mean # height in inches

    MAX_HEIGHT_INCHES = 8.0
    if fig_height > MAX_HEIGHT_INCHES:
        print("WARNING: fig_height too large:" + fig_height + 
              "so will reduce to" + MAX_HEIGHT_INCHES + "inches.")
        fig_height = MAX_HEIGHT_INCHES

    params = {'backend': 'pdf',
              # 'text.latex.preamble': ['\usepackage{gensymb}'],
              'axes.labelsize': 8, # fontsize for x and y labels (was 10)
              'axes.titlesize': 8,
              'font.size': 24, # was 10
              'legend.fontsize': 8, # was 10
              'xtick.labelsize': 8,
              'ytick.labelsize': 8,
              # 'text.usetex': True,
              'figure.figsize': [fig_width, fig_height],
              'font.family': 'serif', 
    }

    matplotlib.rcParams.update(params)

latexify(columns=1)

orders = {
	"germany50":50, 
	"zib54":54, 
	"atlanta":15, 
	"ta2": 65, 
	"test":7, 
	"abilene":12,
	"pdh":11
}

percents = {
	"germany50": [1.0, 1.5, 2.0, 2.5, 3.0], 
	"atlanta":[1, 2.75, 3.5, 4.75, 6], 
	"ta2": [1, 2.75, 4.5, 6.25, 8], 
	"zib54": [1, 3, 5, 7, 9],
	"abilene": [1],
	"pdh": [1]
}

labels = {
	"LP_path_5": "LP", \
	"LP_path-sdn_10": "LP-Fixed Node", \
	'heur_normal_0':"Heur Deg", \
	"heur_fixed_normal_0":"Heur Fixed Deg", \
	'heur_normal_1':"Heur Clo", \
	"heur_fixed_normal_1":"Heur Fixed Clo", \
	'heur_normal_2':"Heur Bet", \
	"heur_fixed_normal_2":"Heur Fixed Bet",\
	"heur_addStretch_10_0":"Heur Fixed Bet 10 maxAddStretch"
}

colors = {"LP_path_5": "green", \
	"LP_path_10": "green", \
	"LP_path-sdn_10": "black", 
	"heur_fixed_normal_3": "cyan",
	"heur_fixed_average_0": "yellow"}

markers = {"LP_path_5": "d", \
	"LP_path_10": "d", \
	"LP_path-sdn_10": "."}

markersize = {"LP_path-sdn_10": 13}

x = [0, 2, 3, 7, 8, 9, 11, 18, 23, 24]
val = [2, 1, 0, 1, 2, 3, 4, 3, 2, 2]

def plotDailyLinks(name, method, energy="real1", percent=1.0):
	savings = collections.defaultdict(dict)
	percentageDeployement = [.1, .25, .5, 1]
	deploys = [int(i*orders[name]) for i in percentageDeployement]
	# _, _, _, _, _, _, maxPower, _, _ = metrics.readFromCSV(name, energy, 0, percents[name][4], method)
	# maxPower = []
	for deploy in deploys:
		# _, _, _, _, _, _, power, _, _ = metrics.readFromCSV(name, energy, 0, percents[name][i], method)
		# maxPower.append(power)
		for i in range(5):
			SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energy, deploy, percents[name][i], method)
			savings[deploy][i] = dg.size() - flowG.size()

	plt.clf()
	for deploy in deploys:
		y = [savings[deploy][val[i]] for i in range(len(x))]
		plt.step(x, y, where='post', label="{} nodes".format(deploy)) 
	
	plt.xticks([i * 4 for i in range(7)])
	plt.xlim([0, 24])
	plt.xlabel("Hours of the day")

	plt.ylabel("# of active links")
	plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda y, position: "{:.2f}%".format(100.0 * y / dg.size())))
	plt.yticks([.1*i*dg.size() for i in range(0, 6)])

	plt.legend(loc='best', framealpha=0)
	plt.grid()
	plt.subplots_adjust(left=0.20, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	plt.savefig("./figures/{}/dailyLinks.pdf".format(name))

def plotTunnels_NbNodes(name, method, energy="real1", percent=1.0):
	nbTunnels = []
	for n in range(orders[name]+1):
		SDNs, dg, demands, paths, gap, flowG, power, nbTunnel, time = metrics.readFromCSV(name, energy, n, percent, method)
		nbTunnels.append(nbTunnel / float(orders[name]))

	plt.clf()
	plt.plot(range(orders[name]+1), nbTunnels)
	plt.xlabel("# of SDN nodes")
	plt.xlim([0, orders[name]])

	plt.ylabel("# of tunnels")
	# plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda y, position: "{:.2f}%".format(100.0 * y / nbTunnels[0])))
	# plt.yticks([.1*i*nbTunnels[0] for i in range(0, 11)])
	plt.grid()
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	plt.savefig("./figures/paper/{}/tunnels_NbNodes.pdf".format(name))

def plotSavingsDuringDay(name, method):
	savings = collections.defaultdict(dict)
	percentageDeployement = [.1, .25, .5, 1]
	deploys = [int(i*orders[name]) for i in percentageDeployement]
	
	energys = ["real1", "real2"]
	nodeEnergy = [95, 60]
	links = sndlib.getGraph(name).size()

	ax = plt.figure().add_subplot(111)    # The big subplot
	ax.spines['top'].set_color('none')
	ax.spines['bottom'].set_color('none')
	ax.spines['left'].set_color('none')
	ax.spines['right'].set_color('none')
	ax.tick_params(labelcolor='w', top='off', bottom='off', left='off', right='off')
	

	# axarr = [fig.add_subplot(211), fig.add_subplot(212)]
	f, axarr = plt.subplots(2, sharex=True, sharey=True)
	for e in range(len(energys)):
		maxSavings = []
		maxPowers = []
		for i in range(5):
			_, _, _, _, _, _, maxPower, _, _ = metrics.readFromCSV(name, energys[e], 0, percents[name][4], method)
			maxPowers.append(maxPower)
		for deploy in deploys:
			# _, _, _, _, _, _, power, _, _ = metrics.readFromCSV(name, energys, 0, percents[name][i], method)
			for i in range(5):
				SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energys[e], deploy, percents[name][i], method)
				savings[deploy][i] = (maxPowers[i] - power) / maxPowers[i]
				maxSavings.append((maxPowers[i] - (links * nodeEnergy[e])) / maxPowers[i])
			print name, deploy, 100 * min(savings[deploy].iteritems(), key=operator.itemgetter(1))[1], 100 * max(savings[deploy].iteritems(), key=operator.itemgetter(1))[1], 100 * sum(savings[deploy].values()) / float(len(savings[deploy])), 100 * max(savings[deploy].iteritems(), key=operator.itemgetter(1))[1] - 100 * min(savings[deploy].iteritems(), key=operator.itemgetter(1))[1]

		for deploy in deploys:
			y = [savings[deploy][val[i]] for i in range(len(x))]
			axarr[e].step(x, y, where='post', label="{} nodes".format(deploy)) 
		
		y = [maxSavings[val[i]] for i in range(len(x))]
		# axarr[e].step(x, y, where='post', label="Maximum savings") 

		axarr[e].set_xticks([i * 4 for i in range(7)])
		axarr[e].set_xlim([0, 24])

		
		axarr[e].yaxis.set_major_formatter(FuncFormatter(lambda y, position: "{:d}".format(int(100 * y))))
		axarr[e].set_yticks([.1*i for i in range(0, 4)])
		axarr[e].grid()
		axarr[e].set_ylim([0, .4])

	lgd = axarr[1].legend(bbox_to_anchor=(0.5, -0.7), loc='center', ncol=2, framealpha=0)	
	plt.ylabel("Energy used (%)")
	plt.xlabel("Hours of the day")
	plt.subplots_adjust(
		# left=0.15, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.1, hspace=0.1)
	print "Saved to ./figures/paper/{}/dailySavings.pdf".format(name)
	plt.savefig("./figures/paper/{}/dailySavings.pdf".format(name), bbox_extra_artists=(lgd,), bbox_inches='tight')

def multStretch_nbSDN(name, method, energy="real1", percent=1.0):
	deploys = [ int(orders[name] * i) for i in [.1, .25, .5, 1]] 
	realX = []
	stretch = []
 
 	dg = sndlib.getGraph(name)
 	dist, pred = dg.shortest_path_all_pairs()

	for i in deploys:
		try:
			SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energy, i, percent, method)
			realX.append( 100 * i / float(orders[name]) )
			stretch.append( [ (len(p) - 1) / float(dist[s][t]) for (s, t), p in paths.iteritems() ] )
		except IOError as e:
			print "Exception:", e, e.args
			pass

			
	plt.clf()
	plt.subplots_adjust(left=0.18, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	if stretch:
		bp = plt.boxplot(stretch, whis=[10, 90], labels=["10%", "25%", "50%", "100%"], flierprops={"marker": 'x'})

		medians = [l.get_xydata()[0][1] for l in bp["medians"]]
		boxes = [(l.get_xydata()[0][1], l.get_xydata()[2][1]) for l in bp["boxes"]]
		caps = [l.get_xydata()[0][1] for l in bp["caps"]]

		for i in range(4):
			print [caps[2*i], boxes[i][0], medians[i], boxes[i][1], caps[2*i+1]],
		print

		plt.xlabel("% of SDN nodes")
		plt.ylabel("Stretch")
		plt.ylim(0, plt.ylim()[1])
		# plt.legend(loc="best")
		plt.grid()
		print "figures/paper/{}/stretch_nbSDN.pdf".format(name)
		plt.savefig("figures/paper/{}/stretch_nbSDN.pdf".format(name))

def delay_nbSDN(name, method, energy="real1", percent=1.0):
	deploys = [ int(orders[name] * i) for i in [0, .1, .25, .5, 1]] 
	realX = []
	delays = []
 
 	# dg = sndlib.getGraph(name)
 	# dist, pred = dg.shortest_path_all_pairs()

	for i in deploys:
		try:
			SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energy, i, percent, method)
			realX.append( 100 * i / float(orders[name]) )
			delays.append( [ (len(p) - 1) * 1.8 for (s, t), p in paths.iteritems() ] )
		except IOError as e:
			print "Exception:", e, e.args
			pass

			
	plt.clf()
	print len(delays)
	if delays:
		bp = plt.boxplot(delays, whis=[10, 90], labels=["0%", "10%", "25%", "50%", "100%"], flierprops={"marker": 'x'})

		medians = [l.get_xydata()[0][1] for l in bp["medians"]]
		boxes = [(l.get_xydata()[0][1], l.get_xydata()[2][1]) for l in bp["boxes"]]
		caps = [l.get_xydata()[0][1] for l in bp["caps"]]

		for i in range(len(deploys)):
			print [caps[2*i], boxes[i][0], medians[i], boxes[i][1], caps[2*i+1]],
		print

		plt.xlabel("% of SDN nodes")
		plt.ylabel("Delay (ms)")
		plt.ylim(0, plt.ylim()[1])
		# plt.legend(loc="best")
		plt.grid()
		plt.subplots_adjust(left=0.15, bottom=0.18, right=0.96, top=0.93,
	                        wspace=0.2, hspace=0.2)
		print "figures/paper/{}/delay_nbSDN.pdf".format(name)
		plt.savefig("figures/paper/{}/delay_nbSDN.pdf".format(name) )

def compareILP(name="atlanta", energy="real1", percent=1.0):
	deploys = range(0, orders[name]+1)
	methods = ["LP_path_5", "heur_normal_0"]

	realX = {}
	Y = {}
	Gap = {}
	_, _, _, _, _, _, maxPower, _, _ = metrics.readFromCSV(name, energy, 0, percent, "LP_path_5")
	for method in methods:
		Y[method] = []
		realX[method] = []
		Gap[method] = []
		for i in deploys:
			try:
				SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energy, i, percent, method)
				realX[method].append( i / float(orders[name]) ) 
				Y[method].append((maxPower - power) / maxPower)
				print power,
				if gap != -1:
					print gap, power * (1-gap)
					Gap[method].append( (power * gap) / maxPower )
				else:
					Gap[method].append(0)
			except IOError as e:
				print "Exception:", e, e.args
				pass

	# Show differences
	for i in range(len(deploys)):
		print i,
		tmp =[]
		for method in methods:
			tmp.append(Y[method][i])
		print [v * 100 for v in numpy.diff(tmp)], tmp

	print realX
	plt.clf()
	i = 0
	for method in methods:
		if realX[method]:
			if sum(Gap[method]) > 0:
				plt.errorbar(realX[method], Y[method], yerr=( [ 0 for _ in xrange(len(realX[method])) ], Gap[method] ), markersize=markersize.get(method, 10), color=colors.get(method, "red"), label=labels.get(method, method), marker=markers.get(method, "+"), mfc='None', mec=colors.get(method, "red"))
			else:
				plt.plot(realX[method], Y[method], markersize=markersize.get(method, 10), color=colors.get(method, "red"), label=labels.get(method, method), marker=markers.get(method, "+"), mfc='None', mec=colors.get(method, "red"))
		i += 1
	plt.xlabel( "% of SDNs nodes" )
	plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, position: "{:d}".format(int(100 * x))))
	plt.xlim([ 0, plt.xlim()[1] ])

	plt.ylabel( "Energy savings (%)" )
	plt.gca().yaxis.set_major_formatter(FuncFormatter(lambda y, position: "{:d}".format(int(100 * y))))
	plt.yticks([.1*i for i in range(0, 3)])
	plt.ylim([0, .2])

	plt.legend( loc="lower right" )
	plt.grid()
	plt.subplots_adjust(left=0.15, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	print "figures/paper/compareILPvsHeur.pdf"
	plt.savefig("figures/paper/compareILPvsHeur.pdf")

def compareILPTime(name="atlanta", energy="real1", percent=1.0):
	deploys = range(0, orders[name]+1)
	methods = ["LP_path_5", "heur_normal_0"]

	realX = {}
	Y = {}
 
	for method in methods:
		Y[method] = []
		realX[method] = []
		for i in deploys:
			try:
				SDNs, dg, demands, paths, gap, flowG, power, nbTunnels, time = metrics.readFromCSV(name, energy, i, percent, method)
				realX[method].append( 100 * i / float(orders[name]) )
				Y[method].append( time )
			except IOError as e:
				print "Exception:", e, e.args
				pass

			
	plt.clf()
	for method in methods:
		print realX[method], Y[method]
		if realX[method]:
			# print Y[method]
			plt.plot(realX[method], Y[method], color=colors.get(method, "red"), markersize=markersize.get(method, 10), label=labels.get(method, method), marker=markers.get(method, "+"), mfc='None', mec=colors.get(method, "red"))
	plt.xlabel("% of SDN nodes")
	plt.ylabel("Time")
	plt.yscale("log")
	plt.yticks([.001, .005, .1, 1, 10, 120, 3600], ['1ms', '5ms', '100ms', '1s', '10s', '2m', '1h'])
	plt.legend(loc="best")	
	plt.grid()
	plt.subplots_adjust(left=0.20, bottom=0.18, right=0.96, top=0.93,
                        wspace=0.2, hspace=0.2)
	print "figures/paper/compareILPvsHeurTime.pdf" 
	plt.savefig("figures/paper/compareILPvsHeurTime.pdf")


if __name__ == "__main__":
	# if sys.argv[1] == "all" or sys.argv[1] == "savings":
	# 	plotSavingsVsNbNodes(sys.argv[2], sys.argv[3])
	names = [sys.argv[2]] if sys.argv[2] != "all" else ["atlanta", "germany50", "zib54", "ta2"]
	for name in names:
		if sys.argv[1] == "all" or sys.argv[1] == "tunnels":
			plotTunnels_NbNodes(name, sys.argv[3])
		if sys.argv[1] == "all" or sys.argv[1] == "daily":
			plotSavingsDuringDay(name, sys.argv[3])
		if sys.argv[1] == "all" or sys.argv[1] == "stretch":
			multStretch_nbSDN(name, sys.argv[3])
		if sys.argv[1] == "all" or sys.argv[1] == "delay":
			delay_nbSDN(name, sys.argv[3])
		if sys.argv[1] == "all" or sys.argv[1] == "links":
			plotDailyLinks(name, sys.argv[3])
	if sys.argv[1] == "all" or sys.argv[1] == "compare":
		compareILP("atlanta")
		compareILPTime("atlanta")


