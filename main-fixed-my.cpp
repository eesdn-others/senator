#include <iostream>
#include <iomanip>

#include <FixedHybridRouting.hpp>
#include <utility.hpp>
#include <tclap/CmdLine.h>
#include <SNDlib.hpp>

struct Param { 
    std::string model;
    std::string name;
    double factor;
    int nbSDN;
    std::string energyModel;
    int nodeMetric;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");
    
    TCLAP::ValuesConstraint<std::string> vCons({ "normal", "maxAddStretch", "maxMultStretch", "average", "first-sdn" });
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used", 
        false, "normal", &vCons);        
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name", 
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<std::string> energyModel("e", "energyModel", "Specify the energy model",
        false, "real1", "string");
    cmd.add(&energyModel);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nodeMetric("n", "nodeMetric", "Specify the metric used for sorting the nodes (0=degree, 1=closeness, 2=betweeness, 3=max cover)",
        false, 0, "int");
    cmd.add(&nodeMetric);

    TCLAP::ValueArg<int> nbSDN("s", "numberSDN", "Specify the number of SDN nodes", 
        true, 4, "int");
    cmd.add(&nbSDN);

    cmd.parse(argc, argv);

    return { modelName.getValue(), networkName.getValue(), demandFactor.getValue(), 
            nbSDN.getValue(), energyModel.getValue(), nodeMetric.getValue() };
}

int main(int argc, char** argv) { 
 //    if(argc < 4) {
 //        std::cerr << "Missing arguments: {name} {nbSDN} {percent}" << '\n';
 //        exit(-1); 
 //    }
	// std::string name(argv[1]);
	// int nbSDN = std::stoi(argv[2]);  
	// double percent = std::stod(argv[3]);
 //    std::string energy = "real1";

 //    auto demands = loadDemandsFromFile(std::string("./instances/" + name + "_demand.txt"));
 //    auto topology = DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));
 //    auto nextHops = loadNextHop(std::string("./instances/" + name + "_nextHop.txt"));
 //    auto energyTuple = loadEnergy(std::string("./instances/energy_" + energy + ".txt"));
 //    auto SDN = loadSDNs(std::string("./instances/" + name + "_SDNs.txt"), nbSDN, 0);

 //    auto lowestDemand = demands;
 //    for(const auto& demand : demands) {
 //        demand.d *= percent;
 //    }
    const auto params = getParams(argc, argv);

    auto demands = loadDemandsFromFile("./instances/" + params.name + "_demand.txt");
    const auto topology = DiGraph::loadFromFile("./instances/" + params.name + "_topo.txt");
    const auto nextHops = loadNextHop("./instances/" + params.name + "_nextHop.txt");
    const auto energyTuple = loadEnergy(std::string("./instances/energy_" + params.energyModel + ".txt"));

    std::vector<int> SDN; 
    if(params.nodeMetric < 3) {
        SDN = loadSDNs(std::string("./instances/" + params.name + "_SDNs.txt"), params.nbSDN, params.nodeMetric);
    } else { 
        SDN = loadSDNsMaxCover(std::string("./instances/" + params.name + "_max-cover.txt"), params.nbSDN);
    }
    const auto lowestDemand = demands;
    std::vector<int> allDemandsID;
    int i = 0; 
    for(auto& demand : demands) {
        demand.d *= params.factor;
        allDemandsID.push_back(i);
        ++i;
    }

    Hybrid::FixedSDNInstance instanceLow(std::get<0>(topology), energyTuple, nextHops, lowestDemand, SDN);
    Hybrid::FixedSDNInstance instance(std::get<0>(topology), energyTuple, nextHops, demands, SDN);


    Hybrid::HybridRouting hr(instanceLow);
    if(heuristic_firstSDN(hr, allDemandsID)) {
        std::ofstream ofs(std::string("./instances/" + params.name + "_backups.txt"));
        ofs << instance.graph.getOrder() << '\n';
        for(const auto& b : hr.getUsedTunnels()) {
            ofs << std::get<0>(b) << '\t' << std::get<1>(b) << '\t' << std::get<2>(b) << '\n';
        }
    }

    const auto backups = loadTunnels(std::string("./instances/" + params.name + "_backups.txt"));
    Hybrid::FixedHybridRouting fhr (instance, backups);
    if(!fhr.routeDemands(allDemandsID)) {
        std::cerr << "No route found!" << '\n';
        return -1;
    }

    auto fSolution = fhr;
    
    std::vector<Graph::Edge> edges = fhr.getSDNLinks();
    std::list<Graph::Edge> edgesDown;
    while(!edges.empty()) {
        std::sort(edges.begin(), edges.end(), [&](const Graph::Edge& _e1, const Graph::Edge& _e2) {
            return fhr.getEdgeWithSmallestLoadFirstSDN(_e1, _e2);
        } );

        // Try remove edges
        const auto& edge = edges.back();
        std::cout << "Try removing " << edge;
        if( fhr.routeDemands( fhr.shutdownLink(edge.first, edge.second)) ) {
            fSolution = fhr;
            edgesDown.push_back(edge);
            std::cout << " => succes" << '\n';
        } else {
            std::cout << " => fail" << '\n';
            fhr = fSolution;
        }
        edges.pop_back();
    }

    fSolution.checkSolution();

    std::string filename = "./results/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_edges.txt";
    std::ofstream ofs(filename);
    std::cout << filename << '\n';
    for(const auto& edge : edgesDown) {
        ofs << edge.first << '\t' << edge.second << '\n';
    }
    ofs.close();

    filename = "./results/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_rules.txt";
    ofs.open(filename);
    std::cout << filename << '\n';
    for(int u = 0; u < instance.graph.getOrder(); ++u) {
        if(SDN[u]) {
            for(const auto& rule : fSolution.getRules(u)) {
                ofs << u << '\t' << std::get<0>(rule) << '\t' << std::get<1>(rule) << '\t' <<  std::get<2>(rule) << '\n';
            }
        }
    }
    ofs.close();

    filename = "./results/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_backupRules.txt";
    ofs.open(filename);
    std::cout << filename << '\n';
    for(int u = 0; u < instance.graph.getOrder(); ++u) {
        if(SDN[u]) {
            for(const auto& rule : fSolution.getBackupRules(u)) {
                ofs << u << '\t' << std::get<0>(rule) << '\t' << std::get<1>(rule) << '\t' <<  std::get<2>(rule) << '\n';
            }
        }
    }
    ofs.close();

    filename = "./results/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_usedTunnels.txt";
    ofs.open(filename);
    std::cout << filename << '\n';
    auto usedTunnels = fSolution.getUsedTunnels();
    ofs << usedTunnels.size() << '\n';
    for(const auto& tunnel : usedTunnels) {
        ofs << std::get<0>(tunnel) << '\t' << std::get<1>(tunnel) << '\t' << std::get<2>(tunnel) << '\n';
    }
    ofs.close();
	return 0;
}