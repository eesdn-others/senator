WORKDIR = %cd%
UNAME_S := $(shell uname -s)

ifeq ($(UNAME_S), Linux)
	CC = gcc
	CXX = g++
	AR = ar
	LD = g++
else 
	CC = gcc-5
	CXX = g++-5
	AR = ar
	LD = g++-5
endif

SRC = $(wildcard ./src/*.cpp)
HEADER = $(wildcard ./include/*.h ./include/*.hpp)

ifeq ($(UNAME_S), Linux)
	CPLEX_DIR = $(HOME)/CPLEX/
else
	CPLEX_DIR = /Users/nhuin/Applications/IBM/ILOG/CPLEX_Studio1261/
endif


BOOST_LIB =  /usr/local/include/
JNI_LIB1 = /usr/lib/jvm/java-openjdk/include/linux 
JNI_LIB2 = /usr/lib/jvm/java-openjdk/include
ifeq ($(UNAME_S), Linux)
	MY_INC = ~/Documents/MyC++/
else
	MY_INC = ../MyC++/
endif

CPLEX_LIB = $(CPLEX_DIR)cplex/include/
CPLEX_CONCERT_LIB = $(CPLEX_DIR)concert/include/

INC = -I./include -I./src -isystem $(BOOST_LIB) -I $(MY_INC)

# ifeq ($(USE_CPLEX), 1)
# INC += -isystem $(CPLEX_LIB) -isystem $(CPLEX_CONCERT_LIB)
# endif

OPT_CFLAGS = 
CFLAGS = -Wextra -Wall -Werror -ansi -Weffc++ -Wfatal-errors -Wno-unused-parameter -DIL_STD
GCCVERSION = $(shell gcc --version | grep ^gcc | sed 's/^.* //g')
ifeq "$(GCCVERSION)" "4.8.2"
CFLAGS += -std=c++11
else
CFLAGS += -std=c++14
endif
RESINC = 

LIB =  -lm -lpthread
LIBDIR =
ifeq ($(UNAME_S), Linux)
	LIBDIR_CPLEX = -L$(CPLEX_DIR)cplex/lib/x86-64_sles10_4.1/static_pic -L$(CPLEX_DIR)concert/lib/x86-64_sles10_4.1/static_pic/
else
	LIBDIR_CPLEX = -L$(CPLEX_DIR)cplex/lib/x86-64_osx/static_pic -L$(CPLEX_DIR)concert/lib/x86-64_osx/static_pic/
endif


LDFLAGS = 

ifdef DEBUG
	CFLAGS += -g -O0
	DIR = Debug
else 
ifdef PROFILE
	CFLAGS += -g -fexpensive-optimizations -O3 -DNDEBUG -DBOOST_UBLAS_NDEBUG
	OPT_CFLAGS += -DPROFILE
	DIR = Profile
else
	CFLAGS += -fexpensive-optimizations -O3 -DNDEBUG -DBOOST_UBLAS_NDEBUG
	DIR = Release
endif
endif

ifdef LOG_LEVEL
	OPT_CFLAGS += -DLOG_LEVEL=$(LOG_LEVEL) 
endif

CFLAGS += $(OPT_CFLAGS)
OBJDIR = obj/$(DIR)

before_lp:
	$(eval LIBDIR += $(LIBDIR_CPLEX))
	$(eval INC += -isystem $(CPLEX_LIB) -isystem $(CPLEX_CONCERT_LIB))
	$(eval LIB += -lilocplex -lconcert -lcplex)

################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################
################################################################################################################################


SRC_MAIN = $(SRC) ./main.cpp
OUT_MAIN = bin/Hybrid
OBJ_MAIN = $(SRC_MAIN:.%.cpp=$(OBJDIR)%.o)
DEPS_MAIN = $(SRC_MAIN:.%.cpp=$(OBJDIR)%.d)
-include $(DEPS_MAIN)

sim: before_all out_main 
out_main: $(OBJ_MAIN)
	$(LD) $(LIBDIR) -o $(OUT_MAIN) $(OBJ_MAIN) $(LDFLAGS) $(LIB)
clean-main: 
	rm -f $(OBJ_MAIN) $(OUT_MAIN) $(DEPS_MAIN)

################################################################################################################################

SRC_FIXED = $(SRC) ./main-fixed.cpp
OUT_FIXED = bin/FixedHybrid
OBJ_FIXED = $(SRC_FIXED:.%.cpp=$(OBJDIR)%.o)
DEPS_FIXED = $(SRC_FIXED:.%.cpp=$(OBJDIR)%.d)
-include $(DEPS_FIXED)

fixed: before_all out_fixed 
out_fixed: $(OBJ_FIXED)
	$(LD) $(LIBDIR) -o $(OUT_FIXED) $(OBJ_FIXED) $(LDFLAGS) $(LIB)
clean-fixed: 
	rm -f $(OBJ_FIXED) $(OUT_FIXED) $(DEPS_FIXED)

################################################################################################################################

SRC_FIXED_MY = $(SRC) ./main-fixed-my.cpp
OUT_FIXED_MY = bin/FixedHybridMy
OBJ_FIXED_MY = $(SRC_FIXED_MY:.%.cpp=$(OBJDIR)%.o)
DEPS_FIXED_MY = $(SRC_FIXED_MY:.%.cpp=$(OBJDIR)%.d)
-include $(DEPS_FIXED_MY)

fixed-my: before_all out_fixed-my
out_fixed-my: $(OBJ_FIXED_MY)
	$(LD) $(LIBDIR) -o $(OUT_FIXED_MY) $(OBJ_FIXED_MY) $(LDFLAGS) $(LIB)
clean-fixed-my: 
	rm -f $(OBJ_FIXED_MY) $(OUT_FIXED_MY) $(DEPS_FIXED_MY)

################################################################################################################################

SRC_LP = $(SRC) ./main-lp.cpp
OUT_LP = bin/LP
OBJ_LP = $(SRC_LP:.%.cpp=$(OBJDIR)%.o)
DEPS_LP = $(SRC_LP:.%.cpp=$(OBJDIR)%.d)
-include $(DEPS_LP)

lp: before_all before_lp out_lp
out_lp: $(OBJ_LP)
	$(LD) $(LIBDIR) -o $(OUT_LP) $(OBJ_LP) $(LDFLAGS) $(LIB)
clean-lp: 
	rm -f $(OBJ_LP) $(OUT_LP) $(DEPS_LP)

################################################################################################################################

SRC_TEST = $(SRC) ./main-test.cpp
OUT_TEST = bin/Test
OBJ_TEST = $(SRC_TEST:.%.cpp=$(OBJDIR)%.o)
DEPS_TEST = $(SRC_TEST:.%.cpp=$(OBJDIR)%.d)
-include $(DEPS_TEST)

test: before_all out_test
out_test: $(OBJ_TEST)
	$(LD) $(LIBDIR) -o $(OUT_TEST) $(OBJ_TEST) $(LDFLAGS) $(LIB)
clean-test: 
	rm -f $(OBJ_TEST) $(OUT_TEST) $(DEPS_TEST)

################################################################################################################################


all: sim fixed fixed-my lp
before_all:	
	mkdir -p $(OBJDIR)/src 
	mkdir -p bin/results

$(OBJDIR)%.o: .%.cpp
	$(CXX) $(CFLAGS) $(INC) -MMD -MP -c $< -o $@

$(OBJDIR)%.o.chrono: .%.cpp
	$(CXX) $(CFLAGS) $(INC) -MMD -MP -c $< -o $@

clean: clean-main clean-fixed clean-fixed-my clean-lp clean-test


.PHONY: 