#include <iostream>
#include <iomanip>

#include "FixedHybridRouting.hpp"
#include "utility.hpp"
#include "SNDlib.hpp"

int main(int argc, char** argv) {
	auto topology = DiGraph::loadFromFile(std::string("./instances/pdh_topo.txt"));
	auto ksp = ShortestPath<DiGraph>(&std::get<0>(topology)).getKShortestPath(10, 0, 10);
	for(const auto& p : ksp) {
		std::cout << p << std::endl;
	}
	return 0;
}