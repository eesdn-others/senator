#include <iostream>
#include <iomanip>
#include <map>
#include <set>

#include <tclap/CmdLine.h>
#include <utility.hpp>
#include <SNDlib.hpp>


#include "HybridRouting.hpp"
#include "PathFormulation.hpp"

void placement(const DiGraph& _topology, const int& _nbSDN, const std::vector<Demand>& _demands, const Matrix<int>& _nextHops, const std::string& _filename);
void placement_pathFormulation(int _time, const DiGraph& _topology, const int& _nbSDN, const std::vector<Demand>& _demands, const Matrix<int>& _nextHops, int _k, const std::string& _filename, const Hybrid::HybridRouting* _hr = nullptr, const std::vector<int>* _SDNs = nullptr);

struct Param { 
    std::string model;
    std::string name;
    double factor;
    int nbSDN;
    std::string energyModel;
    int maxTime;
    int nbPath;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");
    
    TCLAP::ValuesConstraint<std::string> vCons({ "edge", "path", "path-sdn", "old-path"});
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used", 
        false, "path", &vCons);        
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name", 
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<std::string> energyModel("e", "energyModel", "Specify the energy model",
        false, "real1", "string");
    cmd.add(&energyModel);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nbSDN("s", "numberSDN", "Specify the number of SDN nodes", 
        true, 4, "int");
    cmd.add(&nbSDN);

    TCLAP::ValueArg<int> maxTime("t", "maxTime", "Specify the maximum time allowed for the LP (in seconds)", 
        false, 3600, "int");
    cmd.add(&maxTime);

    TCLAP::ValueArg<int> nbPath("p", "nbPath", "Specify the number of shortest paths for the path formulation", 
        false, 5, "int");
    cmd.add(&nbPath);

    cmd.parse(argc, argv);

    return { modelName.getValue(), networkName.getValue(), demandFactor.getValue(), 
            nbSDN.getValue(), energyModel.getValue(), maxTime.getValue(), nbPath.getValue()};
}

int main(int argc, char** argv) {
    #if defined(LOG_LEVEL)
        std::cout << "Loggin level at " << LOG_LEVEL << '\n';
    #endif
    Param params = getParams(argc, argv);

    auto demands = loadDemandsFromFile("./instances/" + params.name + "_demand.txt");
    const auto topology = DiGraph::loadFromFile("./instances/" + params.name + "_topo.txt");
    const auto nextHops = loadNextHop("./instances/" + params.name + "_nextHop.txt");
    const auto SDN = loadSDNs("./instances/" + params.name + "_SDNs.txt", params.nbSDN, 0);
    const auto energyTuple = loadEnergy(std::string("./instances/energy_" + params.energyModel + ".txt"));

    std::vector<int> allDemandsID;
    int i = 0; 
    for(auto& demand : demands) {
        demand.d *= params.factor;
        allDemandsID.push_back(i);
        ++i;
    }
    std::cout << nextHops << '\n';

    Hybrid::FixedSDNInstance instance(std::get<0>(topology), energyTuple, nextHops, demands, SDN);

    if(params.model == "edge") {
       const std::string filename = "results/lp_"+params.model+"_"+params.name+"_"+std::to_string(params.nbSDN)+"_"+std::to_string(params.factor)+".txt";
    } else if(params.model == "path") {
        const std::string filename = "results/real1/"+params.name+"_"+std::to_string(params.nbSDN)+"_"+to_string_with_precision(params.factor, 2)+"_LP_"+params.model+"_"+std::to_string(params.nbPath)+".res";
        Hybrid::PathFormulation pathFormulation(instance, params.nbPath, params.maxTime);   
        Hybrid::HybridRouting hr(instance);
        if(heuristic(hr, allDemandsID)) {
            pathFormulation.addMIPStart(hr);
        }
        std::cout << "Energy used: " << hr.getPower() << '\n';
        pathFormulation.solve();
        pathFormulation.getSolution().save(filename);
    } else if(params.model == "old-path") {
        const std::string filename = "results/real1/"+params.name+"_"+std::to_string(params.nbSDN)+"_"+to_string_with_precision(params.factor, 2)+"_LP_"+params.model+"_"+std::to_string(params.nbPath)+".res";
        Hybrid::HybridRouting hr(instance);
        if(heuristic(hr, allDemandsID)) {
            placement_pathFormulation(params.maxTime, instance.graph, params.nbSDN, instance.demands, instance.nextHops, params.nbPath, filename, &hr);    
        } else {
            placement_pathFormulation(params.maxTime, instance.graph, params.nbSDN, instance.demands, instance.nextHops, params.nbPath, filename);
        }
    } else if(params.model == "path-sdn") {
        const std::string filename = "results/real1/"+params.name+"_"+std::to_string(params.nbSDN)+"_"+to_string_with_precision(params.factor, 2)+"_LP_"+params.model+"_"+std::to_string(params.nbPath)+".res";
        // Hybrid::HybridRouting hr(dg, SDN, nextHops, std::make_tuple(0, 0, 0));
    }
    return 1;
}

void placement(const DiGraph& _topology, const int& _nbSDN, const std::vector<Demand>& _demands, const Matrix<int>& _nextHops, const std::string& _filename, const std::vector<int>* _SDNs) {
    IloEnv env;
    IloModel model(env);

    std::map<std::tuple<int, int, int, int, int, int>, IloBoolVar> g;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> f;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> m;
    std::map<std::tuple<int, int, int, int>, IloBoolVar> n;
    std::map<std::tuple<int, int, int>, IloBoolVar> b;
    std::map<std::tuple<int, int>, IloBoolVar> x;
    std::vector<IloBoolVar> a;
    a.reserve(_topology.getOrder());
    for(int i = 0; i < _topology.getOrder(); ++i)
    {
        a.emplace_back(env, std::string("a"+std::to_string(i)).c_str()); 
    }

    const std::vector<int> vertices = _topology.getVertices();
    std::set<int> sources, destinations;
    for(const auto& demand : _demands) {
        sources.insert(demand.s);
        destinations.insert(demand.t);
    }
 
    std::cout << "Building variables..." << '\n';
    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        x.emplace(std::make_pair(u, v), IloBoolVar(env, std::string("x("+std::to_string(u)+"_"+std::to_string(v)+")").c_str()));
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;

            f.emplace( std::make_tuple(u, v, s, t), IloBoolVar( env, std::string("f("+std::to_string(u)+"_"+std::to_string(v)+"_"+std::to_string(s)+"_"+std::to_string(t)+")").c_str() ) );
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(z != y) {
                        g.emplace(std::make_tuple(u, v, y, z, s, t), IloBoolVar(env, std::string("g(" + to_string(std::make_tuple(u, v, y, z, s, t)) + ")").c_str()));
                    }
                }
            }
        }
        for(const auto& y : vertices) {
            for(const auto& z : vertices) {
                if(z != y) {
                    n.emplace( std::make_tuple(u, v, y, z), IloBoolVar( env, std::string("n("+std::to_string(u)+"_"+std::to_string(v)+"_"+std::to_string(y)+"_"+std::to_string(z)+")").c_str() ) );
                }
            }
        }
    }

    for(auto& t: destinations) {
        for(const auto& y : vertices) {
            for(const auto& z : vertices) {
                if(z != y) {
                    b.emplace(std::make_tuple(y, z, t), IloBoolVar(env, std::string("b("+std::to_string(y)+"_"+std::to_string(z)+"_"+std::to_string(t)+")").c_str()));
                }
            }
        }
    }
    

    std::cout << "Building objective function..." << '\n';
    IloExpr objectiveFunc(env);
    for(const auto& pair : x) {
        objectiveFunc += pair.second;
    }
    model.add(IloMinimize(env, objectiveFunc));
    model.add(2 * (_topology.getOrder() - 1) <= objectiveFunc <= _topology.size());
    // model.add(x[std::make_tuple(5, 3)] == 0);

    std::cout << "Building flow conservation constraints..." << '\n';
    for(const auto& u : vertices) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            int val = 0;
            if(u == s) {
                val = 1;
            } else if (u == t) {
                val = -1;
            }
            IloExpr flowCons(env);
            for(const auto& v : _topology.getNeighbors(u)) {
                flowCons += f[std::make_tuple(u, v, s, t)];
                flowCons -= f[std::make_tuple(v, u, s, t)];
            }
            for(const auto& y : vertices) {
                if(u != y) {
                    flowCons += b[std::make_tuple(u, y, t)];
                    flowCons -= b[std::make_tuple(y, u, t)];
                }
            }
            model.add(flowCons == val).setName("flow conservation constraints");
        }
    }

    std::cout << "Building capacity constraints..." << '\n';
    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        IloExpr capa(env);
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            const double& d = demand.d;

            capa += d * f[std::make_tuple(u, v, s, t)];
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(z != y) {
                        capa += d * g[std::make_tuple(u, v, y, z, s, t)];
                    }
                }
            }
        }
        model.add(capa <= x[std::make_tuple(u, v)] * _topology.getEdgeWeight(u, v)).setName("capa constraints");
    }

    std::cout << "Building flow conservation for tunnels..." << '\n';
    for(const auto& u : vertices) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(z != y) {
                        IloExpr flowConsTunnels(env);
                        for(const auto& v : _topology.getNeighbors(u)) {
                            flowConsTunnels += g[std::make_tuple(u, v, y, z, s, t)];
                            flowConsTunnels -= g[std::make_tuple(v, u, y, z, s, t)];
                        }
                        if(u == y) {
                            model.add(flowConsTunnels == b[std::make_tuple(y, z, t)]).setName("flow conservation for tunnels");
                        } else if( u == z ) {
                            model.add(flowConsTunnels == -b[std::make_tuple(y, z, t)]).setName("flow conservation for tunnels");
                        } else {
                            model.add(flowConsTunnels == 0).setName("flow conservation for tunnels");
                        }
                    }
                }
            }
        }
    }

    std::cout << "Building flow next hop constraints..." << '\n';
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& edge : _topology.getEdges()) {
            const int u = edge.first, v = edge.second;
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(u != z && z != y) {
                        assert(n.find(std::make_tuple(u, v, y, z)) != n.end());
                        model.add( g[std::make_tuple(u, v, y, z, s, t)] <= n[std::make_tuple(u, v, y, z)] ).setName("flow next hop constraints");
                    }
                }
            }
        }
    }

    std::cout << "Building flow usage constraints..." << '\n';
    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            if(u != t) {
                model.add(f[std::make_tuple(u, v, s, t)] <= n[std::make_tuple(u, v, s, t)]).setName("flow usage constraints");
            }
        }
    }

    std::cout << "Building one next hop constraints..." << '\n';
    for(const auto& u : vertices) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            IloExpr oneNextHop(env);
            for(const auto& v : _topology.getNeighbors(u)) {
                oneNextHop += n[std::make_tuple(u, v, s, t)];
            }
            model.add(oneNextHop <= 1).setName(std::string("one next hop (#1) for " + to_string(std::make_tuple(u, s, t))).c_str());
        }
    }


    std::cout << "Building link sdn constraints..." << '\n';
    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        model.add( x[std::make_tuple(u, v)] >= 1 - a[u] - a[v] ).setName("link/sdn");
    }

    std::cout << "Building next hop usage..." << '\n';
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& edge : _topology.getEdges()) {
            const int& u = edge.first, v = edge.second;
            model.add(n[std::make_tuple(u, v, s, t)] <= x[std::make_tuple(u, v)]).setName("next hop usage #1");
        }

        for(const auto& u : vertices) {
            for(const auto& v : _topology.getNeighbors(u)) {
                if(v != _nextHops(u, t)) {
                    model.add(n[std::make_tuple(u, v, s, t)] <= a[u]).setName("next hop usage #2");
                }
            }
        }
    }

    std::cout << "Building backup usage..." << '\n';
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& edge : _topology.getEdges()) {
            const int& u = edge.first, v = edge.second;
            IloExpr backups(env);
            for(const auto& y : vertices) {
                if(u != y) {
                    backups += b[std::make_tuple(u, y, t)];
                }
            }
            // model.add(n[std::make_tuple(u, v, s, t)] - x[std::make_tuple(u, v)] <= backups).setName( std::string("backup usage (#1) on " + std::to_string(u) + ", " + std::to_string(v) + " for " + std::to_string(s) + ", " + std::to_string(t)).c_str() );
            model.add(backups <= 2 - n[std::make_tuple(u, v, s, t)] - x[std::make_tuple(u, v)]).setName( std::string("backup usage (#2) on " + std::to_string(u) + ", " + std::to_string(v) + " for " + std::to_string(s) + ", " + std::to_string(t)).c_str() );
        }
    }

    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        model.add(x[std::make_tuple(u, v)] == x[std::make_tuple(v, u)]).setName("link equality");
    }

    IloExpr nbSDN(env);
    for(const auto& u: vertices) {
        nbSDN += a[u];
    }
    model.add(nbSDN <= _nbSDN).setName("nbSDN");

    IloCplex cplex(model);
    cplex.setParam(IloCplex::TiLim, 60*60);
    cplex.setParam(IloCplex::ProbeTime, 5*60);
    // cplex.setParam(IloCplex::Threads, 1);
    // cplex.setParam(IloCplex::Probe, -1);
    cplex.setParam(IloCplex::MIPDisplay, 5);
    cplex.exportModel("lp.lp");
    if(!cplex.solve()) {
        if ( ( cplex.getStatus() == IloAlgorithm::Infeasible ) ||
            ( cplex.getStatus() == IloAlgorithm::InfeasibleOrUnbounded  ) ) {
         std::cout << '\n' << "No solution" << '\n';
        }
    }
    cplex.writeSolutions(_filename.c_str());
    std::cout << cplex.getTime() << '\t' << cplex.getObjValue() << '\t' << cplex.getMIPRelativeGap() << '\n';
}

void placement_pathFormulation(int _time, const DiGraph& _topology, const int& _nbSDN, const std::vector<Demand>& _demands, const Matrix<int>& _nextHops, int _k, const std::string& _filename, const Hybrid::HybridRouting* _hr, const std::vector<int>* _SDNs) {
    IloEnv env;
    IloModel model(env, _filename.c_str());

    const std::vector<int> vertices = _topology.getVertices();
    std::cout << "Building k-shortest path..." << std::endl;
    Matrix< std::vector<Graph::Path> > kShortestPath( _topology.getOrder(), _topology.getOrder() );
    Matrix< std::vector<Graph::Path*> > pathOnEdge( _topology.getOrder(), _topology.getOrder() );
    for(const auto& y : vertices) {
        for(const auto& z : vertices) {
            if(z != y) {
                kShortestPath(y, z) = ShortestPath<DiGraph>(_topology).getKShortestPath(y, z, _k);
                // std::cout << "kShortestPath" << std::make_tuple(y, z) << ": " << kShortestPath(y, z) << std::endl;
                for(auto& path : kShortestPath(y, z)) {
                    for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                        pathOnEdge(*iteU, *iteV).push_back(&path);
                    }
                }
            }
        }
    }

    std::map< std::tuple< Graph::Path*, int, int >, IloBoolVar > g; // Path p is used for demand s and t
    std::map< std::tuple< int, int, int >, IloBoolVar > h;
    std::map< std::tuple< int, int, int >, IloBoolVar > e;
    std::map< std::tuple< int, int, int, int >, IloBoolVar > f; // Flow for demand s and t on (u, v)
    std::map< std::tuple< int, int, int, int >, IloBoolVar > n;
    std::map< Graph::Edge, IloBoolVar > x;
    std::vector<IloBoolVar> a;

    std::set<int> sources, destinations;
    for(const auto& demand : _demands) {
        sources.insert(demand.s);
        destinations.insert(demand.t);
    }

    std::cout << "Building variables..." << std::endl;
    a.reserve(_topology.getOrder());

    for(int i = 0; i < _topology.getOrder(); ++i)
    {
        a.emplace_back(env, std::string("a"+std::to_string(i)).c_str()); 
    }

    for(const auto& arc : _topology.getEdges()) {
        const int u = arc.first, v = arc.second;
        x.emplace(arc, IloBoolVar(env, std::string("x("+std::to_string(u)+"_"+std::to_string(v)+")").c_str()));
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            f.emplace( std::make_tuple(u, v, s, t), IloBoolVar( env, std::string("f("+std::to_string(u)+"_"+std::to_string(v)+"_"+std::to_string(s)+"_"+std::to_string(t)+")").c_str() ) );
        }

        for(const auto& y : vertices) {
            for(const auto& z : vertices) {
                if(z != y && u != z) {
                    n.emplace( std::make_tuple(u, v, y, z), IloBoolVar( env, std::string("n("+std::to_string(u)+"_"+std::to_string(v)+"_"+std::to_string(y)+"_"+std::to_string(z)+")").c_str() ) );

                }
            }
        }
    }
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(auto& u : vertices) {
            if(u != t) {
                e.emplace( std::make_tuple(u, s, t), IloBoolVar( env, std::string("e("+to_string(std::make_tuple(u, s, t))+")").c_str() ) );
            }
        }
    }

    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& y : vertices) {
            for(const auto& z : vertices) {
                if(z != y) {
                    int i = 0;
                    for(Graph::Path& path : kShortestPath(y, z)) {
                        g.emplace( std::make_tuple(&path, s, t), IloBoolVar(env, std::string("g(" + to_string(std::make_tuple(y, z, ++i, s, t)) + ")").c_str()));
                    }
                }
            }
        }
    }

    for(const auto& t : destinations) {
        for(const auto& u : vertices) {
            for(const auto& y : vertices) {
                if(u != t && t != y && u != y) {
                    h.emplace( std::make_tuple(u, t, y), IloBoolVar(env, std::string("h(" + to_string(std::make_tuple(u, t, y)) + ")").c_str()));
                }
            }
        }
    }

    #ifndef NDEBUG
        // Hybrid::HybridRouting hr(_topology, _SDNs, _nextHops, std::make_tuple(0, 0, 0));
    #endif

    std::cout << "Building objective function..." << std::endl;
    IloExpr objectiveFunc(env);
    for(const auto& pair : x) {
        if(pair.first.first < pair.first.second) {
            objectiveFunc += pair.second;
        }
    }
    model.add(IloMinimize(env, objectiveFunc));
    model.add(2 * (_topology.getOrder() - 1) <= 2 * objectiveFunc <= _topology.size());

    // model.add(x[std::make_tuple(5, 3)] == 0);


    std::cout << "Building flow conservation constraints..." << std::endl;
    for(const auto& u : vertices) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            int val = 0;
            if(u == s) {
                val = 1;
            } else if (u == t) {
                val = -1;
            }
            IloExpr flowCons(env);
            for(const auto& v : _topology.getNeighbors(u)) {
                flowCons += f[ std::make_tuple(u, v, s, t) ];
                flowCons -= f[ std::make_tuple(v, u, s, t) ];
            }            
            for(const auto& y : vertices) {
                if(u != y && y != t) {
                    for(Graph::Path& path : kShortestPath(u, y)) {
                        flowCons += g[ std::make_tuple(&path, s, t) ];
                    }
                    for(Graph::Path& path : kShortestPath(y, u)) {
                        flowCons -= g[ std::make_tuple(&path, s, t) ];
                    }
                }
            }
            model.add(flowCons == val).setName("flow conservation constraints");
        }
    }

    std::cout << "Building capacity constraints..." << std::endl;
    for(const auto& arc : _topology.getEdges()) {
        const int u = arc.first, v = arc.second;
        IloExpr capa(env);
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            const double& d = demand.d;

            capa += d * f[std::make_tuple(u, v, s, t)];
            for(Graph::Path* pathId : pathOnEdge(u, v)) {
                capa += d * g[std::make_tuple(pathId, s, t)];
            }
        }
        model.add(capa <= x[arc] * _topology.getEdgeWeight(u, v)).setName("capa constraints");
    }

    std::cout << "Building path/next hop constraints..." << std::endl;
    for(const auto& y : vertices) {
        for(const auto& z : vertices) {
            if(y != z) {
                for(Graph::Path& path: kShortestPath(y, z)) {
                    IloExpr usage(env);
                    for(auto iteU = path.begin(), iteV = std::next(iteU); iteV != path.end(); ++iteU, ++iteV) {
                        usage += n[std::make_tuple(*iteU, *iteV, y, z)];
                    }
                    for(const auto& demand: _demands) {
                        const auto& s = demand.s, t = demand.t;
                        model.add( (int( path.size()-1 ) * g[std::make_tuple(&path, s, t)]) <= usage ).setName("path/next hop");
                    }
                }
            }
        }
    }

    std::cout << "Building next hop usability constraints..." << std::endl;
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(auto& u : vertices) {
            if ( u != t ) {
                for(auto& v : _topology.getNeighbors(u)) {
                    model.add( e[std::make_tuple(u, s, t)] >= n[std::make_tuple(u, v, s, t)] - x[std::make_pair(u, v)] );
                }
            }
        }
    }

    for(auto& u : vertices) {
        for(auto& t : destinations) {
            if(u != t) {
                IloExpr left(env);
                for(auto& y : vertices) {
                    if(u != y && y != t) {
                        left += h[std::make_tuple(u, t, y)];
                    }
                }
                IloExpr right(env);
                for(const auto& demand: _demands) {
                    const auto& s = demand.s, t_ = demand.t;
                    if(t_ == t) {
                        right += e[std::make_tuple(u, s, t)];
                    }
                }
                model.add(left <= right);
                model.add(left <= 1);
            }
        }
    }


    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& y : vertices) {
            for(const auto& z : vertices) {
                if(y != t && y != z && t != z) {
                    for(Graph::Path& path: kShortestPath(y, z)) {
                        model.add(g[std::make_tuple(&path, s, t)] <= h[std::make_tuple(y, t, z)]).setName("backup demand/backup");
                    }
                }
            }
        }
    }

    // std::cout << "Building backup/next hop constraints..." << std::endl;
    // for(const auto& demand: _demands) {
    //     const auto& s = demand.s, t = demand.t;
    //     for(const auto& edge : _topology.getEdges()) {
    //         const int& u = edge.first, v = edge.second;
    //         if(u != t) {
    //             IloExpr sumBackup(env);
    //             for(const auto& y : vertices) {
    //                 if(u != t && t != y && u != y) {
    //                     sumBackup += h[std::make_tuple(u, t, y)];
    //                 }
    //             }
    //             model.add( sumBackup <= 2 - x[ edge ] - n[ std::make_tuple(u, v, s, t) ] ).setName("backup/next hop");
    //         }
    //     }
    // }

    std::cout << "Building flow/next hop constraints..." << std::endl;
    for(const auto& edge : _topology.getEdges()) {
        const int u = edge.first, v = edge.second;
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            if(u != t) {
                model.add( f[ std::make_tuple(u, v, s, t) ] <= n[ std::make_tuple(u, v, s, t) ] ).setName("flow/next hop");
            }
        }
    }

    std::cout << "Building one next hop constraints..." << std::endl;
    for(const auto& u : vertices) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            if(u != t) {
                IloExpr oneNextHop(env);
                for(const auto& v : _topology.getNeighbors(u)) {
                    oneNextHop += n[ std::make_tuple(u, v, s, t) ];
                }
                model.add( oneNextHop == 1 ).setName(std::string("one next hop (#1) for " + to_string(std::make_tuple(u, s, t))).c_str());
            }
        }
    }

    std::cout << "Building next hop usage..." << std::endl;
    for(const auto& demand: _demands) {
        const auto& s = demand.s, t = demand.t;
        for(const auto& arc : _topology.getEdges()) {
            const int& u = arc.first, v = arc.second;
            if(u != t) {
                model.add( f[std::make_tuple(u, v, s, t)] <= x[arc] ).setName("next hop usage #1");
            }
        }

        for(const auto& u : vertices) {
            for(const auto& v : _topology.getNeighbors(u)) {
                if(u != t && v != _nextHops(u, t)) {
                    model.add(n[std::make_tuple(u, v, s, t)] <= a[u]).setName("next hop usage #2");
                }
            }
        }
    }

    std::cout << "Building link equality constraints..." << std::endl;
    for(const auto& arc : _topology.getEdges()) {
        const int u = arc.first, v = arc.second;
        model.add(x[arc] == x[Graph::Edge(v, u)]).setName("link equality");
    }

    std::cout << "Building link sdn constraints..." << std::endl;
    for(const auto& arc : _topology.getEdges()) {
        const int u = arc.first, v = arc.second;
        model.add( x[arc] >= 1 - a[u] - a[v] ).setName("link/sdn");
    }

    IloExpr nbSDN(env);
    for(const auto& u: vertices) {
        nbSDN += a[u];
    }
    model.add(nbSDN <= _nbSDN).setName("nbSDN");


    if(_SDNs) {
        for (int i = 0; i < int(_SDNs->size()); ++i)
        {
            if((*_SDNs)[i]) {
                model.add(a[i] == 1);
            }
        }
    }

    IloCplex cplex(model);
    if(_time != -1){
        cplex.setParam(IloCplex::TiLim, _time);
        cplex.setParam(IloCplex::ProbeTime, 15*60);
    }
    // cplex.setParam(IloCplex::Threads, 1);
    // cplex.setParam(IloCplex::Probe, -1);
    cplex.setParam(IloCplex::MIPDisplay, 5);
    std::cout << "Exporting model..." << std::endl;
    cplex.exportModel("lp.lp"); 
    std::cout << "MIP Start" << std::endl;

    IloNumVarArray varArr(env);
    IloNumArray valArr(env);
    if(!_hr) {
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            int u = s;
            while(u != t) {
                varArr.add( f[std::make_tuple(u, _nextHops(u, t), s, t)] );
                valArr.add( 1 );
                u = _nextHops(u, t);
            }
        }
        for(const auto& arc : _topology.getEdges()) {
            varArr.add(x[arc]);
            valArr.add(1);
        }
    } else {
        // Fix SDN variable
        for(int i = 0; i < _topology.getOrder(); ++i) {   
            varArr.add(a[i]);
            if(_hr->isSDN(i)) {
                valArr.add(1); 
            } else {
                valArr.add(0);
            }
        }

        for(const auto& arc : _topology.getEdges()) {
            const int u = arc.first, v = arc.second;
            varArr.add(x[arc]);
            if(_hr->getFlowGraph().hasEdge(u, v)) {
                valArr.add(1);
            } else {
                valArr.add(0);
            }
        }


        for(const auto& demand: _demands) {
            for(const auto& arc : _topology.getEdges()) {
                const int u = arc.first, v = arc.second;
                const auto& s = demand.s, t = demand.t;
                varArr.add(f[std::make_tuple(u, v, s, t)]);
                valArr.add(0);
            }
        }
        

        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(z != y) {
                        for(Graph::Path& path : kShortestPath(y, z)) {
                            varArr.add( g[std::make_tuple(&path, s, t)] );
                            valArr.add(0);
                        }
                    }
                }
            }
        }
        
        int i = 0;
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            auto direct_tunnels = _hr->getDecomposedPath(s, t);
            for(const std::tuple<int, int, int, int>& tpl : direct_tunnels.first) {
                varArr.add( f[tpl] );
                #ifndef NDEBUG 
                std::cout << "f["<< tpl << "]" << std::endl; 
                #endif
                valArr.add(1);
            }
            for(auto& tpl : direct_tunnels.second) {
                auto& bPath = std::get<0>(tpl);
                bool found = false;
                for(auto& path : kShortestPath(bPath.front(), bPath.back())) {
                    if(path == bPath) {
                        varArr.add( g[std::make_tuple(&path, std::get<1>(tpl), std::get<2>(tpl))] );
                        #ifndef NDEBUG 
                        std::cout << "g["<< std::make_tuple(path, std::get<1>(tpl), std::get<2>(tpl)) << "]" << std::endl; 
                        #endif
                        valArr.add(1);
                        found = true;
                        break;
                    }
                }    
                if(!found) {
                    std::cerr << "Backup path not found!" << bPath << " => " << kShortestPath(bPath.front(), bPath.back()) << std::endl;
                }
            }
            ++i;
        }

        for(const auto& t : destinations) {
            for(const auto& u : vertices) {
                for(const auto& y : vertices) {
                    if(u != t && t != y && u != y) {
                        varArr.add( h[std::make_tuple(u, t, y)] );
                        if(_hr->isBackupEnd(u, t, y)) {
                            #ifndef NDEBUG 
                            std::cout << "h["<< std::make_tuple(u, t, y) << "]" << std::endl; 
                            #endif
                            valArr.add(1);
                        } else {
                            valArr.add(0);
                        }
                    }
                }
            }
        }
    }
    cplex.addMIPStart(varArr, valArr);

    IloConstraintArray consArr(env);
    IloNumArray numArr(env);
    for(IloModel::Iterator iter(model); iter.ok(); ++iter) {
        if ((*iter).asConstraint().getImpl()) {
            consArr.add((*iter).asConstraint());
            numArr.add(1);
        }
    }
    if(cplex.refineMIPStartConflict(0, consArr, numArr)) {
        auto conflict = cplex.getConflict(consArr);
        env.getImpl()->useDetailedDisplay(IloTrue);
        for(int i = 0; i < int(consArr.getSize()); ++i) {
            if ( conflict[i] == IloCplex::ConflictMember) {
                std::cout << "Proved  : " << consArr[i] << std::endl;
            } else if ( conflict[i] == IloCplex::ConflictPossibleMember) {
                std::cout << "Possible: " << consArr[i] << std::endl;
            }
        }
        
    }

    if(cplex.solve()) {
        std::cout << cplex.getTime() << '\t' << cplex.getObjValue() << '\t' << cplex.getMIPRelativeGap() << std::endl;
        std::ofstream ofs(_filename);
                
        bool first = true;
        for(int i = 0; i < _topology.getOrder(); ++i) {
            if(cplex.getValue(a[i])) {
                if(!first) {
                    ofs << '\t';
                } else {
                    first = false;
                }
                ofs << i;
            }
        }
        ofs << std::endl;

        int nbActive = 0;
        for(const auto& edge : _topology.getEdges()) {
            if(cplex.getValue(x[edge])) {
                ++nbActive;
            }
        }
        ofs << nbActive << std::endl;
        for(const auto& edge : _topology.getEdges()) {
            if(cplex.getValue(x[edge])) {
                ofs << edge.first << '\t' << edge.second << '\t' << 0 << std::endl;
            }
        }
        ofs << 0 << std::endl;
        ofs << cplex.getMIPRelativeGap() << std::endl;
        std::list< std::tuple<int, int, int> > backups;
        
        for(const auto& t : destinations) {
            for(const auto& y : vertices) {
                for(const auto& z : vertices) {
                    if(y != t && t != z && y != z) {
                        if(cplex.getValue( h[std::make_tuple(y, t, z)] ) == 1) {
                            // std::cout << n[std::make_tuple(y, )]
                            backups.emplace_back(y, t, z);
                        }
                    }
                }
            }
        }
        ofs << backups.size() << std::endl;
        for(auto& t : backups) {
            ofs << std::get<0>(t) << '\t' << std::get<1>(t) << '\t' << std::get<2>(t) << std::endl;
        }
        ofs << cplex.getTime() << std::endl;

        #ifndef NDEBUG
        for(const auto& demand: _demands) {
            const auto& s = demand.s, t = demand.t;
            std::cout << demand << " => ";
            int u = s;
            while(u != t) {
                bool direct = false;
                for(auto& v : _topology.getNeighbors(u)) {
                    if(cplex.getValue(f[std::make_tuple(u, v, s, t)])) {
                        std::cout << "f" << std::make_tuple(u, v, s, t) << '\t';
                        u = v;
                        direct = true;
                        goto endOfLoop;
                    }
                }
                if(!direct) {
                    for(const auto& y : vertices) {
                        for(const auto& z : vertices) {
                            if(z != y) {
                                for(Graph::Path& path : kShortestPath(y, z)) {
                                    if(cplex.getValue(g[std::make_tuple(&path, s, t)])) {
                                        std::cout << "g" << std::make_tuple(path, s, t) << '(' << cplex.getValue(h[std::make_tuple(u, t, path.back())]) << ')' << '\t';
                                        u = path.back();
                                        goto endOfLoop;
                                    }
                                }
                            }
                        }
                    }
                }
                endOfLoop:;
            }
            std::cout << std::endl;
        }
        #endif
    } else {
        std::cout << std::endl << "No solution" << std::endl;
    }
}