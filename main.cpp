#include <iostream>
#include <iomanip>
#include <chrono>

#include <tclap/CmdLine.h>
#include <HybridRouting.hpp>
#include <SNDlib.hpp>
#include <utility.hpp>

struct Param { 
    std::string model;
    std::string name;
    double factor;
    int nbSDN;
    std::string energyModel;
    int nodeMetric;
};

Param getParams(int argc, char** argv) {
    TCLAP::CmdLine cmd("Solve the SFC problem with column generation", ' ', "1.0");
    
    TCLAP::ValuesConstraint<std::string> vCons({ "normal", "maxAddStretch", "maxMultStretch", "average", "first-sdn" });
    TCLAP::ValueArg<std::string> modelName("m", "model", "Specify the model used", 
        false, "normal", &vCons);        
    cmd.add(&modelName);

    TCLAP::UnlabeledValueArg<std::string> networkName("network", "Specify the network name", 
        true, "", "network");
    cmd.add(&networkName);

    TCLAP::ValueArg<std::string> energyModel("e", "energyModel", "Specify the energy model",
        false, "real1", "string");
    cmd.add(&energyModel);

    TCLAP::ValueArg<double> demandFactor("d", "demandFactor", "Specify the multiplicative factor for the demands",
        false, 1, "int");
    cmd.add(&demandFactor);

    TCLAP::ValueArg<int> nodeMetric("n", "nodeMetric", "Specify the metric used for sorting the nodes (0=degree, 1=closeness, 2=betweeness, 3=max cover)",
        false, 0, "int");
    cmd.add(&nodeMetric);

    TCLAP::ValueArg<int> nbSDN("s", "numberSDN", "Specify the number of SDN nodes", 
        true, 4, "int");
    cmd.add(&nbSDN);

    cmd.parse(argc, argv);

    return { modelName.getValue(), networkName.getValue(), demandFactor.getValue(), 
            nbSDN.getValue(), energyModel.getValue(), nodeMetric.getValue() };
}

int main(int argc, char** argv) {

 //    if(argc < 7) {
 //        std::cerr << "Missing arguments: ./Hybrid {name} {nbSDN} {params.factor} {energy} {params.nodeMetric} {params.}\n";
 //        exit(-1);
 //    }
	// std::string name(argv[1]);
	// int nbSDN = std::stoi(argv[2]);
	// double params.factor = std::stod(argv[3]);
 //    std::string energy(argv[4]);
 //    int params.nodeMetric = std::stoi(argv[5]);
 //    std::string params.(argv[6]);

 //    auto demands = loadDemandsFromFile(std::string("./instances/" + name + "_demand.txt"));
 //    auto topology = DiGraph::loadFromFile(std::string("./instances/" + name + "_topo.txt"));
 //    auto nextHops = loadNextHop(std::string("./instances/" + name + "_nextHop.txt"));
    
    #if defined(LOG_LEVEL)
        std::cout << "Loggin level at " << LOG_LEVEL << '\n';
    #endif
    const auto params = getParams(argc, argv);

    auto demands = loadDemandsFromFile("./instances/" + params.name + "_demand.txt");
    const auto topology = DiGraph::loadFromFile("./instances/" + params.name + "_topo.txt");
    const auto nextHops = loadNextHop("./instances/" + params.name + "_nextHop.txt");
    const auto energyTuple = loadEnergy(std::string("./instances/energy_" + params.energyModel + ".txt"));

    std::vector<int> SDN; 
    if(params.nodeMetric < 3) {
        SDN = loadSDNs(std::string("./instances/" + params.name + "_SDNs.txt"), params.nbSDN, params.nodeMetric);
    } else { 
        SDN = loadSDNsMaxCover(std::string("./instances/" + params.name + "_max-cover.txt"), params.nbSDN);
    }

    std::vector<int> allDemandsID;
    int i = 0; 
    for(auto& demand : demands) {
        demand.d *= params.factor;
        allDemandsID.push_back(i);
        ++i;
    }

    Hybrid::FixedSDNInstance instance(std::get<0>(topology), energyTuple, nextHops, demands, SDN);


    std::cout << "Next hops:\n";
    std::cout << nextHops << '\n';
    std::cout << std::list<int>() << '\n';

    std::cout << "SDNs are :";
    for(int i = 0; i < std::get<0>(topology).getOrder(); ++i) {
    	if(SDN[i]) {
            std::cout << i << ", ";
        }
    }
    std::cout << '\n';
    if(params.model == "normal") {
        // Hybrid::HybridRouting hr(std::get<0>(topology), SDN, nextHops, energyTuple);
        
        Hybrid::HybridRouting hr(instance);
        auto start = std::chrono::high_resolution_clock::now();
        if(heuristic(hr, allDemandsID)) {
            const auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> diff = end-start;
            for(auto& edge : hr.getFlowGraph().getEdges()) {
                std::cout << edge << " -> " << hr.getFlowGraph().getEdgeWeight(edge) << " / " << instance.graph.getEdgeWeight(edge) << " (" << hr.getFlowGraph().getEdgeWeight(edge) / instance.graph.getEdgeWeight(edge) << ')' << '\n';
            }
            std::cout << "# Active links " << hr.getActiveLinks() << '/' << instance.graph.size() << '\n';

            std::cout << "Backups: " << hr.getUsedTunnels() << '\n' << hr.getBackupRules() << '\n';
            std::cout << "Rules: " << hr.getRules() << '\n';

            std::cout << "# backup: " << hr.getNbBackup() << ", " << hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << hr.getNbRules() << '\n';
            std::cout << "Energy used: " << hr.getPower() << '\n';
            
            hr.save("results/" + params.energyModel + "/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_heur_"+params.model+"_"+std::to_string(params.nodeMetric)+".res", diff.count());
        }
    } else if(params.model == "first-sdn") {
        // Hybrid::HybridRouting hr(std::get<0>(topology), SDN, nextHops, energyTuple);
        Hybrid::HybridRouting hr(instance);
        auto start = std::chrono::high_resolution_clock::now();
        if(heuristic_firstSDN(hr, allDemandsID)) {
            const auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> diff = end-start;
            for(auto& edge : hr.getFlowGraph().getEdges()) {
                std::cout << edge << " -> " << hr.getFlowGraph().getEdgeWeight(edge) << " / " << instance.graph.getEdgeWeight(edge) << " (" << hr.getFlowGraph().getEdgeWeight(edge) / instance.graph.getEdgeWeight(edge) << ')' << '\n';
            }
            std::cout << "# Active links " << hr.getActiveLinks() << '/' << instance.graph.size() << '\n';

            std::cout << "Backups: " << hr.getUsedTunnels() << '\n' << hr.getBackupRules() << '\n';
            std::cout << "Rules: " << hr.getRules() << '\n';

            std::cout << "# backup: " << hr.getNbBackup() << ", " << hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << hr.getNbRules() << '\n';
            std::cout << "Energy used: " << hr.getPower() << '\n';
            
            hr.save("results/" + params.energyModel + "/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_heur_"+params.model+"_"+std::to_string(params.nodeMetric)+".res", diff.count());
        }
    }
    else if( params.model == "average" ) {
        // Hybrid::HybridRouting hr(std::get<0>(topology), SDN, nextHops, energyTuple);
        Hybrid::HybridRouting hr(instance);
        auto start = std::chrono::high_resolution_clock::now();
        if( heuristic_averageDemand(hr, allDemandsID) ) {
            auto end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> diff = end-start;
            for(auto& edge : hr.getFlowGraph().getEdges()) {
                std::cout << edge << " -> " << hr.getFlowGraph().getEdgeWeight(edge) << " / " << instance.graph.getEdgeWeight(edge) << " (" << hr.getFlowGraph().getEdgeWeight(edge) / instance.graph.getEdgeWeight(edge) << ')' << '\n';
            }
            std::cout << "# Active links " << hr.getActiveLinks() << '/' << instance.graph.size() << '\n';

            std::cout << "Backups: " << hr.getUsedTunnels() << '\n' << hr.getBackupRules() << '\n';
            std::cout << "Rules: " << hr.getRules() << '\n';

            std::cout << "# backup: " << hr.getNbBackup() << ", " << hr.getNbBackupRules() << '\n';
            std::cout << "# rules: " << hr.getNbRules() << '\n';
            std::cout << "Energy used: " << hr.getPower() << '\n';
            
            hr.save("results/" + params.energyModel + "/" + params.name + "_" + std::to_string(params.nbSDN) + "_" + to_string_with_precision(params.factor, 2) + "_heur_"+params.model+"_"+std::to_string(params.nodeMetric)+".res", diff.count());
        }
    } else {
        std::cerr << "No heuristic specified!\n";
        return -1;
    }
	return 0;
}